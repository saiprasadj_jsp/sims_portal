Steps To Configure AS2:-

Server Side:-

1) Open keystore explorer and create key value pair. Eg: PartyA.jks (Alias name: keyvaluepairentryname)
2) Export certificate chain from this JKS file. Eg: PartyA.cer

Client Side:-

3) Open keystore explorer and create key value pair. Eg: PartyB.jks (Alias name: keyvaluepairentryname)
4) Export certificate chain from this JKS file. Eg: PartyB.cer

5) Import PartyA.cer on PartyB.jks (Alias name: importedcertificateentryname)
6) Import PartyB.cer on PartyA.jks (Alias name: importedcertificateentryname)


Alias names should be similar at either ends for saved/created JKS entries.

i.e., Every JKS file will consist of two entries 
 
 i)  Public and Private key pair entry it has one alias name.
 ii) Imported certificate entry it has one alias name.
 
 Here in above cases alias names should be same.


In return to EDI message sent and From receiving flow after integrity check, decryption and verifying signature process completion an MDN will be sent back as response to caller this message contains message id.

This message id is sent to receiver flow from caller flow after successfull validation, decryption and siganture verification an MDN is created and sent back to client as response.

