package com.sims.portal.masters.constants;

public class MastersPageConstants {

	public static final String ACCOUNT_MASTER_MAIN_PAGE = "master/account.page/Account";
	public static final String DEPARTMENT_MASTER_MAIN_PAGE = "master/department.page/Department";
	public static final String EMPLOYEE_MASTER_MAIN_PAGE = "master/employee.page/Employee";
	public static final String SECTION_MASTER_MAIN_PAGE = "master/section.page/Section";
	public static final String PRODUCT_MASTER_MAIN_PAGE = "master/product.page/Product";
	public static final String WARE_HOUSE_MASTER_MAIN_PAGE = "master/warehouse.page/WareHouse";
	public static final String PRODUCT_MASTER_INNER_POPUP = "productdetailspopup.page/masters/Product Details";
	public static final String DEPARTMENT_MASTER_INNER_POPUP = "departmentdetailspopup.page/masters/Department Details";
	public static final String EMPLOYEE_MASTER_INNER_POPUP = "employeedetailspopup.page/masters/Employee Details";
	public static final String SECTION_MASTER_INNER_POPUP = "tankdetailspopup.page/masters/Tank Details";
	public static final String WAREHOUSE_MASTER_INNER_POPUP = "warehousedetailspopup.page/masters/Tank Details";
	
	
	// Purchase Order CONSTANT 
	public static final String PURCHASE_REQUISITION_MAIN_PAGE = "purchase.page";
	public static final String PURCHASE_ORDER_MAIN_PAGE = "purchaseOrder.page";
	public static final String MATETIAL_RECEIPT_MAIN_PAGE = "materialReceipt.page";
	public static final String DIRECT_PURCHASE_MAIN_PAGE = "directPurchasp.page";
	
		public static final String TILES_LINE_PURCHASE_ORDER_MAIN_URL= "/admin/purchase/purchaseorder";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_MAIN_URL = "/purchase/purchaseorderline";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SAVE_URL = "purchase/purchaseorderline/save";
		
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SHOW_MAIN_JSP = "purchase/purchaseorder";
		
		
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_UPDATE_URL = "/purchase/purchaseorderline/update/";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_DELETE_URL = "/purchase/purchaseorderline/delete/";

}
