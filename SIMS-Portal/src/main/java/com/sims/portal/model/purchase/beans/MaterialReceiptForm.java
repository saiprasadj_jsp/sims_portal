package com.sims.portal.model.purchase.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;

@Entity
@Table(name = "material_receipt")
public class MaterialReceiptForm implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long MRId;
	private Long documentNo;
	private Date date;
	private String purchaseType;
	private String supplierName;
	private String department;	
	private String warehouse;
	private String DCNo;
	private Date DCDate;
	private String invoiceNumber;
	private Date invoiceDate;
	private String LRNumber;
	private String LRDate;
	private String vehicleNumber;
	private String transporterName;
	private String freight;
	private String paymentTerms;
	private String deliveryTerms;
	private String narration;
		
	private List<MaterialReceiptLine> materialReceiptLineList;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getMRId() {
		return MRId;
	}

	public void setMRId(Long MRId) {
		this.MRId = MRId;
	}

	@Column(name = "DOCUMNET_NO", nullable = false, length=300)
	public Long getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(Long documentNo) {
		this.documentNo = documentNo;
	}

	@Column(name = "DOCUMNET_DATE", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name="PURCHASE_TYPE",  nullable = false, length=300)
	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	@Column(name="DEPARTMENT",  nullable = false, length=300)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Column(name="SUPPLIER_NAME",  nullable = false, length=300)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@Column(name="WAREHOUSE",  nullable = false, length=300)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column(name="DC_NO",  nullable = false, length=300)
	public String getDCNo() {
		return DCNo;
	}

	public void setDCNo(String dCNo) {
		DCNo = dCNo;
	}
	@Column(name="DC_DATE")
	public Date getDCDate() {
		return DCDate;
	}

	public void setDCDate(Date dCDate) {
		DCDate = dCDate;
	}

	@Column(name="INVOICE_NUMBER",  nullable = false, length=300)
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Column(name="INVOICE_DATE",  nullable = false)
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	@Column(name="LR_NUMBER",  nullable = false, length=300)
	public String getLRNumber() {
		return LRNumber;
	}

	public void setLRNumber(String lRNumber) {
		LRNumber = lRNumber;
	}

	@Column(name="LR_DATE",  nullable = false)
	public String getLRDate() {
		return LRDate;
	}

	public void setLRDate(String lRDate) {
		LRDate = lRDate;
	}

	@Column(name="VEHICLE_NUMBER",  nullable = false, length=300)
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	@Column(name="TRANSPORT_NAME",  nullable = false, length=300)
	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	@Column(name="FRIGHT",  nullable = false, length=300)
	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	@Column(name="PAYMENT_TERMS",  nullable = false, length=300)
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	@Column(name="DELIVERY_TERMS",  nullable = false, length=300)
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Column(name="NARRATION",  nullable = false, length=300)
	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "materialReceiptForm")
	public List<MaterialReceiptLine> getMaterialReceiptLineList() {
		return materialReceiptLineList;
	}
	public void setMaterialReceiptLineList(List<MaterialReceiptLine> materialReceiptLineList) {
		this.materialReceiptLineList = materialReceiptLineList;
	}

}
