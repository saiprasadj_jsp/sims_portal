package com.sims.portal.model.purchase.line.beans;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sims.portal.model.purchase.beans.PurchaseOrderForm;

@Entity
@Table(name = "purchase_order_line")
public class PurchaseOrderLine implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long POLId;	
	
	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
	private Double rate;
	private Double gross;
	private Double discount;
	private Double VAT;
	private Double CST;
	private String purchaseRequisitionNumber;
	private Date purchaseRequisitionDate;
	
	private PurchaseOrderForm purchaseOrderForm;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getPOLId() {
		return POLId;
	}
	public void setPOLId(Long POLId) {
		this.POLId = POLId;
	}
	
	@Column(name="PRODUCT_CODE",  nullable = true, length=300)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name="PRODUCT_NAME",  nullable = true, length=300)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name="UOM",  nullable = true, length=300)
	public String getUOM() {
		return UOM;
	}

	public void setUOM(String UOM) {
		this.UOM = UOM;
	}

	@Column(name="QUANTITY",  nullable = true, length=300)
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	@Column(name="AVILABLE_QUANTITY",  nullable = true, length=300)
	public String getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(String availableStock) {
		this.availableStock = availableStock;
	}
	@Column(name="REQUIRED_DATE")
	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	@Column(name="PRODUCT_PURPOSE",  nullable = true, length=300)
	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name="REMARKS",  nullable = true, length=300)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Column(name="RATE",  nullable = true, length=300)
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name="GROSS",  nullable = true, length=300)
	public Double getGross() {
		return gross;
	}

	public void setGross(Double gross) {
		this.gross = gross;
	}

	@Column(name="DISCOUNT",  nullable = true, length=300)
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getVAT() {
		return VAT;
	}
	@Column(name="VAT",  nullable = true, length=300)
	public void setVAT(Double VAT) {
		this.VAT = VAT;
	}

	@Column(name="CST",  nullable = true, length=300)
	public Double getCST() {
		return CST;
	}

	public void setCST(Double CST) {
		this.CST = CST;
	}

	@Column(name="PR_NUMBER",  nullable = true, length=300)
	public String getPurchaseRequisitionNumber() {
		return purchaseRequisitionNumber;
	}

	public void setPurchaseRequisitionNumber(String purchaseRequisitionNumber) {
		this.purchaseRequisitionNumber = purchaseRequisitionNumber;
	}

	@Column(name="PR_DATE",  nullable = true)
	public Date getPurchaseRequisitionDate() {
		return purchaseRequisitionDate;
	}

	public void setPurchaseRequisitionDate(Date purchaseRequisitionDate) {
		this.purchaseRequisitionDate = purchaseRequisitionDate;
	}
	
	@ManyToOne
	@JoinColumn(name = "POId")
	public PurchaseOrderForm getPurchaseOrderForm() {
		return purchaseOrderForm;
	}
	public void setPurchaseOrderForm(PurchaseOrderForm purchaseOrderForm) {
		this.purchaseOrderForm = purchaseOrderForm;
	}

}
