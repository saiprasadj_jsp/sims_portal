package com.sims.portal.model.purchase.line.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class PurchaseOrderLineFormDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long POLId;	

	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
	private Double rate;
	private Double gross;
	private Double discount;
	private Double VAT;
	private Double CST;
	private String purchaseRequisitionNumber;
	private Date purchaseRequisitionDate;
	
	private String updateURL;
	private String deleteURL;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getPOLId() {
		return POLId;
	}

	public void setPOLId(Long pOLId) {
		POLId = pOLId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(String availableStock) {
		this.availableStock = availableStock;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getGross() {
		return gross;
	}

	public void setGross(Double gross) {
		this.gross = gross;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getVAT() {
		return VAT;
	}

	public void setVAT(Double vAT) {
		VAT = vAT;
	}

	public Double getCST() {
		return CST;
	}

	public void setCST(Double cST) {
		CST = cST;
	}

	public String getPurchaseRequisitionNumber() {
		return purchaseRequisitionNumber;
	}

	public void setPurchaseRequisitionNumber(String purchaseRequisitionNumber) {
		this.purchaseRequisitionNumber = purchaseRequisitionNumber;
	}

	public Date getPurchaseRequisitionDate() {
		return purchaseRequisitionDate;
	}

	public void setPurchaseRequisitionDate(Date purchaseRequisitionDate) {
		this.purchaseRequisitionDate = purchaseRequisitionDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public PurchaseOrderLineFormDTO getPurchaseOrderLineFormDTO(
			PurchaseOrderLine PurchaseOrderLineForm) {

		this.setPOLId(PurchaseOrderLineForm.getPOLId());
		this.setProductCode(PurchaseOrderLineForm.getProductCode());
		this.setProductName(PurchaseOrderLineForm.getProductName());
		this.setQuantity(PurchaseOrderLineForm.getQuantity());
		this.setRemarks(PurchaseOrderLineForm.getRemarks());
		this.setUOM(PurchaseOrderLineForm.getUOM());

		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}
}
