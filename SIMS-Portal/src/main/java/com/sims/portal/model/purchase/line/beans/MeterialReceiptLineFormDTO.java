package com.sims.portal.model.purchase.line.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MeterialReceiptLineFormDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long MRLId;	
	private String productCode;
	private String productName;
	private String UOM;	
	private String remarks;
	private Double receivedQuantityNumber;
	private Double rejectedQuantityNumber;
	private Double acceptedQuantityNumber;
	private Double grossQuantity;
	private Double rate;
	private Double Discount;
	private Double VAT;
	private Double CST;
	private String PONumber;	
	private Date PODate;
	private Double freight;
	
	private String updateURL;
	private String deleteURL;

	

	
	public MeterialReceiptLineFormDTO getMeterialReceiptLineFormDTO(
			MaterialReceiptLine materialReceiptLineForm) {
		this.setMRLId(materialReceiptLineForm.getMRLId());
		this.setProductCode(materialReceiptLineForm.getProductCode());
		this.setProductName(materialReceiptLineForm.getProductName());
		this.setAcceptedQuantityNumber(materialReceiptLineForm.getAcceptedQuantityNumber());
		this.setGrossQuantity(materialReceiptLineForm.getGrossQuantity());
		this.setDiscount(materialReceiptLineForm.getDiscount());
		this.setReceivedQuantityNumber(materialReceiptLineForm.getReceivedQuantityNumber());
		this.setRejectedQuantityNumber(materialReceiptLineForm.getRejectedQuantityNumber());
		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}

	public Long getMRLId() {
		return MRLId;
	}

	public void setMRLId(Long mRLId) {
		MRLId = mRLId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getReceivedQuantityNumber() {
		return receivedQuantityNumber;
	}

	public void setReceivedQuantityNumber(Double receivedQuantityNumber) {
		this.receivedQuantityNumber = receivedQuantityNumber;
	}

	public Double getRejectedQuantityNumber() {
		return rejectedQuantityNumber;
	}

	public void setRejectedQuantityNumber(Double rejectedQuantityNumber) {
		this.rejectedQuantityNumber = rejectedQuantityNumber;
	}

	public Double getAcceptedQuantityNumber() {
		return acceptedQuantityNumber;
	}

	public void setAcceptedQuantityNumber(Double acceptedQuantityNumber) {
		this.acceptedQuantityNumber = acceptedQuantityNumber;
	}

	public Double getGrossQuantity() {
		return grossQuantity;
	}

	public void setGrossQuantity(Double grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getDiscount() {
		return Discount;
	}

	public void setDiscount(Double discount) {
		Discount = discount;
	}

	public Double getVAT() {
		return VAT;
	}

	public void setVAT(Double vAT) {
		VAT = vAT;
	}

	public Double getCST() {
		return CST;
	}

	public void setCST(Double cST) {
		CST = cST;
	}

	public String getPONumber() {
		return PONumber;
	}

	public void setPONumber(String pONumber) {
		PONumber = pONumber;
	}

	public Date getPODate() {
		return PODate;
	}

	public void setPODate(Date pODate) {
		PODate = pODate;
	}

	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}
}
