package com.sims.portal.model.purchase.line.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class PurchaseRequisitionLineFormDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long PRLId;	
	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
	
	private String updateURL;
	private String deleteURL;

	public Long getPRLId() {
		return PRLId;
	}
	public void setPRLId(Long PRLId) {
		PRLId =this.PRLId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	
	public String getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(String availableStock) {
		this.availableStock = availableStock;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	
	public PurchaseRequisitionLineFormDTO getPurchaseRequisitionLineFormDTO(
			PurchaseRequisitionLine purchaseRequisitionLineForm) {
		this.setPRLId(purchaseRequisitionLineForm.getPRLId());
		this.setProductCode(purchaseRequisitionLineForm.getProductCode());
		this.setProductName(purchaseRequisitionLineForm.getProductName());

		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}
}
