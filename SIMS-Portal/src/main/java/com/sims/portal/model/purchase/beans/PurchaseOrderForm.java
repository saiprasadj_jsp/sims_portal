package com.sims.portal.model.purchase.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;

@Entity
@Table(name = "purchase_order")
public class PurchaseOrderForm implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long POId;	
	private Long documentNo;
	private Date date;
	private String purchaseType;
	private String supplierName;
	private String department;	
	private String paymentTerms;
	private String deliveryTerms;
	private String narration;
		
	private Collection<PurchaseOrderLine> purchaseOrderLine;	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getPOId() {
		return POId;
	}

	public void setPOId(Long POId) {
		this.POId = POId;
	}

	@Column(name = "DOCUMNET_NO", nullable = false, length=300)
	public Long getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(Long documentNo) {
		this.documentNo = documentNo;
	}
	@Column(name = "DOCUMNET_DATE", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	@Column(name="PURCHASE_TYPE",  nullable = false, length=300)
	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	@Column(name="DEPARTMENT",  nullable = false, length=300)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Column(name="SUPPLIER_NAME",  nullable = false, length=300)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@Column(name="PAYMENT_TERMS",  nullable = false, length=300)
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	
	@Column(name="DELIVERY_TERMS",  nullable = false, length=300)
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Column(name="NARRATION",  nullable = false, length=300)
	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}
	
	//@OneToMany(cascade = CascadeType.ALL)
	//@JoinTable(name = "PURCHASE_ORDER_RELATION", joinColumns = {@JoinColumn(name = "PURCHASE_ORDER_ID")}, inverseJoinColumns = { @JoinColumn(name = "POL_ID") })
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseOrderForm")
	public Collection<PurchaseOrderLine> getPurchaseOrderLine() {
		return purchaseOrderLine;
	}
	public void setPurchaseOrderLine(Collection<PurchaseOrderLine> purchaseOrderLine) {
		this.purchaseOrderLine = purchaseOrderLine;
	}

}
