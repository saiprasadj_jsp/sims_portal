package com.sims.portal.model.purchase.line.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class DirectPurchaseLineFormDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private Long DPLId;
	
	private String productCode;
	private String productName;
	private String UOM;
	private Double receivedQuantity;
	private Double rejectedQuantity;
	private Double acceptedQuantity;
	private Double grossQuantity;
	private Double Discount;
	private Double VAT;
	private Double CST;
	private Double freightAmount;
	
	private String updateURL;
	private String deleteURL;

	

	
	public DirectPurchaseLineFormDTO getDirectPurchaseLineFormDTO(
			DirectPurchaseLine directPurchaseLineForm) {
		this.setDPLId(directPurchaseLineForm.getDPLId());
		this.setProductCode(directPurchaseLineForm.getProductCode());
		this.setProductName(directPurchaseLineForm.getProductName());
        this.setAcceptedQuantity(directPurchaseLineForm.getAcceptedQuantity());
        this.setUOM(directPurchaseLineForm.getUOM());
        this.setReceivedQuantity(directPurchaseLineForm.getReceivedQuantity());
        this.setRejectedQuantity(directPurchaseLineForm.getRejectedQuantity());
        this.setDiscount(directPurchaseLineForm.getDiscount());
        this.setVAT(directPurchaseLineForm.getVAT());
        this.setCST(directPurchaseLineForm.getCST());
        this.setFreightAmount(directPurchaseLineForm.getFreightAmount());
        this.setGrossQuantity(directPurchaseLineForm.getGrossQuantity());
        
		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}

	public Long getDPLId() {
		return DPLId;
	}

	public void setDPLId(Long dPLId) {
		DPLId = dPLId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Double receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public Double getRejectedQuantity() {
		return rejectedQuantity;
	}

	public void setRejectedQuantity(Double rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}

	public Double getAcceptedQuantity() {
		return acceptedQuantity;
	}

	public void setAcceptedQuantity(Double acceptedQuantity) {
		this.acceptedQuantity = acceptedQuantity;
	}

	public Double getGrossQuantity() {
		return grossQuantity;
	}

	public void setGrossQuantity(Double grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	public Double getDiscount() {
		return Discount;
	}

	public void setDiscount(Double discount) {
		Discount = discount;
	}

	public Double getVAT() {
		return VAT;
	}

	public void setVAT(Double vAT) {
		VAT = vAT;
	}

	public Double getCST() {
		return CST;
	}

	public void setCST(Double cST) {
		CST = cST;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

}
