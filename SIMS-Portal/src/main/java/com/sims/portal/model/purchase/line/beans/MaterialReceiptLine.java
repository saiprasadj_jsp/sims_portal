package com.sims.portal.model.purchase.line.beans;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sims.portal.model.purchase.beans.MaterialReceiptForm;

@Entity
@Table(name = "material_receipt_line")
public class MaterialReceiptLine implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long MRLId;	
	private String productCode;
	private String productName;
	private String UOM;	
	private String remarks;
	private Double receivedQuantityNumber;
	private Double rejectedQuantityNumber;
	private Double acceptedQuantityNumber;
	private Double grossQuantity;
	private Double rate;
	private Double Discount;
	private Double VAT;
	private Double CST;
	private String PONumber;	
	private Date PODate;
	private Double freight;

	private MaterialReceiptForm materialReceiptForm;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getMRLId() {
		return MRLId;
	}
	public void setMRLId(Long MRLId) {
		this.MRLId = MRLId;
	}
	

	@Column(name = "PRODUCT_CODE", nullable = false, length=300)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name = "PRODUCT_NAME", nullable = false, length=300)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "UOM", nullable = false, length=300)
	public String getUOM() {
		return UOM;
	}

	public void setUOM(String UOM) {
		this.UOM = UOM;
	}

	@Column(name = "REMARK", nullable = false, length=300)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "RECEIVED_QUANTITY_NUMBER", nullable = false, length=300)
	public Double getReceivedQuantityNumber() {
		return receivedQuantityNumber;
	}

	public void setReceivedQuantityNumber(Double receivedQuantityNumber) {
		this.receivedQuantityNumber = receivedQuantityNumber;
	}

	@Column(name = "REJECTED_QUANTITY_NUMBER", nullable = false, length=300)
	public Double getRejectedQuantityNumber() {
		return rejectedQuantityNumber;
	}

	public void setRejectedQuantityNumber(Double rejectedQuantityNumber) {
		this.rejectedQuantityNumber = rejectedQuantityNumber;
	}
	@Column(name = "ACCEPTED_QUANTITY_NUMBER", nullable = false, length=300)
	public Double getAcceptedQuantityNumber() {
		return acceptedQuantityNumber;
	}

	public void setAcceptedQuantityNumber(Double acceptedQuantityNumber) {
		this.acceptedQuantityNumber = acceptedQuantityNumber;
	}

	@Column(name = "GROSS_QUANTITY", nullable = false, length=300)
	public Double getGrossQuantity() {
		return grossQuantity;
	}

	public void setGrossQuantity(Double grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	@Column(name = "RATE", nullable = false, length=300)
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "DISCOUNT", nullable = false, length=300)
	public Double getDiscount() {
		return Discount;
	}

	public void setDiscount(Double discount) {
		Discount = discount;
	}

	@Column(name = "VAT", nullable = false, length=300)
	public Double getVAT() {
		return VAT;
	}

	public void setVAT(Double VAT) {
		this.VAT = VAT;
	}

	@Column(name = "CST", nullable = false, length=300)
	public Double getCST() {
		return CST;
	}

	public void setCST(Double CST) {
		this.CST = CST;
	}

	@Column(name = "PO_NUMBER", nullable = false, length=300)
	public String getPONumber() {
		return PONumber;
	}

	public void setPONumber(String PONumber) {
		this.PONumber = PONumber;
	}
	@Column(name = "PO_DATE", nullable = false)
	public Date getPODate() {
		return PODate;
	}

	public void setPODate(Date PODate) {
		this.PODate = PODate;
	}

	@Column(name = "FREIGHT", nullable = false)
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}
	
	@ManyToOne
	@JoinColumn(name = "MRId")
	public MaterialReceiptForm getMaterialReceiptForm() {
		return materialReceiptForm;
	}
	
	public void setMaterialReceiptForm(MaterialReceiptForm materialReceiptForm) {
		this.materialReceiptForm = materialReceiptForm;
	}

}
