package com.sims.portal.model.purchase.line.beans;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sims.portal.model.purchase.beans.DirectPurchasesForm;

@Entity
@Table(name = "direct_purchase_line")
public class DirectPurchaseLine implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long DPLId;
	
	private String productCode;
	private String productName;
	private String UOM;
	private Double receivedQuantity;
	private Double rejectedQuantity;
	private Double acceptedQuantity;
	private Double grossQuantity;
	private Double Discount;
	private Double VAT;
	private Double CST;
	private Double freightAmount;
	
	private DirectPurchasesForm directPurchasesForm;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getDPLId() {
		return DPLId;
	}

	public void setDPLId(Long DPLId) {
		this.DPLId = DPLId;
	}

	
	@Column(name="PRODUCT_CODE",  nullable = false, length=300)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name="PRODUCT_NAME",  nullable = false, length=300)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name="UOM",  nullable = false, length=300)
	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}
	@Column(name="RECEIVED_QUANTITY",  nullable = false, length=300)
	public Double getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Double receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	@Column(name="REJECTED_QUANTITY",  nullable = false, length=300)
	public Double getRejectedQuantity() {
		return rejectedQuantity;
	}

	public void setRejectedQuantity(Double rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}
	@Column(name="ACCEPTED_QUANTITY",  nullable = false, length=300)
	public Double getAcceptedQuantity() {
		return acceptedQuantity;
	}

	public void setAcceptedQuantity(Double acceptedQuantity) {
		this.acceptedQuantity = acceptedQuantity;
	}
	@Column(name="GROSS_QUANTITY",  nullable = false, length=300)
	public Double getGrossQuantity() {
		return grossQuantity;
	}

	public void setGrossQuantity(Double grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	@Column(name="DISCOUNT",  nullable = false, length=300)
	public Double getDiscount() {
		return Discount;
	}

	public void setDiscount(Double discount) {
		Discount = discount;
	}
	
	@Column(name="VAT",  nullable = false, length=300)
	public Double getVAT() {
		return VAT;
	}
	public void setVAT(Double vAT) {
		VAT = vAT;
	}
	
	@Column(name="CST",  nullable = false, length=300)
	public Double getCST() {
		return CST;
	}

	public void setCST(Double cST) {
		CST = cST;
	}

	@Column(name="FRIGHT_AMOUNT",  nullable = false, length=300)
	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}
	
	@ManyToOne
	@JoinColumn(name = "DPId")
	
	public DirectPurchasesForm getDirectPurchasesForm() {
		return directPurchasesForm;
	}
	
	public void setDirectPurchasesForm(DirectPurchasesForm directPurchasesForm) {
		this.directPurchasesForm = directPurchasesForm;
	}


}
