package com.sims.portal.model.purchase.line.beans;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;

@Entity
@Table(name = "purchase_requisition_line")
public class PurchaseRequisitionLine implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long PRLId;	
	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
	private PurchaseRequisitionForm purchaseRequisitionForm;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getPRLId() {
		return PRLId;
	}
	public void setPRLId(Long PRLId) {
		this.PRLId =PRLId;
	}

	@Column(name="PRODUCT_CODE",  nullable = true, length=300)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name="PRODUCT_NAME",  nullable = true, length=300)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name="UOM",  nullable = true, length=300)
	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	@Column(name="QUANTITY",  nullable = true, length=300)
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="AVILABLE_QUANTITY",  nullable = true, length=300)
	public String getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(String availableStock) {
		this.availableStock = availableStock;
	}

	@Column(name="REQUIRED_DATE", nullable = true)
	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	@Column(name="PURCHASE_PURPOSE",  nullable = true, length=300)
	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name="REMARKS",  nullable = true, length=300)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne
	@JoinColumn(name = "PRId")
	public PurchaseRequisitionForm getPurchaseRequisitionForm() {
		return purchaseRequisitionForm;
	}
	public void setPurchaseRequisitionForm(PurchaseRequisitionForm purchaseRequisitionForm) {
		this.purchaseRequisitionForm = purchaseRequisitionForm;
	}
}
