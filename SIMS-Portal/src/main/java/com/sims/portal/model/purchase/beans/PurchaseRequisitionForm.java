package com.sims.portal.model.purchase.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;

@Entity
@Table(name = "purchase_requisition")
public class PurchaseRequisitionForm implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long PRId;
	private Long documentNo;
	private Date date;
	private String purchaseType;
	private String department;
	private String employeeName;
	private String narration;
	private String sectionName;
	
	private Collection<PurchaseRequisitionLine> purchaseRequisitionLine;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getPRId() {
		return PRId;
	}

	public void setPRId(Long PRId) {
		this.PRId = PRId;
	}


	@Column(name = "DOCUMNET_NO", nullable = false, length=300)
	public Long getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(Long documentNo) {
		this.documentNo = documentNo;
	}

	@Column(name = "DOCUMNET_DATE", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name="PURCHASE_TYPE",  nullable = false, length=300)
	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	@Column(name="DEPARTMENT",  nullable = false, length=300)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Column(name="EMPLOYEE_NAME",  nullable = false, length=300)
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	@Column(name="NARRATION",  nullable = false, length=300)
	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@Column(name="SACTION_NAME",  nullable = false, length=300)
	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseRequisitionForm")
	public Collection<PurchaseRequisitionLine> getPurchaseRequisitionLine() {
		return purchaseRequisitionLine;
	}

	public void setPurchaseRequisitionLine(Collection<PurchaseRequisitionLine> purchaseRequisitionLine) {
		this.purchaseRequisitionLine = purchaseRequisitionLine;
	}
}
