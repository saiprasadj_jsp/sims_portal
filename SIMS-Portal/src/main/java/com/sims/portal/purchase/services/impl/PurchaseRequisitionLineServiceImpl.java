package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.purchase.dao.PurchaseRequisitionLineDao;
import com.sims.portal.purchase.services.PurchaseRequisitionLineService;

@Service
public class PurchaseRequisitionLineServiceImpl implements PurchaseRequisitionLineService {

	@Autowired
	private PurchaseRequisitionLineDao purchaseRequisitionLineDaoObj;

	@Override
	public PurchaseRequisitionLine savePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm) {

		purchaseRequisitionLineDaoObj.savePurchaseRequisitionLineForm(PurchaseRequisitionLineForm);
		return PurchaseRequisitionLineForm;
	}

	@Override
	public List<PurchaseRequisitionLine> findPurchaseRequisitionLineDetails() {

		List<PurchaseRequisitionLine> purchaseRequisitionLineFormListData = purchaseRequisitionLineDaoObj.findPurchaseRequisitionLineForm();

		return purchaseRequisitionLineFormListData;
	}

	@Override
	public PurchaseRequisitionLine findPurchaseRequisitionLineByID(Long id){

		return purchaseRequisitionLineDaoObj.findPurchaseRequisitionLineFormByID(id);
	}
	
	@Override
	public PurchaseRequisitionLine updatePurchaseRequisitionLine(PurchaseRequisitionLine purchaseRequisitionLineForm) {

		purchaseRequisitionLineDaoObj.updatePurchaseRequisitionLineForm(purchaseRequisitionLineForm);

		return purchaseRequisitionLineForm;
	}

	@Override
	public Boolean deletePurchaseRequisitionLine(PurchaseRequisitionLine purchaseRequisitionLineForm) {

		return purchaseRequisitionLineDaoObj.deletePurchaseRequisitionLineForm(purchaseRequisitionLineForm);
	}
}
