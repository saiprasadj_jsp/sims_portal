package com.sims.portal.purchase.controllers;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.model.purchase.beans.PurchaseOrderForm;
import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.model.purchase.line.beans.PurchaseOrderLineFormDTO;
import com.sims.portal.purchase.services.PurchaseOrderLineService;
import com.sims.portal.purchase.services.PurchaseOrderService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_MAIN_URL)
public class PurchaseOrderLineController {

	@Autowired
	private PurchaseOrderService purchaseOrderService;

	@Autowired
	private PurchaseOrderLineService purchaseOrderLineService;

	
	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;
	
	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLinePurchaseOrderLineForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLinePurchaseOrderLineForm(modelAndView);
		modelAndView.addObject("purchaseOrderLineForm", new PurchaseOrderLine());
		
		modelAndView.addObject("purchaseOrderLineURL",
				TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{POId}", method = RequestMethod.POST)
	public void savePurchaseOrderLine(@PathVariable(name = "POId") Long POId,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity,
			@RequestParam("availableStock") Double availableStock,
			@RequestParam("requiredDate") String requiredDate,
			@RequestParam("purpose") String purpose,
			@RequestParam("rate") Double rate,
			@RequestParam("gross") Double gross,
			@RequestParam("discount") Double discount,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("purchaseRequisitionNumber") Double purchaseRequisitionNumber,
			@RequestParam("purchaseRequisitionDate") String purchaseRequisitionDate	,
			@RequestParam("remarks") String remarks
			) {	
		
			PurchaseOrderForm purchaseOrderForm = purchaseOrderService.findPurchaseOrderDetailsById(POId);

		System.out.println(
				"List Size ======= " + purchaseOrderForm.getPurchaseOrderLine().size());
	
		PurchaseOrderLine purchaseOrderLineForm = new PurchaseOrderLine();
		purchaseOrderLineForm.setPurchaseOrderForm(purchaseOrderForm);
		
		setPurchaseOrderLineData(productCode, productName,UOM, quantity,availableStock, requiredDate,purpose,rate,gross,
				discount,VAT,CST,purchaseRequisitionNumber,purchaseRequisitionDate,remarks,purchaseOrderLineForm);

	
		ModelAndView modelAndView = new ModelAndView();
		purchaseOrderLineService.savePurchaseOrderLineForm(purchaseOrderLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		findAllLinePurchaseOrderLineForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

	}

	private void setPurchaseOrderLineData(String productCode, String productName,
											String UOM, Double quantity, 
											Double availableStock,String requiredDate,
											String purpose,Double rate,
											Double gross,Double discount,
											Double VAT,Double CST,
											Double purchaseRequisitionNumber,String purchaseRequisitionDate,
											String remarks,PurchaseOrderLine purchaseOrderLineForm) {
		
		
		purchaseOrderLineForm.setProductCode(productCode);
		purchaseOrderLineForm.setProductName(productName);
		purchaseOrderLineForm.setQuantity(quantity);
		purchaseOrderLineForm.setUOM(UOM);
		purchaseOrderLineForm.setAvailableStock(availableStock.toString());
		purchaseOrderLineForm.setRequiredDate(requiredDate);
		purchaseOrderLineForm.setPurpose(purpose);
		purchaseOrderLineForm.setRate(rate);
		purchaseOrderLineForm.setGross(gross);
		purchaseOrderLineForm.setDiscount(discount);
		purchaseOrderLineForm.setVAT(VAT);
		purchaseOrderLineForm.setCST(CST);
		purchaseOrderLineForm.setPurchaseRequisitionNumber(purchaseRequisitionNumber.toString());
		purchaseOrderLineForm.setPurchaseRequisitionDate(new Date(purchaseRequisitionDate));
		purchaseOrderLineForm.setRemarks(remarks);
	}

	public ModelAndView findAllLinePurchaseOrderLineForm(ModelAndView modelAndView) {

		List<PurchaseOrderLine> purchaseOrderLineData = purchaseOrderLineService.findPurchaseOrderLineDetails();
		modelAndView.addObject("purchaseOrderLineListData", purchaseOrderLineData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForPurchaseOrderLine(ModelAndView modelAndView) {

		modelAndView.addObject("purchaseOrderLineForm", new PurchaseOrderLine());
		modelAndView.addObject("purchaseOrderLineURL",
				TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public PurchaseOrderLineFormDTO findPurchaseOrderLineByID(
			@PathVariable(name = "id") Long id) {

		System.out.println("PurchaseOrderLine ID Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseOrderLine purchaseOrderLineForm = purchaseOrderLineService.findPurchaseOrderLineByID(id);
		
		modelAndView.addObject("purchaseOrderLineForm", purchaseOrderLineForm);
		// findLineStockingOfShrimpsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		PurchaseOrderLineFormDTO dto = new PurchaseOrderLineFormDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_UPDATE_URL
				+ purchaseOrderLineForm.getPOLId());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_DELETE_URL
				+ purchaseOrderLineForm.getPOLId());
		return dto.getPurchaseOrderLineFormDTO(purchaseOrderLineForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLinePurchaseOrderLineForm(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity,
			@RequestParam("availableStock") Double availableStock,
			@RequestParam("requiredDate") String requiredDate,
			@RequestParam("purpose") String purpose,
			@RequestParam("rate") Double rate,
			@RequestParam("gross") Double gross,
			@RequestParam("discount") Double discount,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("purchaseRequisitionNumber") Double purchaseRequisitionNumber,
			@RequestParam("purchaseRequisitionDate") String purchaseRequisitionDate	,
			@RequestParam("remarks") String remarks) {

		System.out.println("UPDATING Line ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);

		PurchaseOrderLine purchaseOrderLineForm = purchaseOrderLineService.findPurchaseOrderLineByID(id);
				
		
		setPurchaseOrderLineData(productCode, productName,UOM, quantity,availableStock, requiredDate,purpose,rate,gross,
				discount,VAT,CST,purchaseRequisitionNumber,purchaseRequisitionDate,remarks,purchaseOrderLineForm);


		purchaseOrderLineService.updatePurchaseOrderLine(purchaseOrderLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deleteLinePurchaseOrderLineByID(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("DELETING ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseOrderLine purchaseOrderLineForm = purchaseOrderLineService.findPurchaseOrderLineByID(id);
				
		purchaseOrderLineService.deletePurchaseOrderLine(purchaseOrderLineForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findLinePurchaseOrderLineDetails(ModelAndView modelAndView) {

		List<PurchaseOrderLine> linePurchaseOrderLineList = purchaseOrderLineService.findPurchaseOrderLineDetails();
			
		modelAndView.addObject("linePurchaseOrderLineListData", linePurchaseOrderLineList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}
	
	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}