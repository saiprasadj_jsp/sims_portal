package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;

public interface DirectPurchasesLineDao {

	public DirectPurchaseLine saveDirectPurchasesLineForm(
			DirectPurchaseLine PurchaseRequisitionLineForm);

	public List<DirectPurchaseLine> findDirectPurchasesLineForm();

	public DirectPurchaseLine findDirectPurchasesLineFormByID(Long id);

	public DirectPurchaseLine updateDirectPurchasesLineForm(
			DirectPurchaseLine PurchaseRequisitionLineForm);

	public Boolean deleteDirectPurchasesLineForm(
			DirectPurchaseLine PurchaseRequisitionLineForm);

}
