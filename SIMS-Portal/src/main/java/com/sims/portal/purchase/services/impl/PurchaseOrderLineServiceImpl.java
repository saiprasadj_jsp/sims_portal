package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.purchase.dao.PurchaseOrderLineDao;
import com.sims.portal.purchase.services.PurchaseOrderLineService;

@Service
public class PurchaseOrderLineServiceImpl implements PurchaseOrderLineService {

	@Autowired
	private PurchaseOrderLineDao purchaseOrderLineDaoObj;

	@Override
	public PurchaseOrderLine savePurchaseOrderLineForm(PurchaseOrderLine purchaseOrderLineForm) {

		purchaseOrderLineDaoObj.savePurchaseOrderLineForm(purchaseOrderLineForm);
		return purchaseOrderLineForm;
	}

	@Override
	public List<PurchaseOrderLine> findPurchaseOrderLineDetails() {

		List<PurchaseOrderLine> purchaseOrderLineFormListData = purchaseOrderLineDaoObj.findPurchaseOrderLineForm();

		return purchaseOrderLineFormListData;
	}

	@Override
	public PurchaseOrderLine findPurchaseOrderLineByID(Long id){

		return purchaseOrderLineDaoObj.findPurchaseOrderLineFormByID(id);
	}
	
	@Override
	public PurchaseOrderLine updatePurchaseOrderLine(PurchaseOrderLine purchaseOrderLineForm) {

		purchaseOrderLineDaoObj.updatePurchaseOrderLineForm(purchaseOrderLineForm);

		return purchaseOrderLineForm;
	}

	@Override
	public Boolean deletePurchaseOrderLine(PurchaseOrderLine purchaseOrderLineForm) {

		return purchaseOrderLineDaoObj.deletePurchaseOrderLineForm(purchaseOrderLineForm);
	}
}
