package com.sims.portal.purchase.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLineFormDTO;
import com.sims.portal.purchase.services.PurchaseRequisitionLineService;
import com.sims.portal.purchase.services.PurchaseRequisitionService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_MAIN_URL)
public class PurchaseRequisitionLineController {

	@Autowired
	private PurchaseRequisitionService purchaseRequisitionService;
	
	@Autowired
	private PurchaseRequisitionLineService purchaseRequisitionLineService;

	
	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;
	
	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLinePurchaseOrderLineForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLinePurchaseRequisitionForm(modelAndView);
		modelAndView.addObject("purchaseRequisitionLineForm", new PurchaseRequisitionLine());
		
		modelAndView.addObject("purchaseRequisitionLineURL",
				TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
	public void savepurchaseRequisitionLine(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity,
			@RequestParam("availableStock") Double availableStock,
			@RequestParam("requiredDate") String requiredDate,
			@RequestParam("purpose") String purpose,
			@RequestParam("remarks") String remarks
			) {	
		
		PurchaseRequisitionForm purchaseRequisitionForm = purchaseRequisitionService.findPurchaseRequisitionDetailsById(id);

		System.out.println(
				"List Size ======= " + purchaseRequisitionForm.getPurchaseRequisitionLine().size());
	
		PurchaseRequisitionLine purchaseRequisitionLineForm = new PurchaseRequisitionLine();
		purchaseRequisitionLineForm.setPurchaseRequisitionForm(purchaseRequisitionForm);
		
		setpurchaseRequisitionLineData(productCode, productName,UOM, quantity,availableStock,requiredDate,purpose,remarks,purchaseRequisitionLineForm);
	
		ModelAndView modelAndView = new ModelAndView();
		purchaseRequisitionLineService.savePurchaseRequisitionLineForm(purchaseRequisitionLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		findAllLinePurchaseRequisitionForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

	}

	private void setpurchaseRequisitionLineData(String productCode, String productName,
											String UOM, Double quantity, 
											Double availableStock,String requiredDate,
											String purpose,
											String remarks,PurchaseRequisitionLine purchaseRequisitionLineForm) {
		
		
		purchaseRequisitionLineForm.setProductCode(productCode);
		purchaseRequisitionLineForm.setProductName(productName);
		purchaseRequisitionLineForm.setQuantity(quantity);
		purchaseRequisitionLineForm.setUOM(UOM);
		purchaseRequisitionLineForm.setAvailableStock(availableStock.toString());
		purchaseRequisitionLineForm.setRequiredDate(requiredDate);
		purchaseRequisitionLineForm.setPurpose(purpose);
		purchaseRequisitionLineForm.setRemarks(remarks);
	}

	public ModelAndView findAllLinePurchaseRequisitionForm(ModelAndView modelAndView) {

		List<PurchaseRequisitionLine> purchaseRequisitionLineData = purchaseRequisitionLineService.findPurchaseRequisitionLineDetails();
		modelAndView.addObject("purchaseRequisitionLineListData", purchaseRequisitionLineData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForPurchaseOrderLine(ModelAndView modelAndView) {

		modelAndView.addObject("purchaseRequisitionLineForm", new PurchaseRequisitionLine());
		modelAndView.addObject("purchaseRequisitionLineURL",
				TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public PurchaseRequisitionLineFormDTO findRequisitionLineByID(
			@PathVariable(name = "id") Long id) {

		System.out.println("PurchaseRequisitionLine ID Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseRequisitionLine purchaseRequisitionLineForm = purchaseRequisitionLineService.findPurchaseRequisitionLineByID(id);
		
		modelAndView.addObject("purchaseRequisitionLineForm", purchaseRequisitionLineForm);
		// findLineStockingOfShrimpsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		PurchaseRequisitionLineFormDTO dto = new PurchaseRequisitionLineFormDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_UPDATE_URL
				+ purchaseRequisitionLineForm.getPRLId());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_DELETE_URL
				+ purchaseRequisitionLineForm.getPRLId());
		return dto.getPurchaseRequisitionLineFormDTO(purchaseRequisitionLineForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLinePurchaseOrderLineForm(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity,
			@RequestParam("availableStock") Double availableStock,
			@RequestParam("requiredDate") String requiredDate,
			@RequestParam("purpose") String purpose,
			@RequestParam("remarks") String remarks) {

		System.out.println("UPDATING Line ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);

		PurchaseRequisitionLine purchaseRequisitionLineForm = purchaseRequisitionLineService.findPurchaseRequisitionLineByID(id);
				
		
		setpurchaseRequisitionLineData(productCode, productName,UOM, quantity,availableStock, requiredDate,purpose,remarks,purchaseRequisitionLineForm);


		purchaseRequisitionLineService.updatePurchaseRequisitionLine(purchaseRequisitionLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deletePurchaseRequisitionLineByID(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("DELETING ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseRequisitionLine purchaseRequisitionLineForm = purchaseRequisitionLineService.findPurchaseRequisitionLineByID(id);
				
		purchaseRequisitionLineService.deletePurchaseRequisitionLine(purchaseRequisitionLineForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findPurchaseRequisitionLineDetails(ModelAndView modelAndView) {

		List<PurchaseRequisitionLine> linePurchaseRequisitionLineList = purchaseRequisitionLineService.findPurchaseRequisitionLineDetails();
			
		modelAndView.addObject("linePurchaseRequisitionLineListData", linePurchaseRequisitionLineList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}
	
	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}