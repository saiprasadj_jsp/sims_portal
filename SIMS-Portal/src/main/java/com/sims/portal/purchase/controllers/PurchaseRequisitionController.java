package com.sims.portal.purchase.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;
import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.purchase.services.PurchaseRequisitionService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RequestMapping(value = "/admin/purchase/purchaserequisition")
@Controller
public class PurchaseRequisitionController {

	@Autowired
	private PurchaseRequisitionService purchaseRequisitionService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showPurchaseRequisitionPage() {

		ModelAndView modelAndView = new ModelAndView();
		setDefaultDataForPurchaseRequisitionPage(modelAndView);
		
		modelAndView.addObject("PurchaseRequisitionLineForm", new PurchaseRequisitionLine());
		modelAndView.addObject("purchaseRequisitionLineSaveURL",
				TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SAVE_URL);
		// LIST PAGE DATA
		findPurchaseRequisitionDetails(modelAndView);

		return modelAndView;
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView savePurchaseRequisition(@ModelAttribute("purchaseRequisitionForm") PurchaseRequisitionForm purchaseRequisitionForm) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("PurchaseRequisitionForm " + purchaseRequisitionForm.getDocumentNo());
		purchaseRequisitionService.savePurchaseRequisition(purchaseRequisitionForm);
		setDefaultDataForPurchaseRequisitionPage(modelAndView);
		findPurchaseRequisitionDetails(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findPurchaseRequisitionDetails(ModelAndView modelAndView) {

		List<PurchaseRequisitionForm> purchaseRequisitionFormList = purchaseRequisitionService.findPurchaseRequisitionDetails();
		modelAndView.addObject("purchaseRequisitionListData", purchaseRequisitionFormList);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findPurchaseRequisitionDetailsByCode(@PathVariable(name = "id") Long id) {

		System.out.println("Code Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseRequisitionForm purchaseRequisitionForm = purchaseRequisitionService.findPurchaseRequisitionDetailsById(id);
		modelAndView.addObject("purchaseRequisitionForm", purchaseRequisitionForm);
		findPurchaseRequisitionDetails(modelAndView);
		modelAndView.setViewName(MastersPageConstants.PURCHASE_REQUISITION_MAIN_PAGE);
		modelAndView.addObject("tabToShow", "details");
		
		Collection<PurchaseRequisitionLine> listOfPurchaseRequisitionLines = purchaseRequisitionForm.getPurchaseRequisitionLine();
		modelAndView.addObject("listOfLinePurchaseRequisitionLineObj", listOfPurchaseRequisitionLines);
		
		
		modelAndView.addObject("purchaseRequisitionURL", "purchase/purchaserequisition/update");

		return modelAndView;
	}
	
	@RequestMapping(value = "/update/{PRId}", method = RequestMethod.POST)
	public ModelAndView updatePurchaseRequisition(@PathVariable(name="PRId")Long PRId,@ModelAttribute("purchaseRequisitionForm") PurchaseRequisitionForm purchaseRequisitionForm) {
		
		System.out.println("UPDATING ID =========== "+PRId);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + PRId);
		purchaseRequisitionForm.setPRId(PRId);
		purchaseRequisitionService.updatePurchaseRequisition(purchaseRequisitionForm);
		setDefaultDataForPurchaseRequisitionPage(modelAndView);
		findPurchaseRequisitionDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{PRId}", method = RequestMethod.GET)
	public ModelAndView deletePurchaseRequisitionDetailsByCode(@PathVariable(name = "PRId") Long PRId,@ModelAttribute("purchaseRequisitionForm") PurchaseRequisitionForm purchaseRequisitionForm) {

		System.out.println("DELETING ID =========== "+PRId);
		ModelAndView modelAndView = new ModelAndView();
		purchaseRequisitionForm.setPRId(PRId);
		purchaseRequisitionService.deletePurchaseRequisition(purchaseRequisitionForm);
		setDefaultDataForPurchaseRequisitionPage(modelAndView);
		findPurchaseRequisitionDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}
	
	private ModelAndView setDefaultDataForPurchaseRequisitionPage(ModelAndView modelAndView) {

		modelAndView.addObject("purchaseRequisitionForm", new PurchaseRequisitionForm());

		HashMap<String, String> typesOfCustomersMap = new HashMap<String, String>();

		typesOfCustomersMap.put("customertype1", "Customer Type 1");
		typesOfCustomersMap.put("customertype2", "Customer Type 2");
		typesOfCustomersMap.put("customertype3", "Customer Type 3");
		typesOfCustomersMap.put("customertype4", "Customer Type 4");
		typesOfCustomersMap.put("customertype5", "Customer Type 5");

		modelAndView.addObject("typesOfCustomersMap", typesOfCustomersMap);
		modelAndView.addObject("purchaseRequisitionURL", "purchase/purchaserequisition/save");
		modelAndView.setViewName(MastersPageConstants.PURCHASE_REQUISITION_MAIN_PAGE);
		return modelAndView;
	}

}
