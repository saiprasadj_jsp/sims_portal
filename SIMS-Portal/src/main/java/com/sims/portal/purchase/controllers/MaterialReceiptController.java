package com.sims.portal.purchase.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.model.purchase.beans.MaterialReceiptForm;
import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;
import com.sims.portal.purchase.services.MaterialReceiptService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RequestMapping(value = "/admin/meterial/receipt")
@Controller
public class MaterialReceiptController {

	@Autowired
	private MaterialReceiptService materialReceiptService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showMaterialReceiptPage() {

		ModelAndView modelAndView = new ModelAndView();
		setDefaultDataForMaterialReceiptPage(modelAndView);
		
		modelAndView.addObject("materialReceiptLineForm", new MaterialReceiptLine());
		modelAndView.addObject("materialReceiptLineSaveURL",
				TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SAVE_URL);

		// LIST PAGE DATA
		findMaterialReceiptDetails(modelAndView);

		return modelAndView;
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveMaterialReceipt(@ModelAttribute("materialReceiptForm") MaterialReceiptForm materialReceiptForm) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("MaterialReceiptForm " + materialReceiptForm.getDocumentNo());
		materialReceiptService.saveMaterialReceipt(materialReceiptForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findMaterialReceiptDetails(ModelAndView modelAndView) {

		List<MaterialReceiptForm> materialReceiptFormList = materialReceiptService.findMaterialReceiptDetails();
		modelAndView.addObject("materialReceiptListData", materialReceiptFormList);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findMaterialReceiptDetailsByCode(@PathVariable(name = "id") long id) {

		System.out.println("Code Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		MaterialReceiptForm materialReceiptForm = materialReceiptService.findMaterialReceiptDetailsById(id);
		modelAndView.addObject("materialReceiptForm", materialReceiptForm);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.setViewName(MastersPageConstants.MATETIAL_RECEIPT_MAIN_PAGE);
		modelAndView.addObject("tabToShow", "details");
		
	
		Collection<MaterialReceiptLine> listOfMaterialReceiptLines =materialReceiptForm.getMaterialReceiptLineList();
		modelAndView.addObject("listOfLinelistOfMaterialReceiptLineObj", listOfMaterialReceiptLines);				
		modelAndView.addObject("materialReceiptURL", "meterial/receipt/update");

		return modelAndView;
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateMaterialReceipt(@PathVariable(name="id")Long id,@ModelAttribute("materialReceiptForm") MaterialReceiptForm materialReceiptForm) {
		
		System.out.println("UPDATING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);
		materialReceiptForm.setMRId(id);
		materialReceiptService.updateMaterialReceipt(materialReceiptForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteMaterialReceiptDetailsByCode(@PathVariable(name = "id") Long id,@ModelAttribute("materialReceiptForm") MaterialReceiptForm materialReceiptForm) {

		System.out.println("DELETING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		materialReceiptForm.setMRId(id);
		materialReceiptService.deleteMaterialReceipt(materialReceiptForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}
	
	private ModelAndView setDefaultDataForMaterialReceiptPage(ModelAndView modelAndView) {

		modelAndView.addObject("materialReceiptForm", new MaterialReceiptForm());

		HashMap<String, String> typesOfCustomersMap = new HashMap<String, String>();

		typesOfCustomersMap.put("customertype1", "Customer Type 1");
		typesOfCustomersMap.put("customertype2", "Customer Type 2");
		typesOfCustomersMap.put("customertype3", "Customer Type 3");
		typesOfCustomersMap.put("customertype4", "Customer Type 4");
		typesOfCustomersMap.put("customertype5", "Customer Type 5");

		modelAndView.addObject("typesOfCustomersMap", typesOfCustomersMap);
		modelAndView.addObject("materialReceiptURL", ""
				+ "meterial/receipt/save");
		modelAndView.setViewName(MastersPageConstants.MATETIAL_RECEIPT_MAIN_PAGE);
		return modelAndView;
	}

}
