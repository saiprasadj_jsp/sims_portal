package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.beans.DirectPurchasesForm;
import com.sims.portal.purchase.dao.DirectPurchaseDao;
import com.sims.portal.purchase.services.DirectPurchaseService;

@Service
public class DirectPurchaseServiceImpl implements DirectPurchaseService {

	@Autowired
	private DirectPurchaseDao directPurchaseDao;

	public DirectPurchasesForm saveDirectPurchase(DirectPurchasesForm directPurchasesForm) {

		directPurchaseDao.saveDirectPurchase(directPurchasesForm);

		return directPurchasesForm;

	}

	@Override
	public DirectPurchasesForm updateDirectPurchase(DirectPurchasesForm directPurchasesForm) {

		directPurchaseDao.updateDirectPurchase(directPurchasesForm);

		return directPurchasesForm;
	}

	@Override
	public Boolean deleteDirectPurchase(DirectPurchasesForm directPurchasesForm) {

		return directPurchaseDao.deleteDirectPurchase(directPurchasesForm);
	}

	@Override
	public List<DirectPurchasesForm> findDirectPurchaseDetails() {

		return directPurchaseDao.findDirectPurchaseDetails();
	}

	@Override
	public DirectPurchasesForm findDirectPurchaseDetailsById(long id) {

		return directPurchaseDao.findDirectPurchaseDetailsById(id);
	}
}
