package com.sims.portal.purchase.services;

import java.util.List;

import com.sims.portal.model.purchase.beans.DirectPurchasesForm;

public interface DirectPurchaseService{

	public DirectPurchasesForm saveDirectPurchase(DirectPurchasesForm materialReceiptForm);
	
	public DirectPurchasesForm updateDirectPurchase(DirectPurchasesForm materialReceiptForm);
	
	public Boolean deleteDirectPurchase(DirectPurchasesForm materialReceiptForm);
	
	public List<DirectPurchasesForm> findDirectPurchaseDetails();

	public DirectPurchasesForm findDirectPurchaseDetailsById(long id);
}
