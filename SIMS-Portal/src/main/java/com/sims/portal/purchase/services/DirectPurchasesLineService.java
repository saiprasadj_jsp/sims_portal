package com.sims.portal.purchase.services;
import java.util.List;

import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;

public interface DirectPurchasesLineService {
	
	public DirectPurchaseLine saveDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm);

	public List<DirectPurchaseLine> findDirectPurchasesLineForm();

	public DirectPurchaseLine findDirectPurchasesLineFormByID(Long id);

	public DirectPurchaseLine updateDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm);

	public Boolean deleteDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm);
}
