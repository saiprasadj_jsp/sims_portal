package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.purchase.dao.PurchaseRequisitionLineDao;

@Repository
@Transactional
public class PurchaseRequisitionLineDaoImpl implements PurchaseRequisitionLineDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public PurchaseRequisitionLine savePurchaseRequisitionLineForm(PurchaseRequisitionLine PurchaseRequisitionLineForm) {

		entityManager.persist(PurchaseRequisitionLineForm);
		return PurchaseRequisitionLineForm;
	}

	@Override
	public List<PurchaseRequisitionLine> findPurchaseRequisitionLineForm(){

		List<PurchaseRequisitionLine> LineStockingOfShrimpsToTankFormListData = entityManager.createQuery(
				"SELECT PurchaseRequisitionLine FROM PurchaseRequisitionLine PurchaseRequisitionLine ",
				PurchaseRequisitionLine.class).getResultList();

		return LineStockingOfShrimpsToTankFormListData;
	}

	@Override
	public PurchaseRequisitionLine findPurchaseRequisitionLineFormByID(Long id) {

		List<PurchaseRequisitionLine> PurchaseRequisitionLineForm = entityManager.createQuery(
				"SELECT PurchaseRequisitionLine FROM PurchaseRequisitionLine PurchaseRequisitionLine WHERE PurchaseRequisitionLine.PRLId='"
						+ id + "'",
						PurchaseRequisitionLine.class).getResultList();

		if (PurchaseRequisitionLineForm.size() > 0) {

			return PurchaseRequisitionLineForm.get(0);
		}

		return new PurchaseRequisitionLine();
	}

	@Override
	public PurchaseRequisitionLine updatePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm) {

		System.out.println("Merged ID BEFORE ====== " + PurchaseRequisitionLineForm.getPRLId());
		entityManager.merge(PurchaseRequisitionLineForm);
		System.out.println("Merged ID AFTER ====== " + PurchaseRequisitionLineForm.getPRLId());

		return PurchaseRequisitionLineForm;
	}

	@Override
	public Boolean deletePurchaseRequisitionLineForm(PurchaseRequisitionLine PurchaseRequisitionLineForm) {

		entityManager.remove(entityManager.merge(PurchaseRequisitionLineForm));
		return true;
	}

}