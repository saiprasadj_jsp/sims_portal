package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;

public interface MaterialReceiptLineDao {

	public MaterialReceiptLine saveMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

	public List<MaterialReceiptLine> findMaterialReceiptLineForm();

	public MaterialReceiptLine findMaterialReceiptLineFormByID(Long id);

	public MaterialReceiptLine updateMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

	public Boolean deleteMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

}
