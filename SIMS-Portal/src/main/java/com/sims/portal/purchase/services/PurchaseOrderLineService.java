package com.sims.portal.purchase.services;
import java.util.List;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;

public interface PurchaseOrderLineService {

	public PurchaseOrderLine savePurchaseOrderLineForm(
			PurchaseOrderLine purchaseOrderLineForm);

	public List<PurchaseOrderLine> findPurchaseOrderLineDetails();

	public PurchaseOrderLine findPurchaseOrderLineByID(Long id);

	public PurchaseOrderLine updatePurchaseOrderLine(
			PurchaseOrderLine purchaseOrderLineForm);

	public Boolean deletePurchaseOrderLine(
			PurchaseOrderLine purchaseOrderLineForm);
}
