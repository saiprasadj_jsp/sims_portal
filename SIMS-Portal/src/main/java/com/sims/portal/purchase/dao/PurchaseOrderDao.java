package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.beans.PurchaseOrderForm;

public interface PurchaseOrderDao {

	public PurchaseOrderForm savePurchaseOrder(PurchaseOrderForm PurchaseOrderForm);
	
	public PurchaseOrderForm updatePurchaseOrder(PurchaseOrderForm PurchaseOrderForm);
	
	public Boolean deletePurchaseOrder(PurchaseOrderForm PurchaseOrderForm);

	public List<PurchaseOrderForm> findPurchaseOrderDetails();

	public PurchaseOrderForm findPurchaseOrderDetailsByCode(Long id);

}
