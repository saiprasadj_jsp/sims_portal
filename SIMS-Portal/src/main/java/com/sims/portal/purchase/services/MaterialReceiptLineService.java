package com.sims.portal.purchase.services;

import java.util.List;

import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;

public interface MaterialReceiptLineService {

	public MaterialReceiptLine saveMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

	public List<MaterialReceiptLine> findMaterialReceiptLineDetails();

	public MaterialReceiptLine findMaterialReceiptLineFormByID(Long id);

	public MaterialReceiptLine updateMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

	public Boolean deleteMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm);

}
