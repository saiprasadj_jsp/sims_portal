package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.beans.MaterialReceiptForm;
import com.sims.portal.purchase.dao.MaterialReceiptDao;

@Repository
@Transactional
public class MaterialReceiptDaoImpl implements MaterialReceiptDao {

	@PersistenceContext
	private EntityManager entityManager;

	public MaterialReceiptForm saveMaterialReceipt(MaterialReceiptForm materialReceiptForm) {

		entityManager.persist(materialReceiptForm);

		return materialReceiptForm;
	}

	@Override
	public MaterialReceiptForm updateMaterialReceipt(MaterialReceiptForm materialReceiptForm) {
		
		System.out.println("Merged ID BEFORE ====== "+materialReceiptForm.getMRId());
		entityManager.merge(materialReceiptForm);
		System.out.println("Merged ID AFTER ====== "+materialReceiptForm.getMRId());
		
		return materialReceiptForm;
	}
	
	
	@Override
	public Boolean deleteMaterialReceipt(MaterialReceiptForm materialReceiptForm) {
		
		entityManager.remove(materialReceiptForm);
		return true;
	}
	
	@Override
	public List<MaterialReceiptForm> findMaterialReceiptDetails() {

		List<MaterialReceiptForm> materialReceiptFormList = entityManager
				.createQuery("SELECT materialReceiptForm FROM MaterialReceiptForm materialReceiptForm ",
						MaterialReceiptForm.class)
				.getResultList();

		return materialReceiptFormList;
	}

	@Override
	public MaterialReceiptForm findMaterialReceiptDetailsById(long id) {

		List<MaterialReceiptForm> MaterialReceiptForm = entityManager.createQuery(
				"SELECT materialReceiptForm FROM MaterialReceiptForm materialReceiptForm WHERE materialReceiptForm.MRId="
						+id+"",
				MaterialReceiptForm.class).getResultList();

		if (MaterialReceiptForm.size() > 0) {

			return MaterialReceiptForm.get(0);
		}

		return new MaterialReceiptForm();
	}
}
