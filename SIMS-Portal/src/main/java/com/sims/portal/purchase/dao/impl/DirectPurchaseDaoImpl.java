package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.beans.DirectPurchasesForm;
import com.sims.portal.purchase.dao.DirectPurchaseDao;

@Repository
@Transactional
public class DirectPurchaseDaoImpl implements DirectPurchaseDao {

	@PersistenceContext
	private EntityManager entityManager;

	public DirectPurchasesForm saveDirectPurchase(DirectPurchasesForm directPurchasesForm) {

		entityManager.persist(directPurchasesForm);

		return directPurchasesForm;
	}

	@Override
	public DirectPurchasesForm updateDirectPurchase(DirectPurchasesForm directPurchasesForm) {
		
		System.out.println("Merged ID BEFORE ====== "+directPurchasesForm.getDPId());
		entityManager.merge(directPurchasesForm);
		System.out.println("Merged ID AFTER ====== "+directPurchasesForm.getDPId());
		
		return directPurchasesForm;
	}
	
	
	@Override
	public Boolean deleteDirectPurchase(DirectPurchasesForm directPurchasesForm) {
		
		entityManager.remove(directPurchasesForm);
		return true;
	}
	
	@Override
	public List<DirectPurchasesForm> findDirectPurchaseDetails() {

		List<DirectPurchasesForm> materialReceiptFormList = entityManager
				.createQuery("SELECT directPurchasesForm FROM DirectPurchasesForm directPurchasesForm ",
						DirectPurchasesForm.class)
				.getResultList();

		return materialReceiptFormList;
	}

	@Override
	public DirectPurchasesForm findDirectPurchaseDetailsById(long id) {

		List<DirectPurchasesForm> DirectPurchasesForm = entityManager.createQuery(
				"SELECT directPurchasesForm FROM DirectPurchasesForm directPurchasesForm WHERE directPurchasesForm.DPId='"
						+id+"'",
				DirectPurchasesForm.class).getResultList();

		if (DirectPurchasesForm.size() > 0) {

			return DirectPurchasesForm.get(0);
		}

		return new DirectPurchasesForm();
	}
}
