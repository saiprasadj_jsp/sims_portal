package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.purchase.dao.DirectPurchasesLineDao;
import com.sims.portal.purchase.services.DirectPurchasesLineService;

@Service
public class DirectPurchasesLineServiceImpl implements DirectPurchasesLineService {

	@Autowired
	private DirectPurchasesLineDao directPurchaseLineDaoObj;

	@Override
	public DirectPurchaseLine saveDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm) {

		directPurchaseLineDaoObj.saveDirectPurchasesLineForm(directPurchaseLineForm);
		return directPurchaseLineForm;
	}

	@Override
	public List<DirectPurchaseLine> findDirectPurchasesLineForm() {

		List<DirectPurchaseLine> directPurchaseLineFormListData = directPurchaseLineDaoObj.findDirectPurchasesLineForm();

		return directPurchaseLineFormListData;
	}

	@Override
	public DirectPurchaseLine findDirectPurchasesLineFormByID(Long id){

		return directPurchaseLineDaoObj.findDirectPurchasesLineFormByID(id);
	}
	
	@Override
	public DirectPurchaseLine updateDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm) {

		directPurchaseLineDaoObj.updateDirectPurchasesLineForm(directPurchaseLineForm);

		return directPurchaseLineForm;
	}

	@Override
	public Boolean deleteDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm) {

		return directPurchaseLineDaoObj.deleteDirectPurchasesLineForm(directPurchaseLineForm);
	}
}
