package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;
import com.sims.portal.purchase.dao.PurchaseOrderLineDao;

@Repository
@Transactional
public class PurchaseOrderLineDaoImpl implements PurchaseOrderLineDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public PurchaseOrderLine savePurchaseOrderLineForm(PurchaseOrderLine purchaseOrderLineForm) {

		entityManager.persist(purchaseOrderLineForm);
		return purchaseOrderLineForm;
	}

	@Override
	public List<PurchaseOrderLine> findPurchaseOrderLineForm(){

		List<PurchaseOrderLine> LineStockingOfShrimpsToTankFormListData = entityManager.createQuery(
				"SELECT PurchaseOrderLine FROM PurchaseOrderLine PurchaseOrderLine ",
				PurchaseOrderLine.class).getResultList();

		return LineStockingOfShrimpsToTankFormListData;
	}

	@Override
	public PurchaseOrderLine findPurchaseOrderLineFormByID(Long id) {

		List<PurchaseOrderLine> PurchaseOrderLineForm = entityManager.createQuery(
				"SELECT PurchaseOrderLine FROM PurchaseOrderLine PurchaseOrderLine WHERE PurchaseOrderLine.POLId='"
						+ id + "'",
						PurchaseOrderLine.class).getResultList();

		if (PurchaseOrderLineForm.size() > 0) {

			return PurchaseOrderLineForm.get(0);
		}

		return new PurchaseOrderLine();
	}

	@Override
	public PurchaseOrderLine updatePurchaseOrderLineForm(
			PurchaseOrderLine purchaseOrderLineForm) {

		System.out.println("Merged ID BEFORE ====== " + purchaseOrderLineForm.getPOLId());
		entityManager.merge(purchaseOrderLineForm);
		System.out.println("Merged ID AFTER ====== " + purchaseOrderLineForm.getPOLId());

		return purchaseOrderLineForm;
	}

	@Override
	public Boolean deletePurchaseOrderLineForm(PurchaseOrderLine purchaseOrderLineForm) {

		entityManager.remove(entityManager.merge(purchaseOrderLineForm));
		return true;
	}

}