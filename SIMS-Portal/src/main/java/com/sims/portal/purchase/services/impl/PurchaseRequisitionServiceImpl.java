package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;
import com.sims.portal.purchase.dao.PurchaseRequisitionDao;
import com.sims.portal.purchase.services.PurchaseRequisitionService;

@Service
public class PurchaseRequisitionServiceImpl implements PurchaseRequisitionService {

	@Autowired
	private PurchaseRequisitionDao purchaseRequisitionDao;

	public PurchaseRequisitionForm savePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm) {

		purchaseRequisitionDao.savePurchaseRequisition(purchaseRequisitionForm);

		return purchaseRequisitionForm;

	}

	@Override
	public PurchaseRequisitionForm updatePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm) {

		purchaseRequisitionDao.updatePurchaseRequisition(purchaseRequisitionForm);

		return purchaseRequisitionForm;
	}

	@Override
	public Boolean deletePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm) {

		return purchaseRequisitionDao.deletePurchaseRequisition(purchaseRequisitionForm);
	}

	@Override
	public List<PurchaseRequisitionForm> findPurchaseRequisitionDetails() {

		return purchaseRequisitionDao.findPurchaseRequisitionDetails();
	}

	@Override
	public PurchaseRequisitionForm findPurchaseRequisitionDetailsById(Long id) {

		return purchaseRequisitionDao.findPurchaseRequisitionDetailsById(id);
	}
}
