package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;

public interface PurchaseOrderLineDao {

	public PurchaseOrderLine savePurchaseOrderLineForm(
			PurchaseOrderLine purchaseOrderLineForm);

	public List<PurchaseOrderLine> findPurchaseOrderLineForm();

	public PurchaseOrderLine findPurchaseOrderLineFormByID(Long id);

	public PurchaseOrderLine updatePurchaseOrderLineForm(
			PurchaseOrderLine purchaseOrderLineForm);

	public Boolean deletePurchaseOrderLineForm(
			PurchaseOrderLine purchaseOrderLineForm);

}
