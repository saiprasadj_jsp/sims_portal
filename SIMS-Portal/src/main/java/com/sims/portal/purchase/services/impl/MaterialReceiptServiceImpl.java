package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.beans.MaterialReceiptForm;
import com.sims.portal.purchase.dao.MaterialReceiptDao;
import com.sims.portal.purchase.services.MaterialReceiptService;

@Service
public class MaterialReceiptServiceImpl implements MaterialReceiptService {

	@Autowired
	private MaterialReceiptDao materialReceiptDao;

	public MaterialReceiptForm saveMaterialReceipt(MaterialReceiptForm materialReceiptForm) {

		materialReceiptDao.saveMaterialReceipt(materialReceiptForm);

		return materialReceiptForm;

	}

	@Override
	public MaterialReceiptForm updateMaterialReceipt(MaterialReceiptForm materialReceiptForm) {

		materialReceiptDao.updateMaterialReceipt(materialReceiptForm);

		return materialReceiptForm;
	}

	@Override
	public Boolean deleteMaterialReceipt(MaterialReceiptForm materialReceiptForm) {

		return materialReceiptDao.deleteMaterialReceipt(materialReceiptForm);
	}

	@Override
	public List<MaterialReceiptForm> findMaterialReceiptDetails() {

		return materialReceiptDao.findMaterialReceiptDetails();
	}

	@Override
	public MaterialReceiptForm findMaterialReceiptDetailsById(long id) {

		return materialReceiptDao.findMaterialReceiptDetailsById(id);
	}
}
