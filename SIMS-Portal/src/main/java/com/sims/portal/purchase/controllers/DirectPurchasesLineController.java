package com.sims.portal.purchase.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.model.purchase.beans.DirectPurchasesForm;
import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.model.purchase.line.beans.DirectPurchaseLineFormDTO;
import com.sims.portal.purchase.services.DirectPurchaseService;
import com.sims.portal.purchase.services.DirectPurchasesLineService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_MAIN_URL)
public class DirectPurchasesLineController {

	@Autowired
	private DirectPurchaseService directPurchaseService;
	
	@Autowired
	private DirectPurchasesLineService directPurchasesLineService;

	
	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;
	
	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLineDirectPurchasesLineForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLieDirectPurchaseForm(modelAndView);
		modelAndView.addObject("directPurchaseLineForm", new DirectPurchaseLine());
		
		modelAndView.addObject("DirectPurchaseLineURL",
				TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
	public void saveDirectPurchaseLine(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("receivedQuantity") Double receivedQuantity,
			@RequestParam("rejectedQuantity") Double rejectedQuantity,
			@RequestParam("acceptedQuantity") Double acceptedQuantity,
			@RequestParam("grossQuantity") Double grossQuantity,
			
			@RequestParam("discount") Double discount,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("freightAmount") Double freightAmount
			) {	
		
		DirectPurchasesForm directPurchasesForm = directPurchaseService.findDirectPurchaseDetailsById(id);

		System.out.println(
				"List Size ======= " +directPurchasesForm.getDirectPurchaseLineList().size());
	
		DirectPurchaseLine directPurchaseLineForm = new DirectPurchaseLine();
		directPurchaseLineForm.setDirectPurchasesForm(directPurchasesForm);
		
		setDirectPurchaseLineData(productCode, productName,UOM, receivedQuantity,rejectedQuantity,
				 acceptedQuantity,grossQuantity,discount,VAT,CST, freightAmount,directPurchaseLineForm);

	
		ModelAndView modelAndView = new ModelAndView();
		directPurchasesLineService.saveDirectPurchasesLineForm(directPurchaseLineForm);
		setDefaultDataForDirectPurchaseLine(modelAndView);
		findAllLieDirectPurchaseForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");
		
	}

	private void setDirectPurchaseLineData(String productCode, String productName,
											 String UOM,
											 Double receivedQuantity,
											 Double rejectedQuantity,
											 Double acceptedQuantity,
											 Double grossQuantity,
											 Double discount,
											 Double VAT,
											 Double CST,
											 Double freightAmount,DirectPurchaseLine directPurchaseLineForm) {
		
		
		directPurchaseLineForm.setProductCode(productCode);
		directPurchaseLineForm.setProductName(productName);
		directPurchaseLineForm.setUOM(UOM);
		directPurchaseLineForm.setReceivedQuantity(receivedQuantity);
		directPurchaseLineForm.setAcceptedQuantity(acceptedQuantity);
		directPurchaseLineForm.setRejectedQuantity(rejectedQuantity);
		directPurchaseLineForm.setGrossQuantity(grossQuantity);
		directPurchaseLineForm.setVAT(VAT);
		directPurchaseLineForm.setCST(CST);
		directPurchaseLineForm.setDiscount(discount);
		directPurchaseLineForm.setFreightAmount(freightAmount);
		
			}

	public ModelAndView findAllLieDirectPurchaseForm(ModelAndView modelAndView) {

		List<DirectPurchaseLine> directPurchaseLineData = directPurchasesLineService.findDirectPurchasesLineForm();
		modelAndView.addObject("directPurchaseLineDataListData", directPurchaseLineData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForDirectPurchaseLine(ModelAndView modelAndView) {

		modelAndView.addObject("directPurchaseLineForm", new DirectPurchaseLine());
		modelAndView.addObject("directPurchaseLineURL",
				TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public DirectPurchaseLineFormDTO findMaterialReceiptLineByID(
			@PathVariable(name = "id") Long id) {

		System.out.println("DirectPurchaseLine ID Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		DirectPurchaseLine directPurhaseLineForm = directPurchasesLineService.findDirectPurchasesLineFormByID(id);
		
		modelAndView.addObject("directPurhaseLineForm", directPurhaseLineForm);
		// findLineStockingOfShrimpsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		DirectPurchaseLineFormDTO dto = new DirectPurchaseLineFormDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_UPDATE_URL
				+ directPurhaseLineForm.getDPLId());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_DELETE_URL
				+ directPurhaseLineForm.getDPLId());
		return dto.getDirectPurchaseLineFormDTO(directPurhaseLineForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLinePurchaseOrderLineForm(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("receivedQuantity") Double receivedQuantity,
			@RequestParam("rejectedQuantity") Double rejectedQuantity,
			@RequestParam("acceptedQuantity") Double acceptedQuantity,
			@RequestParam("grossQuantity") Double grossQuantity,			
			@RequestParam("discount") Double discount,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("freightAmount") Double freightAmount) {
			
		
		System.out.println("UPDATING Line ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);

		DirectPurchaseLine directPurchaseLineForm = directPurchasesLineService.findDirectPurchasesLineFormByID(id);
				
		
		setDirectPurchaseLineData(productCode, productName,UOM, receivedQuantity,rejectedQuantity,
		 acceptedQuantity,grossQuantity,discount,VAT,CST, freightAmount,directPurchaseLineForm);


		directPurchasesLineService.updateDirectPurchasesLineForm(directPurchaseLineForm);
		setDefaultDataForDirectPurchaseLine(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deletePurchaseRequisitionLineByID(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("receivedQuantity") Double receivedQuantity,
			@RequestParam("rejectedQuantity") Double rejectedQuantity,
			@RequestParam("acceptedQuantity") Double acceptedQuantity,
			@RequestParam("grossQuantity") Double grossQuantity,			
			@RequestParam("Discount") Double Discount,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("freightAmount") Double freightAmount) {

		System.out.println("DELETING ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		DirectPurchaseLine directPurchaseLineForm = directPurchasesLineService.findDirectPurchasesLineFormByID(id);
				
		directPurchasesLineService.deleteDirectPurchasesLineForm(directPurchaseLineForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findMaterialReceiptLineDetails(ModelAndView modelAndView) {

		List<DirectPurchaseLine> lineMaterialReceiptLineList = directPurchasesLineService.findDirectPurchasesLineForm();
			
		modelAndView.addObject("lineMaterialReceiptLineList", lineMaterialReceiptLineList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}
	
	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}