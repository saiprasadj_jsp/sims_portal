package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.beans.PurchaseOrderForm;
import com.sims.portal.purchase.dao.PurchaseOrderDao;
import com.sims.portal.purchase.services.PurchaseOrderService;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

	@Autowired
	private PurchaseOrderDao purchaseRequisitionDao;

	public PurchaseOrderForm savePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm) {

		purchaseRequisitionDao.savePurchaseOrder(purchaseRequisitionForm);

		return purchaseRequisitionForm;

	}

	@Override
	public PurchaseOrderForm updatePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm) {

		purchaseRequisitionDao.updatePurchaseOrder(purchaseRequisitionForm);

		return purchaseRequisitionForm;
	}

	@Override
	public Boolean deletePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm) {

		return purchaseRequisitionDao.deletePurchaseOrder(purchaseRequisitionForm);
	}

	@Override
	public List<PurchaseOrderForm> findPurchaseOrderDetails() {

		return purchaseRequisitionDao.findPurchaseOrderDetails();
	}

	@Override
	public PurchaseOrderForm findPurchaseOrderDetailsById(Long id) {

		return purchaseRequisitionDao.findPurchaseOrderDetailsByCode(id);
	}
}
