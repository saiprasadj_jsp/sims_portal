package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;

public interface PurchaseRequisitionDao {

	public PurchaseRequisitionForm savePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm);
	
	public PurchaseRequisitionForm updatePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm);
	
	public Boolean deletePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm);

	public List<PurchaseRequisitionForm> findPurchaseRequisitionDetails();

	public PurchaseRequisitionForm findPurchaseRequisitionDetailsById(Long id);

}
