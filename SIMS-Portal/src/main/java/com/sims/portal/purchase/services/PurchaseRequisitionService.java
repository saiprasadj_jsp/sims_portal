package com.sims.portal.purchase.services;

import java.util.List;

import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;

public interface PurchaseRequisitionService {

	public PurchaseRequisitionForm savePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm);
	
	public PurchaseRequisitionForm updatePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm);
	
	public Boolean deletePurchaseRequisition(PurchaseRequisitionForm purchaseRequisitionForm);
	
	public List<PurchaseRequisitionForm> findPurchaseRequisitionDetails();

	public PurchaseRequisitionForm findPurchaseRequisitionDetailsById(Long id);
}
