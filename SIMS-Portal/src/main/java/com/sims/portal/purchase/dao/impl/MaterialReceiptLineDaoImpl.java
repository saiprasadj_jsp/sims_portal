package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;
import com.sims.portal.purchase.dao.MaterialReceiptLineDao;

@Repository
@Transactional
public class MaterialReceiptLineDaoImpl implements MaterialReceiptLineDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public MaterialReceiptLine saveMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		entityManager.persist(materialReceiptLineForm);
		return materialReceiptLineForm;
	}

	@Override
	public List<MaterialReceiptLine> findMaterialReceiptLineForm(){

		List<MaterialReceiptLine> LineMaterialReceiptLineListData = entityManager.createQuery(
				"SELECT MaterialReceiptLine FROM MaterialReceiptLine MaterialReceiptLine ",
				MaterialReceiptLine.class).getResultList();

		return LineMaterialReceiptLineListData;
	}

	@Override
	public MaterialReceiptLine findMaterialReceiptLineFormByID(Long id){

		List<MaterialReceiptLine> directPurchaseLineForm = entityManager.createQuery(
				"SELECT MaterialReceiptLine FROM MaterialReceiptLine MaterialReceiptLine WHERE MaterialReceiptLine.MRLId='"
						+ id + "'",
						MaterialReceiptLine.class).getResultList();

		if (directPurchaseLineForm.size() > 0) {

			return directPurchaseLineForm.get(0);
		}

		return new MaterialReceiptLine();
	}

	@Override
	public MaterialReceiptLine updateMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		System.out.println("Merged ID BEFORE ====== " + materialReceiptLineForm.getMRLId());
		entityManager.merge(materialReceiptLineForm);
		System.out.println("Merged ID AFTER ====== " + materialReceiptLineForm.getMRLId());

		return materialReceiptLineForm;
	}

	@Override
	public Boolean deleteMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		entityManager.remove(entityManager.merge(materialReceiptLineForm));
		return true;
	}

}