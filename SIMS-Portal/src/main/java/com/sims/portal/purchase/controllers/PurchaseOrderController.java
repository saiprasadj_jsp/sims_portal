package com.sims.portal.purchase.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.model.purchase.beans.PurchaseOrderForm;
import com.sims.portal.model.purchase.line.beans.PurchaseOrderLine;
import com.sims.portal.purchase.services.PurchaseOrderService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RequestMapping(value =TankProcessConstants.TILES_LINE_PURCHASE_ORDER_MAIN_URL)
@Controller
public class PurchaseOrderController {

	@Autowired
	private PurchaseOrderService purchaseOrderService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showPurchaseOrderPage() {

		ModelAndView modelAndView = new ModelAndView();
		setDefaultDataForPurchaseOrderPage(modelAndView);
		
		modelAndView.addObject("purchaseOrderLineForm", new PurchaseOrderLine());
		
		modelAndView.addObject("purchaseOrderLineSaveURL",
				TankProcessConstants.TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SAVE_URL);
		
		// LIST PAGE DATA  lineStockingOfShrimpsToTankForm  
		findPurchaseOrderDetails(modelAndView);

		return modelAndView;
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView savePurchaseOrder(@ModelAttribute("purchaseOrderForm") PurchaseOrderForm purchaseOrderForm) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("PurchaseOrderForm " + purchaseOrderForm.getPOId());
		purchaseOrderService.savePurchaseOrder(purchaseOrderForm);
		setDefaultDataForPurchaseOrderPage(modelAndView);
		findPurchaseOrderDetails(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findPurchaseOrderDetails(ModelAndView modelAndView) {

		List<PurchaseOrderForm> purchaseOrderFormList = purchaseOrderService.findPurchaseOrderDetails();
		modelAndView.addObject("purchaseOrderListData", purchaseOrderFormList);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findPurchaseOrderDetailsByCode(@PathVariable(name = "id") Long id) {

		System.out.println("Code Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		PurchaseOrderForm purchaseOrderForm = purchaseOrderService.findPurchaseOrderDetailsById(id);
		modelAndView.addObject("purchaseOrderForm", purchaseOrderForm);
		findPurchaseOrderDetails(modelAndView);
		modelAndView.setViewName(MastersPageConstants.PURCHASE_ORDER_MAIN_PAGE);
		modelAndView.addObject("tabToShow", "details");
		
		Collection<PurchaseOrderLine> listOfPurchaseOrderLines = purchaseOrderForm.getPurchaseOrderLine();
		modelAndView.addObject("listOfLinePurchaseOrderLineObj", listOfPurchaseOrderLines);
		
		modelAndView.addObject("purchaseOrderURL", "purchase/purchaseorder/update");

		return modelAndView;
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updatePurchaseOrder(@PathVariable(name="id") Long id,@ModelAttribute("purchaseOrderForm") PurchaseOrderForm purchaseOrderForm) {
		
		System.out.println("UPDATING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);
		purchaseOrderForm.setPOId(id);
		purchaseOrderService.updatePurchaseOrder(purchaseOrderForm);
		setDefaultDataForPurchaseOrderPage(modelAndView);
		findPurchaseOrderDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deletePurchaseOrderDetailsByCode(@PathVariable(name = "id") Long id,@ModelAttribute("purchaseOrderForm") PurchaseOrderForm purchaseOrderForm) {

		System.out.println("DELETING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		purchaseOrderForm.setPOId(id);
		purchaseOrderService.deletePurchaseOrder(purchaseOrderForm);
		setDefaultDataForPurchaseOrderPage(modelAndView);
		findPurchaseOrderDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}
	
	private ModelAndView setDefaultDataForPurchaseOrderPage(ModelAndView modelAndView) {

		modelAndView.addObject("purchaseOrderForm", new PurchaseOrderForm());

		HashMap<String, String> typesOfCustomersMap = new HashMap<String, String>();

		typesOfCustomersMap.put("customertype1", "Customer Type 1");
		typesOfCustomersMap.put("customertype2", "Customer Type 2");
		typesOfCustomersMap.put("customertype3", "Customer Type 3");
		typesOfCustomersMap.put("customertype4", "Customer Type 4");
		typesOfCustomersMap.put("customertype5", "Customer Type 5");

		modelAndView.addObject("typesOfCustomersMap", typesOfCustomersMap);
		modelAndView.addObject("purchaseOrderURL", "purchase/purchaseorder/save");
		modelAndView.setViewName(MastersPageConstants.PURCHASE_ORDER_MAIN_PAGE);
		return modelAndView;
	}

}
