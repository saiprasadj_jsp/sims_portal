package com.sims.portal.purchase.services;

import java.util.List;

import com.sims.portal.model.purchase.beans.PurchaseOrderForm;

public interface PurchaseOrderService {

	public PurchaseOrderForm savePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm);
	
	public PurchaseOrderForm updatePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm);
	
	public Boolean deletePurchaseOrder(PurchaseOrderForm purchaseRequisitionForm);
	
	public List<PurchaseOrderForm> findPurchaseOrderDetails();

	public PurchaseOrderForm findPurchaseOrderDetailsById(Long id);
}
