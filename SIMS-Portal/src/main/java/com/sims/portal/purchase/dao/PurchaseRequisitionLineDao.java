package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;

public interface PurchaseRequisitionLineDao {

	public PurchaseRequisitionLine savePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);

	public List<PurchaseRequisitionLine> findPurchaseRequisitionLineForm();

	public PurchaseRequisitionLine findPurchaseRequisitionLineFormByID(Long id);

	public PurchaseRequisitionLine updatePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);

	public Boolean deletePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);

}
