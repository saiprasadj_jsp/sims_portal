package com.sims.portal.purchase.services;

import java.util.List;

import com.sims.portal.model.purchase.beans.MaterialReceiptForm;

public interface MaterialReceiptService{

	public MaterialReceiptForm saveMaterialReceipt(MaterialReceiptForm materialReceiptForm);
	
	public MaterialReceiptForm updateMaterialReceipt(MaterialReceiptForm materialReceiptForm);
	
	public Boolean deleteMaterialReceipt(MaterialReceiptForm materialReceiptForm);
	
	public List<MaterialReceiptForm> findMaterialReceiptDetails();

	public MaterialReceiptForm findMaterialReceiptDetailsById(long id);
}
