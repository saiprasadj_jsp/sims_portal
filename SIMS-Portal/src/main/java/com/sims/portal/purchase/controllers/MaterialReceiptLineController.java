package com.sims.portal.purchase.controllers;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.model.purchase.beans.MaterialReceiptForm;
import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;
import com.sims.portal.model.purchase.line.beans.MeterialReceiptLineFormDTO;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;
import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLineFormDTO;
import com.sims.portal.purchase.services.MaterialReceiptLineService;
import com.sims.portal.purchase.services.MaterialReceiptService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_MAIN_URL)
public class MaterialReceiptLineController {

	@Autowired
	private MaterialReceiptService materialReceiptService;
	
	@Autowired
	private MaterialReceiptLineService materialReceiptLineService;

	
	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;
	
	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLinePurchaseOrderLineForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLinePurchaseRequisitionForm(modelAndView);
		modelAndView.addObject("materialReceiptLineForm", new MaterialReceiptLine());
		
		modelAndView.addObject("MaterialReceiptLineURL",
				TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
	public void savepurchaseRequisitionLine(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("receivedQuantityNumber") Double receivedQuantityNumber,
			@RequestParam("rejectedQuantityNumber") Double rejectedQuantityNumber,
			@RequestParam("acceptedQuantityNumber") Double acceptedQuantityNumber,
			@RequestParam("grossQuantity") Double grossQuantity,			
			@RequestParam("discount") Double discount,
			@RequestParam("rate") Double rate,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("PONumber") String PONumber,
			@RequestParam("PODate") Date PODate,
			@RequestParam("freight") Double freight,
			@RequestParam("remarks") String remarks
			) {	
		
	/*	
		private String productCode;
		private String productName;
		private String UOM;	
		private String remarks;
		private Double receivedQuantityNumber;
		private Double rejectedQuantityNumber;
		private Double acceptedQuantityNumber;
		private Double grossQuantity;
//		private Double rate;
		private Double discount;
		private Double VAT;
		private Double CST;
		private String PONumber;	
		private Date PODate;
		private Double freight;

*/		
		MaterialReceiptForm materialReceiptForm = materialReceiptService.findMaterialReceiptDetailsById(id);

		System.out.println(
				"List Size ===ssssssssss==== " + materialReceiptForm.getMaterialReceiptLineList().size());
	
		MaterialReceiptLine materialReceiptLineForm = new MaterialReceiptLine();
		materialReceiptLineForm.setMaterialReceiptForm(materialReceiptForm);
		
		setMaterialReceiptLineData(productCode, productName,UOM,remarks, receivedQuantityNumber,rejectedQuantityNumber,
				acceptedQuantityNumber,grossQuantity,rate,discount,VAT,CST,PONumber,PODate,freight,materialReceiptLineForm);
	
		ModelAndView modelAndView = new ModelAndView();
		materialReceiptLineService.saveMaterialReceiptLineForm(materialReceiptLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		findAllLinePurchaseRequisitionForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

	}

	private void setMaterialReceiptLineData(String productCode,
			                                String productName,
											 String UOM,
											 String remarks,
											 Double receivedQuantity,
											 Double rejectedQuantity,
											 Double acceptedQuantity,
											 Double grossQuantity,
											 Double rate,
											 Double discount,
											 Double VAT,
											 Double CST,								
											 String PONumber,
											 Date PODate,
											 Double freight,
											 MaterialReceiptLine materialReceiptLineForm) {
		
		
		/*private String productCode;
		private String productName;
		private String UOM;	
		private String remarks;
		private Double receivedQuantityNumber;
		private Double rejectedQuantityNumber;
		private Double acceptedQuantityNumber;
		private Double grossQuantity;
		private Double rate;
		private Double discount;
		private Double VAT;
		private Double CST;
		private String PONumber;	
		private Date PODate;
		private Double freight;
*/
	
		
		materialReceiptLineForm.setProductCode(productCode);
		materialReceiptLineForm.setProductName(productName);
		materialReceiptLineForm.setUOM(UOM);
		materialReceiptLineForm.setProductCode(productCode);
		materialReceiptLineForm.setProductName(productName);
		materialReceiptLineForm.setUOM(UOM);
		materialReceiptLineForm.setReceivedQuantityNumber(receivedQuantity);
		materialReceiptLineForm.setAcceptedQuantityNumber(acceptedQuantity);
		materialReceiptLineForm.setRejectedQuantityNumber(rejectedQuantity);
		materialReceiptLineForm.setGrossQuantity(grossQuantity);
		materialReceiptLineForm.setVAT(VAT);
		materialReceiptLineForm.setCST(CST);
		materialReceiptLineForm.setDiscount(discount);
		materialReceiptLineForm.setRemarks(remarks);
		materialReceiptLineForm.setPONumber(PONumber);
		materialReceiptLineForm.setFreight(freight);
		materialReceiptLineForm.setPODate(PODate);
		materialReceiptLineForm.setRate(rate);
					
	}

	public ModelAndView findAllLinePurchaseRequisitionForm(ModelAndView modelAndView) {

		List<MaterialReceiptLine> materialReceiptLineData = materialReceiptLineService.findMaterialReceiptLineDetails();
		modelAndView.addObject("materialReceiptLineListData", materialReceiptLineData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForPurchaseOrderLine(ModelAndView modelAndView) {

		modelAndView.addObject("materialReceiptLineForm", new MaterialReceiptLine());
		modelAndView.addObject("materialReceiptLineURL",
				TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public MeterialReceiptLineFormDTO findMaterialReceiptLineByID(
			@PathVariable(name = "id") Long id) {

		System.out.println("MaterialReceiptLine ID Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		MaterialReceiptLine materialReceiptLineLineForm = materialReceiptLineService.findMaterialReceiptLineFormByID(id);
		
		modelAndView.addObject("materialReceiptLineLineForm", materialReceiptLineLineForm);
		// findLineStockingOfShrimpsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		MeterialReceiptLineFormDTO dto = new MeterialReceiptLineFormDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_UPDATE_URL
				+ materialReceiptLineLineForm.getMRLId());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_DELETE_URL
				+ materialReceiptLineLineForm.getMRLId());
		return dto.getMeterialReceiptLineFormDTO(materialReceiptLineLineForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLinePurchaseOrderLineForm(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("UOM") String UOM,
			@RequestParam("receivedQuantityNumber") Double receivedQuantityNumber,
			@RequestParam("rejectedQuantityNumber") Double rejectedQuantityNumber,
			@RequestParam("acceptedQuantityNumber") Double acceptedQuantityNumber,
			@RequestParam("grossQuantity") Double grossQuantity,			
			@RequestParam("discount") Double discount,
			@RequestParam("rate") Double rate,
			@RequestParam("VAT") Double VAT,
			@RequestParam("CST") Double CST,
			@RequestParam("PONumber") String PONumber,
			@RequestParam("PODate") Date PODate,
			@RequestParam("freight") Double freight,
			@RequestParam("remarks") String remarks) {

		System.out.println("UPDATING Line ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);

		MaterialReceiptLine materialReceiptLineForm = materialReceiptLineService.findMaterialReceiptLineFormByID(id);
				
		
		setMaterialReceiptLineData(productCode, productName,UOM,remarks, receivedQuantityNumber,rejectedQuantityNumber,
				acceptedQuantityNumber,grossQuantity,rate,discount,VAT,CST,PONumber,PODate,freight,materialReceiptLineForm);
	

		materialReceiptLineService.updateMaterialReceiptLineForm(materialReceiptLineForm);
		setDefaultDataForPurchaseOrderLine(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deletePurchaseRequisitionLineByID(@PathVariable(name = "id") Long id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("DELETING ID =========== " + id);
		ModelAndView modelAndView = new ModelAndView();
		MaterialReceiptLine materialReceiptLineForm = materialReceiptLineService.findMaterialReceiptLineFormByID(id);
				
		materialReceiptLineService.deleteMaterialReceiptLineForm(materialReceiptLineForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findMaterialReceiptLineDetails(ModelAndView modelAndView) {

		List<MaterialReceiptLine> lineMaterialReceiptLineList = materialReceiptLineService.findMaterialReceiptLineDetails();
			
		modelAndView.addObject("lineMaterialReceiptLineList", lineMaterialReceiptLineList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}
	
	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}