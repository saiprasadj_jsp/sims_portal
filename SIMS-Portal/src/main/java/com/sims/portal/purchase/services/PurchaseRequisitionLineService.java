package com.sims.portal.purchase.services;
import java.util.List;

import com.sims.portal.model.purchase.line.beans.PurchaseRequisitionLine;

public interface PurchaseRequisitionLineService {

	public PurchaseRequisitionLine savePurchaseRequisitionLineForm(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);

	public List<PurchaseRequisitionLine> findPurchaseRequisitionLineDetails();

	public PurchaseRequisitionLine findPurchaseRequisitionLineByID(Long id);

	public PurchaseRequisitionLine updatePurchaseRequisitionLine(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);

	public Boolean deletePurchaseRequisitionLine(
			PurchaseRequisitionLine PurchaseRequisitionLineForm);
}
