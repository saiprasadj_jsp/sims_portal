package com.sims.portal.purchase.dao;

import java.util.List;

import com.sims.portal.model.purchase.beans.DirectPurchasesForm;

public interface DirectPurchaseDao {

	public DirectPurchasesForm saveDirectPurchase(DirectPurchasesForm directPurchasesForm);
	
	public DirectPurchasesForm updateDirectPurchase(DirectPurchasesForm directPurchasesForm);
	
	public Boolean deleteDirectPurchase(DirectPurchasesForm directPurchasesForm);

	public List<DirectPurchasesForm> findDirectPurchaseDetails();

	
	public DirectPurchasesForm findDirectPurchaseDetailsById(long id);

}
