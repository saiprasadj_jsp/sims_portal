package com.sims.portal.purchase.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;
import com.sims.portal.purchase.dao.MaterialReceiptLineDao;
import com.sims.portal.purchase.services.MaterialReceiptLineService;

@Service
public class MaterialReceiptLineServiceImpl implements MaterialReceiptLineService {

	@Autowired
	private MaterialReceiptLineDao materialReceiptLineDaoObj;

	@Override
	public MaterialReceiptLine saveMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		materialReceiptLineDaoObj.saveMaterialReceiptLineForm(materialReceiptLineForm);
		return materialReceiptLineForm;
	}

	@Override
	public List<MaterialReceiptLine> findMaterialReceiptLineDetails() {

		List<MaterialReceiptLine> materialReceiptLineFormListData = materialReceiptLineDaoObj.findMaterialReceiptLineForm();

		return materialReceiptLineFormListData;
	}

	@Override
	public MaterialReceiptLine findMaterialReceiptLineFormByID(Long id){

		return materialReceiptLineDaoObj.findMaterialReceiptLineFormByID(id);
	}
	
	@Override
	public MaterialReceiptLine updateMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		materialReceiptLineDaoObj.updateMaterialReceiptLineForm(materialReceiptLineForm);

		return materialReceiptLineForm;
	}

	@Override
	public Boolean deleteMaterialReceiptLineForm(
			MaterialReceiptLine materialReceiptLineForm) {

		return materialReceiptLineDaoObj.deleteMaterialReceiptLineForm(materialReceiptLineForm);
	}
}
