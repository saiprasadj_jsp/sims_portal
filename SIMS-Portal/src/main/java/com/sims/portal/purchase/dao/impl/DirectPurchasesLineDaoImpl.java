package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.purchase.dao.DirectPurchasesLineDao;

@Repository
@Transactional
public class DirectPurchasesLineDaoImpl implements DirectPurchasesLineDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public DirectPurchaseLine saveDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm) {

		entityManager.persist(directPurchaseLineForm);
		return directPurchaseLineForm;
	}

	@Override
	public List<DirectPurchaseLine> findDirectPurchasesLineForm(){

		List<DirectPurchaseLine> LineDirectPurchaseLineFormListData = entityManager.createQuery(
				"SELECT DirectPurchaseLine FROM DirectPurchaseLine DirectPurchaseLine ",
				DirectPurchaseLine.class).getResultList();

		return LineDirectPurchaseLineFormListData;
	}

	@Override
	public DirectPurchaseLine findDirectPurchasesLineFormByID(Long id) {

		List<DirectPurchaseLine> directPurchaseLineForm = entityManager.createQuery(
				"SELECT DirectPurchaseLine FROM DirectPurchaseLine DirectPurchaseLine WHERE DirectPurchaseLine.DPLId='"
						+ id + "'",
						DirectPurchaseLine.class).getResultList();

		if (directPurchaseLineForm.size() > 0) {

			return directPurchaseLineForm.get(0);
		}

		return new DirectPurchaseLine();
	}

	@Override
	public DirectPurchaseLine updateDirectPurchasesLineForm(
			DirectPurchaseLine directPurchaseLineForm) {

		System.out.println("Merged ID BEFORE ====== " + directPurchaseLineForm.getDPLId());
		entityManager.merge(directPurchaseLineForm);
		System.out.println("Merged ID AFTER ====== " + directPurchaseLineForm.getDPLId());

		return directPurchaseLineForm;
	}

	@Override
	public Boolean deleteDirectPurchasesLineForm(DirectPurchaseLine directPurchaseLineForm) {

		entityManager.remove(entityManager.merge(directPurchaseLineForm));
		return true;
	}

}