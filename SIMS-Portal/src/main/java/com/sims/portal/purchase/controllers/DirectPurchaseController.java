package com.sims.portal.purchase.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.model.purchase.beans.DirectPurchasesForm;
import com.sims.portal.model.purchase.line.beans.DirectPurchaseLine;
import com.sims.portal.model.purchase.line.beans.MaterialReceiptLine;
import com.sims.portal.purchase.services.DirectPurchaseService;
import com.sims.portal.tankprocess.constants.TankProcessConstants;

@RequestMapping(value = "/admin/direct/purchase")
@Controller
public class DirectPurchaseController {

	@Autowired
	private DirectPurchaseService directPurchaseService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showMaterialReceiptPage() {

		ModelAndView modelAndView = new ModelAndView();
		setDefaultDataForMaterialReceiptPage(modelAndView);

		// LIST PAGE DATA
		findMaterialReceiptDetails(modelAndView);

		return modelAndView;
	}

	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveMaterialReceipt(@ModelAttribute("directPurchasesForm") DirectPurchasesForm directPurchasesForm) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("DirectPurchasesForm " + directPurchasesForm.getDocumentNo());
		directPurchaseService.saveDirectPurchase(directPurchasesForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		
		modelAndView.addObject("directPurchaseLineForm", new DirectPurchaseLine());
		modelAndView.addObject("directPurchaseLineSaveURL",
				TankProcessConstants.TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SAVE_URL);

		
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findMaterialReceiptDetails(ModelAndView modelAndView) {

		List<DirectPurchasesForm> directPurchaseListData = directPurchaseService.findDirectPurchaseDetails();
		modelAndView.addObject("directPurchaseListData", directPurchaseListData);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findMaterialReceiptDetailsById(@PathVariable(name = "id") long id) {

		System.out.println("Code Received &&&&&&&&&&&&&&  " + id);
		ModelAndView modelAndView = new ModelAndView();
		DirectPurchasesForm directPurchasesForm = directPurchaseService.findDirectPurchaseDetailsById(id);
		modelAndView.addObject("directPurchasesForm", directPurchasesForm);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.setViewName(MastersPageConstants.DIRECT_PURCHASE_MAIN_PAGE);
		modelAndView.addObject("tabToShow", "details");
		
		Collection<DirectPurchaseLine> listOfDirectPurchaseLineLines =directPurchasesForm.getDirectPurchaseLineList();
		modelAndView.addObject("listOfLinelistOflistOfDirectPurchaseLineLineObj", listOfDirectPurchaseLineLines);				
		
		modelAndView.addObject("directPurchaseURL", "direct/purchase/update");
		
		
		

		return modelAndView;
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateMaterialReceipt(@PathVariable(name="id")Long id,@ModelAttribute("directPurchasesForm") DirectPurchasesForm directPurchasesForm) {
		
		System.out.println("UPDATING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + id);
		directPurchasesForm.setDPId(id);
		directPurchaseService.updateDirectPurchase(directPurchasesForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteMaterialReceiptDetailsByCode(@PathVariable(name = "id") Long id,@ModelAttribute("directPurchasesForm") DirectPurchasesForm directPurchasesForm) {

		System.out.println("DELETING ID =========== "+id);
		ModelAndView modelAndView = new ModelAndView();
		directPurchasesForm.setDPId(id);
		directPurchaseService.deleteDirectPurchase(directPurchasesForm);
		setDefaultDataForMaterialReceiptPage(modelAndView);
		findMaterialReceiptDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}
	
	private ModelAndView setDefaultDataForMaterialReceiptPage(ModelAndView modelAndView) {

		modelAndView.addObject("directPurchasesForm", new DirectPurchasesForm());

		HashMap<String, String> typesOfCustomersMap = new HashMap<String, String>();

		typesOfCustomersMap.put("customertype1", "Customer Type 1");
		typesOfCustomersMap.put("customertype2", "Customer Type 2");
		typesOfCustomersMap.put("customertype3", "Customer Type 3");
		typesOfCustomersMap.put("customertype4", "Customer Type 4");
		typesOfCustomersMap.put("customertype5", "Customer Type 5");

		modelAndView.addObject("typesOfCustomersMap", typesOfCustomersMap);
		modelAndView.addObject("directPurchaseURL", ""
				+ "direct/purchase/save");
		modelAndView.setViewName(MastersPageConstants.DIRECT_PURCHASE_MAIN_PAGE);
		return modelAndView;
	}

}
