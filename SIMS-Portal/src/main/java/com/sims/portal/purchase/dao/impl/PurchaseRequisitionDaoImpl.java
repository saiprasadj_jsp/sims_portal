package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.beans.PurchaseRequisitionForm;
import com.sims.portal.purchase.dao.PurchaseRequisitionDao;

@Repository
@Transactional
public class PurchaseRequisitionDaoImpl implements PurchaseRequisitionDao {

	@PersistenceContext
	private EntityManager entityManager;

	public PurchaseRequisitionForm savePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm) {

		entityManager.persist(PurchaseRequisitionForm);

		return PurchaseRequisitionForm;
	}

	@Override
	public PurchaseRequisitionForm updatePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm) {
		
		System.out.println("Merged ID BEFORE ====== "+PurchaseRequisitionForm.getPRId());
		entityManager.merge(PurchaseRequisitionForm);
		System.out.println("Merged ID AFTER ====== "+PurchaseRequisitionForm.getPRId());
		
		return PurchaseRequisitionForm;
	}
	
	
	@Override
	public Boolean deletePurchaseRequisition(PurchaseRequisitionForm PurchaseRequisitionForm) {
		
		entityManager.remove(PurchaseRequisitionForm);
		return true;
	}
	
	@Override
	public List<PurchaseRequisitionForm> findPurchaseRequisitionDetails() {

		List<PurchaseRequisitionForm> PurchaseRequisitionFormList = entityManager
				.createQuery("SELECT PurchaseRequisitionForm FROM PurchaseRequisitionForm PurchaseRequisitionForm ",
						PurchaseRequisitionForm.class)
				.getResultList();

		return PurchaseRequisitionFormList;
	}

	@Override
	public PurchaseRequisitionForm findPurchaseRequisitionDetailsById(Long id) {

		List<PurchaseRequisitionForm> PurchaseRequisitionForm = entityManager.createQuery(
				"SELECT PurchaseRequisitionForm FROM PurchaseRequisitionForm PurchaseRequisitionForm WHERE PurchaseRequisitionForm.id='"
						+id+"'",
				PurchaseRequisitionForm.class).getResultList();

		if (PurchaseRequisitionForm.size() > 0) {

			return PurchaseRequisitionForm.get(0);
		}

		return new PurchaseRequisitionForm();
	}
}
