package com.sims.portal.purchase.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.model.purchase.beans.PurchaseOrderForm;
import com.sims.portal.purchase.dao.PurchaseOrderDao;

@Repository
@Transactional
public class PurchaseOrderDaoImpl implements PurchaseOrderDao {

	@PersistenceContext
	private EntityManager entityManager;

	public PurchaseOrderForm savePurchaseOrder(PurchaseOrderForm PurchaseOrderForm) {

		entityManager.persist(PurchaseOrderForm);

		return PurchaseOrderForm;
	}

	@Override
	public PurchaseOrderForm updatePurchaseOrder(PurchaseOrderForm PurchaseOrderForm) {
		
		System.out.println("Merged ID BEFORE ====== "+PurchaseOrderForm.getPOId());
		entityManager.merge(PurchaseOrderForm);
		System.out.println("Merged ID AFTER ====== "+PurchaseOrderForm.getPOId());
		
		return PurchaseOrderForm;
	}
	
	
	@Override
	public Boolean deletePurchaseOrder(PurchaseOrderForm PurchaseOrderForm) {
		
		entityManager.remove(PurchaseOrderForm);
		return true;
	}
	
	@Override
	public List<PurchaseOrderForm> findPurchaseOrderDetails() {

		List<PurchaseOrderForm> PurchaseOrderFormList = entityManager
				.createQuery("SELECT PurchaseOrderForm FROM PurchaseOrderForm PurchaseOrderForm ",
						PurchaseOrderForm.class)
				.getResultList();

		return PurchaseOrderFormList;
	}

	@Override
	public PurchaseOrderForm findPurchaseOrderDetailsByCode(Long id) {

		List<PurchaseOrderForm> PurchaseOrderForm = entityManager.createQuery(
				"SELECT PurchaseOrderForm FROM PurchaseOrderForm PurchaseOrderForm WHERE PurchaseOrderForm.POId="+id,
				PurchaseOrderForm.class).getResultList();

		if (PurchaseOrderForm.size() > 0) {

			return PurchaseOrderForm.get(0);
		}

		return new PurchaseOrderForm();
	}
}
