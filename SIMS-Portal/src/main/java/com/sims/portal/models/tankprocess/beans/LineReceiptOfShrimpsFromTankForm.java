package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_receiptofshrimpsfromtank_line")
public class LineReceiptOfShrimpsFromTankForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineReceiptOfShrimpsFromTank_id;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTank;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getLineReceiptOfShrimpsFromTank_id() {
		return lineReceiptOfShrimpsFromTank_id;
	}

	public void setLineReceiptOfShrimpsFromTank_id(Long lineReceiptOfShrimpsFromTank_id) {
		this.lineReceiptOfShrimpsFromTank_id = lineReceiptOfShrimpsFromTank_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne
	@JoinColumn(name = "receiptofshrimpsfromtank_id")
	public ReceiptOfShrimpsFromTankForm getReceiptOfShrimpsFromTank() {
		return receiptOfShrimpsFromTank;
	}

	public void setReceiptOfShrimpsFromTank(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTank) {
		this.receiptOfShrimpsFromTank = receiptOfShrimpsFromTank;
	}

}
