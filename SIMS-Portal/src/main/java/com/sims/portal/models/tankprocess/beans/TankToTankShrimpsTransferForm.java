package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_tanktotankshrimpstransfer")
public class TankToTankShrimpsTransferForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long tanktotankshrimpstransfer_id;
	private String departmentCode;
	private String departmentName;
	private String transferStartDateAndTime;
	private String fromTankName;
	private String fromTankCode;
	private String transferCompletedDateAndTime;
	private String employeeCode;
	private String employeeName;
	private Integer manPowerUsed;
	private String presentDate;
	private String warehouseCode;
	private String warehouseName;
	private String narration;
	private Collection<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransfer;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getTanktotankshrimpstransfer_id() {
		return tanktotankshrimpstransfer_id;
	}

	public void setTanktotankshrimpstransfer_id(Long tanktotankshrimpstransfer_id) {
		this.tanktotankshrimpstransfer_id = tanktotankshrimpstransfer_id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTransferStartDateAndTime() {
		return transferStartDateAndTime;
	}

	public void setTransferStartDateAndTime(String transferStartDateAndTime) {
		this.transferStartDateAndTime = transferStartDateAndTime;
	}

	public String getFromTankName() {
		return fromTankName;
	}

	public void setFromTankName(String fromTankName) {
		this.fromTankName = fromTankName;
	}

	public String getFromTankCode() {
		return fromTankCode;
	}

	public void setFromTankCode(String fromTankCode) {
		this.fromTankCode = fromTankCode;
	}

	public String getTransferCompletedDateAndTime() {
		return transferCompletedDateAndTime;
	}

	public void setTransferCompletedDateAndTime(String transferCompletedDateAndTime) {
		this.transferCompletedDateAndTime = transferCompletedDateAndTime;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getManPowerUsed() {
		return manPowerUsed;
	}

	public void setManPowerUsed(Integer manPowerUsed) {
		this.manPowerUsed = manPowerUsed;
	}

	public String getPresentDate() {
		return presentDate;
	}

	public void setPresentDate(String presentDate) {
		this.presentDate = presentDate;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "tankToTankShrimpsTransfer")
	public Collection<LineTankToTankShrimpsTransferForm> getLineTankToTankShrimpsTransfer() {
		return lineTankToTankShrimpsTransfer;
	}

	public void setLineTankToTankShrimpsTransfer(
			Collection<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransfer) {
		this.lineTankToTankShrimpsTransfer = lineTankToTankShrimpsTransfer;
	}

}
