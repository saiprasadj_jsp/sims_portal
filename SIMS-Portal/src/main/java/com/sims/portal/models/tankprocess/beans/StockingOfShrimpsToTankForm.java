package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_stockingofshrimpstotank")
public class StockingOfShrimpsToTankForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long stockingofshrimpstotank_id;
	private String departmentCode;
	private String departmentName;
	private String stackingStartDateAndTime;
	private String tankName;
	private String tankCode;
	private String stackingCompletedDateAndTime;
	private String employeeCode;
	private String employeeName;
	private Integer manPowerUsed;
	private String presentDate;
	private String warehouseCode;
	private String warehouseName;
	private String narration;
	private Collection<LineStockingOfShrimpsToTankForm> lineStockingOfShrimpsToTankForm;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getStockingofshrimpstotank_id() {
		return stockingofshrimpstotank_id;
	}

	public void setStockingofshrimpstotank_id(Long stockingofshrimpstotank_id) {
		this.stockingofshrimpstotank_id = stockingofshrimpstotank_id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getStackingStartDateAndTime() {
		return stackingStartDateAndTime;
	}

	public void setStackingStartDateAndTime(String stackingStartDateAndTime) {
		this.stackingStartDateAndTime = stackingStartDateAndTime;
	}

	public String getTankName() {
		return tankName;
	}

	public void setTankName(String tankName) {
		this.tankName = tankName;
	}

	public String getTankCode() {
		return tankCode;
	}

	public void setTankCode(String tankCode) {
		this.tankCode = tankCode;
	}

	public String getStackingCompletedDateAndTime() {
		return stackingCompletedDateAndTime;
	}

	public void setStackingCompletedDateAndTime(String stackingCompletedDateAndTime) {
		this.stackingCompletedDateAndTime = stackingCompletedDateAndTime;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getManPowerUsed() {
		return manPowerUsed;
	}

	public void setManPowerUsed(Integer manPowerUsed) {
		this.manPowerUsed = manPowerUsed;
	}

	public String getPresentDate() {
		return presentDate;
	}

	public void setPresentDate(String presentDate) {
		this.presentDate = presentDate;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "stockingOfShrimpsToTankForm")
	public Collection<LineStockingOfShrimpsToTankForm> getLineStockingOfShrimpsToTankForm() {
		return lineStockingOfShrimpsToTankForm;
	}

	public void setLineStockingOfShrimpsToTankForm(
			Collection<LineStockingOfShrimpsToTankForm> lineStockingOfShrimpsToTankForm) {
		this.lineStockingOfShrimpsToTankForm = lineStockingOfShrimpsToTankForm;
	}

}
