package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_receiptofshrimpsfromtank")
public class ReceiptOfShrimpsFromTankForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long receiptofshrimpsfromtank_id;
	private String departmentCode;
	private String departmentName;
	private String removingStartDateAndTime;
	private String tankName;
	private String tankCode;
	private String removingCompletedDateAndTime;
	private String employeeCode;
	private String employeeName;
	private Integer manPowerUsed;
	private String presentDate;
	private String warehouseCode;
	private String warehouseName;
	private Long issueDocNumber;
	private String issueDocDate;
	private String narration;
	private Collection<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTank;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getReceiptofshrimpsfromtank_id() {
		return receiptofshrimpsfromtank_id;
	}

	public void setReceiptofshrimpsfromtank_id(Long receiptofshrimpsfromtank_id) {
		this.receiptofshrimpsfromtank_id = receiptofshrimpsfromtank_id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getRemovingStartDateAndTime() {
		return removingStartDateAndTime;
	}

	public void setRemovingStartDateAndTime(String removingStartDateAndTime) {
		this.removingStartDateAndTime = removingStartDateAndTime;
	}

	public String getTankName() {
		return tankName;
	}

	public void setTankName(String tankName) {
		this.tankName = tankName;
	}

	public String getTankCode() {
		return tankCode;
	}

	public void setTankCode(String tankCode) {
		this.tankCode = tankCode;
	}

	public String getRemovingCompletedDateAndTime() {
		return removingCompletedDateAndTime;
	}

	public void setRemovingCompletedDateAndTime(String removingCompletedDateAndTime) {
		this.removingCompletedDateAndTime = removingCompletedDateAndTime;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getManPowerUsed() {
		return manPowerUsed;
	}

	public void setManPowerUsed(Integer manPowerUsed) {
		this.manPowerUsed = manPowerUsed;
	}

	public String getPresentDate() {
		return presentDate;
	}

	public void setPresentDate(String presentDate) {
		this.presentDate = presentDate;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public Long getIssueDocNumber() {
		return issueDocNumber;
	}

	public void setIssueDocNumber(Long issueDocNumber) {
		this.issueDocNumber = issueDocNumber;
	}

	public String getIssueDocDate() {
		return issueDocDate;
	}

	public void setIssueDocDate(String issueDocDate) {
		this.issueDocDate = issueDocDate;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "receiptOfShrimpsFromTank")
	public Collection<LineReceiptOfShrimpsFromTankForm> getLineReceiptOfShrimpsFromTank() {
		return lineReceiptOfShrimpsFromTank;
	}

	public void setLineReceiptOfShrimpsFromTank(Collection<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTank) {
		this.lineReceiptOfShrimpsFromTank = lineReceiptOfShrimpsFromTank;
	}
}
