package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class LineTankToTankShrimpsTransferDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineStockingOfShrimpsToTank_id;
	private String toTankName;
	private String toTankCode;
	private String toWarehouseCode;
	private String toWarehouseName;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private String updateURL;
	private String deleteURL;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getLineStockingOfShrimpsToTank_id() {
		return lineStockingOfShrimpsToTank_id;
	}

	public void setLineStockingOfShrimpsToTank_id(Long lineStockingOfShrimpsToTank_id) {
		this.lineStockingOfShrimpsToTank_id = lineStockingOfShrimpsToTank_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LineTankToTankShrimpsTransferDTO getLineStockingOfShrimpsDTO(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		this.setLineStockingOfShrimpsToTank_id(lineTankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer_id());
		this.setToTankCode(lineTankToTankShrimpsTransferForm.getToTankCode());
		this.setToTankName(lineTankToTankShrimpsTransferForm.getToTankName());
		this.setToWarehouseCode(lineTankToTankShrimpsTransferForm.getToWarehouseCode());
		this.setToWarehouseName(lineTankToTankShrimpsTransferForm.getToWarehouseName());
		this.setProductCode(lineTankToTankShrimpsTransferForm.getProductCode());
		this.setProductDescription(lineTankToTankShrimpsTransferForm.getProductDescription());
		this.setProductName(lineTankToTankShrimpsTransferForm.getProductName());
		this.setQuantity(lineTankToTankShrimpsTransferForm.getQuantity());
		this.setRemarks(lineTankToTankShrimpsTransferForm.getRemarks());
		this.setUOM(lineTankToTankShrimpsTransferForm.getUOM());

		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}

	public String getToTankName() {
		return toTankName;
	}

	public void setToTankName(String toTankName) {
		this.toTankName = toTankName;
	}

	public String getToTankCode() {
		return toTankCode;
	}

	public void setToTankCode(String toTankCode) {
		this.toTankCode = toTankCode;
	}

	public String getToWarehouseCode() {
		return toWarehouseCode;
	}

	public void setToWarehouseCode(String toWarehouseCode) {
		this.toWarehouseCode = toWarehouseCode;
	}

	public String getToWarehouseName() {
		return toWarehouseName;
	}

	public void setToWarehouseName(String toWarehouseName) {
		this.toWarehouseName = toWarehouseName;
	}
}
