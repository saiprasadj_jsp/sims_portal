package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

public class LineReceiptOfShrimpsFromTankDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineReceiptOfShrimpsFromTank_id;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private String updateURL;
	private String deleteURL;

	public Long getLineReceiptOfShrimpsFromTank_id() {
		return lineReceiptOfShrimpsFromTank_id;
	}

	public void setLineReceiptOfShrimpsFromTank_id(Long lineReceiptOfShrimpsFromTank_id) {
		this.lineReceiptOfShrimpsFromTank_id = lineReceiptOfShrimpsFromTank_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LineReceiptOfShrimpsFromTankDTO getLineStockingOfShrimpsDTO(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTank) {

		this.setLineReceiptOfShrimpsFromTank_id(lineReceiptOfShrimpsFromTank.getLineReceiptOfShrimpsFromTank_id());
		this.setProductCode(lineReceiptOfShrimpsFromTank.getProductCode());
		this.setProductDescription(lineReceiptOfShrimpsFromTank.getProductDescription());
		this.setProductName(lineReceiptOfShrimpsFromTank.getProductName());
		this.setQuantity(lineReceiptOfShrimpsFromTank.getQuantity());
		this.setRemarks(lineReceiptOfShrimpsFromTank.getRemarks());
		this.setUOM(lineReceiptOfShrimpsFromTank.getUOM());

		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}
}
