package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_dailytankrecording")
public class DailyTankRecordingForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long dailytankrecording_id;
	private String siteName;
	private String tankName;
	private String documentDate;
	private Long doc;
	private String supervisor;
	private String remarks;
	private String phAM;
	private String phPM;
	private Integer doNumber;
	private String mortality;
	private String labreportAttachment;
	private String feedWareHouse;
	private String feedMedicineWarehouse;
	private Double totalFeedQantity;
	private Double totalMedicineQantity;

	private String feed1Name;
	private Double feed1Quantity;
	private String feed1MedicineName;
	private Double feed1MedicineQuantity;
	private String feed1ConsumedPercentage;
	private String feed1MedicineTimings;

	private String feed2Name;
	private Double feed2Quantity;
	private String feed2MedicineName;
	private Double feed2MedicineQuantity;
	private String feed2ConsumedPercentage;
	private String feed2MedicineTimings;

	private String feed3Name;
	private Double feed3Quantity;
	private String feed3MedicineName;
	private Double feed3MedicineQuantity;
	private String feed3ConsumedPercentage;
	private String feed3MedicineTimings;

	private String feed4Name;
	private Double feed4Quantity;
	private String feed4MedicineName;
	private Double feed4MedicineQuantity;
	private String feed4ConsumedPercentage;
	private String feed4MedicineTimings;

	/*
	 * @OneToMany(mappedBy = "dailyTankRecordingForm") private
	 * Set<DailyTankFeedingForm> dailyTankFeeding;
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getDailytankrecording_id() {
		return dailytankrecording_id;
	}

	public void setDailytankrecording_id(Long dailytankrecording_id) {
		this.dailytankrecording_id = dailytankrecording_id;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getTankName() {
		return tankName;
	}

	public void setTankName(String tankName) {
		this.tankName = tankName;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public Long getDoc() {
		return doc;
	}

	public void setDoc(Long doc) {
		this.doc = doc;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhAM() {
		return phAM;
	}

	public void setPhAM(String phAM) {
		this.phAM = phAM;
	}

	public String getPhPM() {
		return phPM;
	}

	public void setPhPM(String phPM) {
		this.phPM = phPM;
	}

	public Integer getDoNumber() {
		return doNumber;
	}

	public void setDoNumber(Integer doNumber) {
		this.doNumber = doNumber;
	}

	public String getMortality() {
		return mortality;
	}

	public void setMortality(String mortality) {
		this.mortality = mortality;
	}

	public String getLabreportAttachment() {
		return labreportAttachment;
	}

	public void setLabreportAttachment(String labreportAttachment) {
		this.labreportAttachment = labreportAttachment;
	}

	public String getFeed1Name() {
		return feed1Name;
	}

	public void setFeed1Name(String feed1Name) {
		this.feed1Name = feed1Name;
	}

	public Double getFeed1Quantity() {
		return feed1Quantity;
	}

	public void setFeed1Quantity(Double feed1Quantity) {
		this.feed1Quantity = feed1Quantity;
	}

	public String getFeed1MedicineName() {
		return feed1MedicineName;
	}

	public void setFeed1MedicineName(String feed1MedicineName) {
		this.feed1MedicineName = feed1MedicineName;
	}

	public Double getFeed1MedicineQuantity() {
		return feed1MedicineQuantity;
	}

	public void setFeed1MedicineQuantity(Double feed1MedicineQuantity) {
		this.feed1MedicineQuantity = feed1MedicineQuantity;
	}

	public String getFeed1ConsumedPercentage() {
		return feed1ConsumedPercentage;
	}

	public void setFeed1ConsumedPercentage(String feed1ConsumedPercentage) {
		this.feed1ConsumedPercentage = feed1ConsumedPercentage;
	}

	public String getFeed1MedicineTimings() {
		return feed1MedicineTimings;
	}

	public void setFeed1MedicineTimings(String feed1MedicineTimings) {
		this.feed1MedicineTimings = feed1MedicineTimings;
	}

	public String getFeedWareHouse() {
		return feedWareHouse;
	}

	public void setFeedWareHouse(String feedWareHouse) {
		this.feedWareHouse = feedWareHouse;
	}

	public String getFeedMedicineWarehouse() {
		return feedMedicineWarehouse;
	}

	public void setFeedMedicineWarehouse(String feedMedicineWarehouse) {
		this.feedMedicineWarehouse = feedMedicineWarehouse;
	}

	public String getFeed2Name() {
		return feed2Name;
	}

	public void setFeed2Name(String feed2Name) {
		this.feed2Name = feed2Name;
	}

	public Double getFeed2Quantity() {
		return feed2Quantity;
	}

	public void setFeed2Quantity(Double feed2Quantity) {
		this.feed2Quantity = feed2Quantity;
	}

	public String getFeed2MedicineName() {
		return feed2MedicineName;
	}

	public void setFeed2MedicineName(String feed2MedicineName) {
		this.feed2MedicineName = feed2MedicineName;
	}

	public Double getFeed2MedicineQuantity() {
		return feed2MedicineQuantity;
	}

	public void setFeed2MedicineQuantity(Double feed2MedicineQuantity) {
		this.feed2MedicineQuantity = feed2MedicineQuantity;
	}

	public String getFeed2ConsumedPercentage() {
		return feed2ConsumedPercentage;
	}

	public void setFeed2ConsumedPercentage(String feed2ConsumedPercentage) {
		this.feed2ConsumedPercentage = feed2ConsumedPercentage;
	}

	public String getFeed2MedicineTimings() {
		return feed2MedicineTimings;
	}

	public void setFeed2MedicineTimings(String feed2MedicineTimings) {
		this.feed2MedicineTimings = feed2MedicineTimings;
	}

	public String getFeed3Name() {
		return feed3Name;
	}

	public void setFeed3Name(String feed3Name) {
		this.feed3Name = feed3Name;
	}

	public Double getFeed3Quantity() {
		return feed3Quantity;
	}

	public void setFeed3Quantity(Double feed3Quantity) {
		this.feed3Quantity = feed3Quantity;
	}

	public String getFeed3MedicineName() {
		return feed3MedicineName;
	}

	public void setFeed3MedicineName(String feed3MedicineName) {
		this.feed3MedicineName = feed3MedicineName;
	}

	public Double getFeed3MedicineQuantity() {
		return feed3MedicineQuantity;
	}

	public void setFeed3MedicineQuantity(Double feed3MedicineQuantity) {
		this.feed3MedicineQuantity = feed3MedicineQuantity;
	}

	public String getFeed3ConsumedPercentage() {
		return feed3ConsumedPercentage;
	}

	public void setFeed3ConsumedPercentage(String feed3ConsumedPercentage) {
		this.feed3ConsumedPercentage = feed3ConsumedPercentage;
	}

	public String getFeed3MedicineTimings() {
		return feed3MedicineTimings;
	}

	public void setFeed3MedicineTimings(String feed3MedicineTimings) {
		this.feed3MedicineTimings = feed3MedicineTimings;
	}

	public String getFeed4Name() {
		return feed4Name;
	}

	public void setFeed4Name(String feed4Name) {
		this.feed4Name = feed4Name;
	}

	public Double getFeed4Quantity() {
		return feed4Quantity;
	}

	public void setFeed4Quantity(Double feed4Quantity) {
		this.feed4Quantity = feed4Quantity;
	}

	public String getFeed4MedicineName() {
		return feed4MedicineName;
	}

	public void setFeed4MedicineName(String feed4MedicineName) {
		this.feed4MedicineName = feed4MedicineName;
	}

	public Double getFeed4MedicineQuantity() {
		return feed4MedicineQuantity;
	}

	public void setFeed4MedicineQuantity(Double feed4MedicineQuantity) {
		this.feed4MedicineQuantity = feed4MedicineQuantity;
	}

	public String getFeed4ConsumedPercentage() {
		return feed4ConsumedPercentage;
	}

	public void setFeed4ConsumedPercentage(String feed4ConsumedPercentage) {
		this.feed4ConsumedPercentage = feed4ConsumedPercentage;
	}

	public String getFeed4MedicineTimings() {
		return feed4MedicineTimings;
	}

	public void setFeed4MedicineTimings(String feed4MedicineTimings) {
		this.feed4MedicineTimings = feed4MedicineTimings;
	}

	public Double getTotalFeedQantity() {
		return totalFeedQantity;
	}

	public void setTotalFeedQantity(Double totalFeedQantity) {
		this.totalFeedQantity = totalFeedQantity;
	}

	public Double getTotalMedicineQantity() {
		return totalMedicineQantity;
	}

	public void setTotalMedicineQantity(Double totalMedicineQantity) {
		this.totalMedicineQantity = totalMedicineQantity;
	}

	/*
	 * public Set<DailyTankFeedingForm> getDailyTankFeeding() { return
	 * dailyTankFeeding; }
	 * 
	 * public void setDailyTankFeeding(Set<DailyTankFeedingForm>
	 * dailyTankFeeding) { this.dailyTankFeeding = dailyTankFeeding; }
	 */

}
