package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class LineStockingOfShrimpsToTankFormDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineStockingOfShrimpsToTank_id;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private String updateURL;
	private String deleteURL;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getLineStockingOfShrimpsToTank_id() {
		return lineStockingOfShrimpsToTank_id;
	}

	public void setLineStockingOfShrimpsToTank_id(Long lineStockingOfShrimpsToTank_id) {
		this.lineStockingOfShrimpsToTank_id = lineStockingOfShrimpsToTank_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LineStockingOfShrimpsToTankFormDTO getLineStockingOfShrimpsDTO(
			LineStockingOfShrimpsToTankForm lineStockingOfShrimpsToTankForm) {

		this.setLineStockingOfShrimpsToTank_id(lineStockingOfShrimpsToTankForm.getLineStockingOfShrimpsToTank_id());
		this.setProductCode(lineStockingOfShrimpsToTankForm.getProductCode());
		this.setProductDescription(lineStockingOfShrimpsToTankForm.getProductDescription());
		this.setProductName(lineStockingOfShrimpsToTankForm.getProductName());
		this.setQuantity(lineStockingOfShrimpsToTankForm.getQuantity());
		this.setRemarks(lineStockingOfShrimpsToTankForm.getRemarks());
		this.setUOM(lineStockingOfShrimpsToTankForm.getUOM());

		return this;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	public String getDeleteURL() {
		return deleteURL;
	}

	public void setDeleteURL(String deleteURL) {
		this.deleteURL = deleteURL;
	}
}
