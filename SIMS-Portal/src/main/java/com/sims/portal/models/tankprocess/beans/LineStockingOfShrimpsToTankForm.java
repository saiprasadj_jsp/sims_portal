package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_stockingofshrimpstotank_line")
public class LineStockingOfShrimpsToTankForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineStockingOfShrimpsToTank_id;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getLineStockingOfShrimpsToTank_id() {
		return lineStockingOfShrimpsToTank_id;
	}

	public void setLineStockingOfShrimpsToTank_id(Long lineStockingOfShrimpsToTank_id) {
		this.lineStockingOfShrimpsToTank_id = lineStockingOfShrimpsToTank_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne
	@JoinColumn(name = "stockingofshrimpstotank_id")
	public StockingOfShrimpsToTankForm getStockingOfShrimpsToTankForm() {
		return stockingOfShrimpsToTankForm;
	}

	public void setStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {
		this.stockingOfShrimpsToTankForm = stockingOfShrimpsToTankForm;
	}
}
