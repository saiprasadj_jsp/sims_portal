package com.sims.portal.models.tankprocess.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tank_operation_tanktotankshrimpstransfer_line")
public class LineTankToTankShrimpsTransferForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long lineTankToTankShrimpsTransfer_id;
	private String toTankCode;
	private String toTankName;
	private String toWarehouseCode;
	private String toWarehouseName;
	private String productName;
	private String productCode;
	private String productDescription;
	private String UOM;
	private Double quantity;
	private String remarks;
	private TankToTankShrimpsTransferForm tankToTankShrimpsTransfer;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getLineTankToTankShrimpsTransfer_id() {
		return lineTankToTankShrimpsTransfer_id;
	}

	public void setLineTankToTankShrimpsTransfer_id(Long lineTankToTankShrimpsTransfer_id) {
		this.lineTankToTankShrimpsTransfer_id = lineTankToTankShrimpsTransfer_id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne
	@JoinColumn(name = "tanktotankshrimpstransfer_id")
	public TankToTankShrimpsTransferForm getTankToTankShrimpsTransfer() {
		return tankToTankShrimpsTransfer;
	}

	public void setTankToTankShrimpsTransfer(TankToTankShrimpsTransferForm tankToTankShrimpsTransfer) {
		this.tankToTankShrimpsTransfer = tankToTankShrimpsTransfer;
	}

	public String getToTankCode() {
		return toTankCode;
	}

	public void setToTankCode(String toTankCode) {
		this.toTankCode = toTankCode;
	}

	public String getToTankName() {
		return toTankName;
	}

	public void setToTankName(String toTankName) {
		this.toTankName = toTankName;
	}

	public String getToWarehouseCode() {
		return toWarehouseCode;
	}

	public void setToWarehouseCode(String toWarehouseCode) {
		this.toWarehouseCode = toWarehouseCode;
	}

	public String getToWarehouseName() {
		return toWarehouseName;
	}

	public void setToWarehouseName(String toWarehouseName) {
		this.toWarehouseName = toWarehouseName;
	}

}
