package com.sims.portal.tankprocess.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;
import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.TankToTankShrimpsTransferService;

@Controller
@RequestMapping(value = TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_MAIN_URL)
public class TankToTankShrimpsTransferController {

	@Autowired
	private TankToTankShrimpsTransferService tankToTankShrimpsTransferService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showTankToTankShrimpsTransferDetailsForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllTankToTankShrimpsTransferDetailsForm(modelAndView);
		modelAndView.addObject("tankToTankShrimpsTransferForm", new TankToTankShrimpsTransferForm());
		modelAndView.addObject("lineTankToTankShrimpsTransferForm", new LineTankToTankShrimpsTransferForm());
		modelAndView.addObject("tankToTankShrimpsTransferURL",
				TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL);
		modelAndView.addObject("lineTankToTankShrimpsTransferToTankURL",
				TankProcessConstants.TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveAccountMaster(
			@ModelAttribute("tankToTankShrimpsTransferForm") TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		ModelAndView modelAndView = new ModelAndView();
		tankToTankShrimpsTransferService.saveTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findAllTankToTankShrimpsTransferDetailsForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findAllTankToTankShrimpsTransferDetailsForm(ModelAndView modelAndView) {

		List<TankToTankShrimpsTransferForm> tankToTankShrimpsTransferFormListData = tankToTankShrimpsTransferService
				.findTankToTankShrimpsTransferFormDetails();
		modelAndView.addObject("tankToTankShrimpsTransferFormListData", tankToTankShrimpsTransferFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForStockingOsShrimpsToTankPage(ModelAndView modelAndView) {

		modelAndView.addObject("tankToTankShrimpsTransferForm", new TankToTankShrimpsTransferForm());
		modelAndView.addObject("tankToTankShrimpsTransferURL",
				TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findStockingofshrimpsDetailsByID(@PathVariable(name = "id") Long tanktotankshrimpstransfer_id) {

		System.out.println("tanktotankshrimpstransfer_id Received &&&&&&&&&&&&&&  " + tanktotankshrimpstransfer_id);
		ModelAndView modelAndView = new ModelAndView();
		TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm = tankToTankShrimpsTransferService
				.findTankToTankShrimpsTransferFormDetailsByID(tanktotankshrimpstransfer_id);
		modelAndView.addObject("tankToTankShrimpsTransferForm", tankToTankShrimpsTransferForm);
		findTankToTankShrimpsTransferFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		modelAndView.addObject("tankToTankShrimpsTransferURL",
				"tanktotankshrimpstransfer/update/" + tankToTankShrimpsTransferForm.getTanktotankshrimpstransfer_id());

		Collection<LineTankToTankShrimpsTransferForm> listOfLineTankToTankShrimpsTransfer = tankToTankShrimpsTransferForm
				.getLineTankToTankShrimpsTransfer();
		modelAndView.addObject("listOfLineTankToTankShrimpsTransferObj", listOfLineTankToTankShrimpsTransfer);

		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateTankToTankShrimpsTransferDetailsForm(
			@PathVariable(name = "id") Long tanktotankshrimpstransfer_id,
			@ModelAttribute("tankToTankShrimpsTransferForm") TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		System.out.println("UPDATING ID =========== " + tanktotankshrimpstransfer_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + tanktotankshrimpstransfer_id);
		tankToTankShrimpsTransferForm.setTanktotankshrimpstransfer_id(tanktotankshrimpstransfer_id);
		tankToTankShrimpsTransferService.updateTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findTankToTankShrimpsTransferFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		// modelAndView.setViewName("redirect:/master/account");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteTankToTankShrimpsTransferFormByID(
			@PathVariable(name = "id") Long tanktotankshrimpstransfer_id,
			@ModelAttribute("tankToTankShrimpsTransferForm") TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		System.out.println("DELETING ID =========== " + tanktotankshrimpstransfer_id);
		ModelAndView modelAndView = new ModelAndView();
		tankToTankShrimpsTransferForm.setTanktotankshrimpstransfer_id(tanktotankshrimpstransfer_id);
		tankToTankShrimpsTransferService.deleteTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findTankToTankShrimpsTransferFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}

	public ModelAndView findTankToTankShrimpsTransferFormDetails(ModelAndView modelAndView) {

		List<TankToTankShrimpsTransferForm> tankToTankShrimpsTransferFormList = tankToTankShrimpsTransferService
				.findTankToTankShrimpsTransferFormDetails();
		modelAndView.addObject("tankToTankShrimpsTransferFormListData", tankToTankShrimpsTransferFormList);
		return modelAndView;
	}

}
