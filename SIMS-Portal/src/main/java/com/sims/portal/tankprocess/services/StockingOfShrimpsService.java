package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.StockingOfShrimpsToTankForm;

public interface StockingOfShrimpsService {

	public StockingOfShrimpsToTankForm saveStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);

	public List<StockingOfShrimpsToTankForm> findStockingOfShrimpsToTankFormDetails();
	
	public StockingOfShrimpsToTankForm findStockingOfShrimpsDetailsByID(Long id);
	
	public StockingOfShrimpsToTankForm updateStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);
	
	public Boolean deleteStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);
}
