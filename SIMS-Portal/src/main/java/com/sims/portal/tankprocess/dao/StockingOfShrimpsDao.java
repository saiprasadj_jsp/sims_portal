package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.StockingOfShrimpsToTankForm;

public interface StockingOfShrimpsDao {

	public StockingOfShrimpsToTankForm saveStockingOfShrimpsToTankForm(
			StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);

	public List<StockingOfShrimpsToTankForm> findStockingOfShrimpsToTankForm();

	public StockingOfShrimpsToTankForm findStockingOfShrimpsToTankFormByID(Long stockingofshrimpstotank_id);

	public StockingOfShrimpsToTankForm updateStockingOfShrimpsToTankForm(
			StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);

	public Boolean deleteStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm);

}
