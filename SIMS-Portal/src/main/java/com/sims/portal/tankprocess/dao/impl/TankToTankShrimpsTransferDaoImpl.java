package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.dao.TankToTankShrimpsTransferDao;

@Repository
@Transactional
public class TankToTankShrimpsTransferDaoImpl implements TankToTankShrimpsTransferDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public TankToTankShrimpsTransferForm saveTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		entityManager.persist(tankToTankShrimpsTransferForm);
		return tankToTankShrimpsTransferForm;
	}

	@Override
	public List<TankToTankShrimpsTransferForm> findTankToTankShrimpsTransferForm() {

		List<TankToTankShrimpsTransferForm> tankToTankShrimpsTransferFormListData = entityManager.createQuery(
				"SELECT tankToTankShrimpsTransferForm FROM TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm ",
				TankToTankShrimpsTransferForm.class).getResultList();

		return tankToTankShrimpsTransferFormListData;
	}

	@Override
	public TankToTankShrimpsTransferForm findTankToTankShrimpsTransferFormByID(Long tanktotankshrimpstransfer_id) {

		List<TankToTankShrimpsTransferForm> tankToTankShrimpsTransferForm = entityManager
				.createQuery(
						"SELECT tankToTankShrimpsTransferForm FROM TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm WHERE tankToTankShrimpsTransferForm.tanktotankshrimpstransfer_id='"
								+ tanktotankshrimpstransfer_id + "'",
						TankToTankShrimpsTransferForm.class)
				.getResultList();

		if (tankToTankShrimpsTransferForm.size() > 0) {

			return tankToTankShrimpsTransferForm.get(0);
		}

		return new TankToTankShrimpsTransferForm();
	}

	@Override
	public TankToTankShrimpsTransferForm updateTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		System.out
				.println("Merged ID BEFORE ====== " + tankToTankShrimpsTransferForm.getTanktotankshrimpstransfer_id());
		entityManager.merge(tankToTankShrimpsTransferForm);
		System.out.println("Merged ID AFTER ====== " + tankToTankShrimpsTransferForm.getTanktotankshrimpstransfer_id());

		return tankToTankShrimpsTransferForm;
	}

	@Override
	public Boolean deleteTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		entityManager.remove(entityManager.merge(tankToTankShrimpsTransferForm));
		return true;
	}

}