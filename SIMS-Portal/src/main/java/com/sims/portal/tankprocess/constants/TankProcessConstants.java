package com.sims.portal.tankprocess.constants;

public class TankProcessConstants {

	// DAILY TANK RECORDING CONSTANTS
	public static final String TILES_DAILY_TANK_RECORDING_CONTROLLER_MAIN_URL = "/tankoperations/dailytankrecording";
	public static final String TILES_DAILY_TANK_RECORDING_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/dailytankrecording.page/DailyTankRecording";
	public static final String TILES_DAILY_TANK_RECORDING_CONTROLLER_SAVE_URL = "dailytankrecording/save";

	// STOCKING OF SHRIMPS CONSTANTS
	public static final String TILES_STOCKING_OF_SHRIMPS_CONTROLLER_MAIN_URL = "/tankoperations/stockingofshrimps";
	public static final String TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/stockingofshrimps.page/StockingOfShrimps";
	public static final String TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SAVE_URL = "stockingofshrimps/save";
	public static final String TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_MAIN_URL = "/tankoperations/linestockingofshrimps";
	public static final String TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/linestockingofshrimps.page/StockingOfShrimps";
	public static final String TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_SAVE_URL = "linestockingofshrimps/save";
	public static final String TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_UPDATE_URL = "/tankoperations/linestockingofshrimps/update/";
	public static final String TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_DELETE_URL = "/tankoperations/linestockingofshrimps/delete/";

	// RECEIPT OF SHRIMPS FROM TANK CONSTANTS
	public static final String TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_MAIN_URL = "/tankoperations/receiptofshrimpsfromtank";
	public static final String TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/receiptofshrimpsfromtank.page/ReceiptOfShrimps";
	public static final String TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL = "receiptofshrimpsfromtank/save";
	public static final String TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_UPDATE_URL = "receiptofshrimpsfromtank/update/";
	public static final String TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_DELETE_URL = "receiptofshrimpsfromtank/delete/";
	public static final String TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_MAIN_URL = "/tankoperations/linereceiptofshrimpsfromtank";
	public static final String TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/linereceiptofshrimpsfromtank.page/ReceiptOfShrimps";
	public static final String TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL = "linereceiptofshrimpsfromtank/save";
	public static final String TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_UPDATE_URL = "/tankoperations/linereceiptofshrimpsfromtank/update/";
	public static final String TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_DELETE_URL = "/tankoperations/linereceiptofshrimpsfromtank/delete/";

	// TANK TO TANK SHRIMPS TRANSFER CONSTANTS
	public static final String TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_MAIN_URL = "/tankoperations/tanktotankshrimpstransfer";
	public static final String TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/tanktotankshrimpstransfer.page/StockingOfShrimps";
	public static final String TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL = "tanktotankshrimpstransfer/save";
	public static final String TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_MAIN_URL = "/tankoperations/linetanktotankshrimpstransfer";
	public static final String TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP = "tankoperations/linetanktotankshrimpstransfer.page/StockingOfShrimps";
	public static final String TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL = "linetanktotankshrimpstransfer/save";
	public static final String TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_UPDATE_URL = "/tankoperations/linetanktotankshrimpstransfer/update/";
	public static final String TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_DELETE_URL = "/tankoperations/linetanktotankshrimpstransfer/delete/";


	// Purchase Order CONSTANT 
	
		public static final String TILES_LINE_PURCHASE_ORDER_MAIN_URL= "/admin/purchase/purchaseorder";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_MAIN_URL = "/admin/purchase/purchaseorderline";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SAVE_URL = "purchase/purchaseorderline/save";
		
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_SHOW_MAIN_JSP = "purchase/purchaseorder";
		
		
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_UPDATE_URL = "/admin/purchase/purchaseorderline/update/";
		public static final String TILES_LINE_PURCHASE_ORDER_LINE_CONTROLLER_DELETE_URL = "/admin/purchase/purchaseorderline/delete/";

		
		// Purchase Order CONSTANT 
		
		//	public static final String TILES_LINE_PURCHASE_ORDER_MAIN_URL= "/admin/purchase/purchaseorder";
			public static final String TILES_LINE_PURCHASE_REQ_LINE_MAIN_URL = "/admin/purchase/purchaserequisitionline";
			public static final String TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SAVE_URL = "purchase/purchaserequisitionline/save";			
			
			public static final String TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_SHOW_MAIN_JSP = "purchase/purchaserequisition";
			
			
			public static final String TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_UPDATE_URL = "/admin/purchase/purchaserequisitionline/update/";
			public static final String TILES_LINE_PURCHASE_REQ_LINE_CONTROLLER_DELETE_URL = "/admin/purchase/purchaserequisitionline/delete/";
			
			//MaterialReceiptLineController
			
			public static final String TILES_LINE_METERIAL_RECEIPT_LINE_MAIN_URL = "/admin/materialreceipt/materialreceiptline";
			public static final String TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SAVE_URL = "materialreceipt/materialreceiptline/save";
			
			public static final String TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_SHOW_MAIN_JSP = "meterial/receipt";
			
			public static final String TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_UPDATE_URL = "/admin/materialreceipt/materialreceiptline/update/";
			public static final String TILES_LINE_METERIAL_RECEIPT_LINE_CONTROLLER_DELETE_URL = "/admin/materialreceipt/materialreceiptline/delete/";
			
			
			
        //DirectPurchasesLineController DirectPurchasesLineController
			
			public static final String TILES_LINE_DIRECT_PURCHASE_LINE_MAIN_URL = "/admin/direct/purchaseline";
			public static final String TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SAVE_URL = "direct/purchaseline/save";
			
			public static final String TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_SHOW_MAIN_JSP = "direct/purchaseline";
			
			public static final String TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_UPDATE_URL = "/admin/direct/purchaseline/update/";
			public static final String TILES_LINE_DIRECT_PURCHASE_LINE_CONTROLLER_DELETE_URL = "/admin/direct/purchaseline/delete/";



}
