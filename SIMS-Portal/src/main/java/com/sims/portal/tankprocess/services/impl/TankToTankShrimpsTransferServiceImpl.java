package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.dao.TankToTankShrimpsTransferDao;
import com.sims.portal.tankprocess.services.TankToTankShrimpsTransferService;

@Service
public class TankToTankShrimpsTransferServiceImpl implements TankToTankShrimpsTransferService {

	@Autowired
	private TankToTankShrimpsTransferDao tankToTankShrimpsTransferDao;

	@Override
	public TankToTankShrimpsTransferForm saveTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		tankToTankShrimpsTransferDao.saveTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);
		return tankToTankShrimpsTransferForm;
	}

	@Override
	public List<TankToTankShrimpsTransferForm> findTankToTankShrimpsTransferFormDetails() {

		List<TankToTankShrimpsTransferForm> tankToTankShrimpsTransferFormListData = tankToTankShrimpsTransferDao
				.findTankToTankShrimpsTransferForm();

		return tankToTankShrimpsTransferFormListData;
	}

	@Override
	public TankToTankShrimpsTransferForm findTankToTankShrimpsTransferFormDetailsByID(Long id) {

		return tankToTankShrimpsTransferDao.findTankToTankShrimpsTransferFormByID(id);
	}

	@Override
	public TankToTankShrimpsTransferForm updateTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		tankToTankShrimpsTransferDao.updateTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);

		return tankToTankShrimpsTransferForm;
	}

	@Override
	public Boolean deleteTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm) {

		return tankToTankShrimpsTransferDao.deleteTankToTankShrimpsTransferForm(tankToTankShrimpsTransferForm);
	}
}
