package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.dao.ReceiptOfShrimpsFromTankDao;

@Repository
@Transactional
public class ReceiptOfShrimpsFromTankDaoImpl implements ReceiptOfShrimpsFromTankDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public ReceiptOfShrimpsFromTankForm saveReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		entityManager.persist(receiptOfShrimpsFromTankForm);
		return receiptOfShrimpsFromTankForm;
	}

	@Override
	public List<ReceiptOfShrimpsFromTankForm> findReceiptOfShrimpsFromTankForm() {

		List<ReceiptOfShrimpsFromTankForm> receiptOfShrimpsFromTankFormListData = entityManager.createQuery(
				"SELECT receiptOfShrimpsFromTankForm FROM ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm ",
				ReceiptOfShrimpsFromTankForm.class).getResultList();

		return receiptOfShrimpsFromTankFormListData;
	}

	@Override
	public ReceiptOfShrimpsFromTankForm findReceiptOfShrimpsFromTankFormByID(Long receiptofshrimpsfromtank_id) {

		List<ReceiptOfShrimpsFromTankForm> receiptOfShrimpsFromTankForm = entityManager
				.createQuery(
						"SELECT receiptOfShrimpsFromTankForm FROM ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm WHERE receiptOfShrimpsFromTankForm.receiptofshrimpsfromtank_id='"
								+ receiptofshrimpsfromtank_id + "'",
						ReceiptOfShrimpsFromTankForm.class)
				.getResultList();

		if (receiptOfShrimpsFromTankForm.size() > 0) {

			return receiptOfShrimpsFromTankForm.get(0);
		}

		return new ReceiptOfShrimpsFromTankForm();
	}

	@Override
	public ReceiptOfShrimpsFromTankForm updateReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		System.out.println("Merged ID BEFORE ====== " + receiptOfShrimpsFromTankForm.getReceiptofshrimpsfromtank_id());
		entityManager.merge(receiptOfShrimpsFromTankForm);
		System.out.println("Merged ID AFTER ====== " + receiptOfShrimpsFromTankForm.getReceiptofshrimpsfromtank_id());

		return receiptOfShrimpsFromTankForm;
	}

	@Override
	public Boolean deleteReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		entityManager.remove(entityManager.merge(receiptOfShrimpsFromTankForm));
		return true;
	}

}