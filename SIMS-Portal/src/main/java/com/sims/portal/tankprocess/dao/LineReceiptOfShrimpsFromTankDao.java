package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;

public interface LineReceiptOfShrimpsFromTankDao {

	public LineReceiptOfShrimpsFromTankForm saveLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);

	public List<LineReceiptOfShrimpsFromTankForm> findLineReceiptOfShrimpsFromTankForm();

	public LineReceiptOfShrimpsFromTankForm findLineReceiptOfShrimpsFromTankFormByID(
			Long lineStockingOfShrimpsToTank_id);

	public LineReceiptOfShrimpsFromTankForm updateLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);

	public Boolean deleteLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);

}
