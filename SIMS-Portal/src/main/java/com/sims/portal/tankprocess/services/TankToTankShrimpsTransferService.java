package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;

public interface TankToTankShrimpsTransferService {

	public TankToTankShrimpsTransferForm saveTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);

	public List<TankToTankShrimpsTransferForm> findTankToTankShrimpsTransferFormDetails();
	
	public TankToTankShrimpsTransferForm findTankToTankShrimpsTransferFormDetailsByID(Long id);
	
	public TankToTankShrimpsTransferForm updateTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);
	
	public Boolean deleteTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);
}
