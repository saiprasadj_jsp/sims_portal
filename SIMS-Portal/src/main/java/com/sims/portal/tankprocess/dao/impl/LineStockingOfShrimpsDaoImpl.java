package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;
import com.sims.portal.tankprocess.dao.LineStockingOfShrimpsDao;

@Repository
@Transactional
public class LineStockingOfShrimpsDaoImpl implements LineStockingOfShrimpsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LineStockingOfShrimpsToTankForm saveLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm) {

		entityManager.persist(LineStockingOfShrimpsToTankForm);
		return LineStockingOfShrimpsToTankForm;
	}

	@Override
	public List<LineStockingOfShrimpsToTankForm> findLineStockingOfShrimpsToTankForm() {

		List<LineStockingOfShrimpsToTankForm> LineStockingOfShrimpsToTankFormListData = entityManager.createQuery(
				"SELECT LineStockingOfShrimpsToTankForm FROM LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm ",
				LineStockingOfShrimpsToTankForm.class).getResultList();

		return LineStockingOfShrimpsToTankFormListData;
	}

	@Override
	public LineStockingOfShrimpsToTankForm findLineStockingOfShrimpsToTankFormByID(Long dailytankrecording_id) {

		List<LineStockingOfShrimpsToTankForm> LineStockingOfShrimpsToTankForm = entityManager.createQuery(
				"SELECT LineStockingOfShrimpsToTankForm FROM LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm WHERE LineStockingOfShrimpsToTankForm.lineStockingOfShrimpsToTank_id='"
						+ dailytankrecording_id + "'",
				LineStockingOfShrimpsToTankForm.class).getResultList();

		if (LineStockingOfShrimpsToTankForm.size() > 0) {

			return LineStockingOfShrimpsToTankForm.get(0);
		}

		return new LineStockingOfShrimpsToTankForm();
	}

	@Override
	public LineStockingOfShrimpsToTankForm updateLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm) {

		System.out.println("Merged ID BEFORE ====== " + LineStockingOfShrimpsToTankForm.getLineStockingOfShrimpsToTank_id());
		entityManager.merge(LineStockingOfShrimpsToTankForm);
		System.out.println("Merged ID AFTER ====== " + LineStockingOfShrimpsToTankForm.getLineStockingOfShrimpsToTank_id());

		return LineStockingOfShrimpsToTankForm;
	}

	@Override
	public Boolean deleteLineStockingOfShrimpsToTankForm(LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm) {

		entityManager.remove(entityManager.merge(LineStockingOfShrimpsToTankForm));
		return true;
	}

}