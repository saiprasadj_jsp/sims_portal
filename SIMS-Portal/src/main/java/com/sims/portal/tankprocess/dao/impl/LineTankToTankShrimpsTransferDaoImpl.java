package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.dao.LineTankToTankShrimpsTransferDao;

@Repository
@Transactional
public class LineTankToTankShrimpsTransferDaoImpl implements LineTankToTankShrimpsTransferDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LineTankToTankShrimpsTransferForm saveLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		entityManager.persist(lineTankToTankShrimpsTransferForm);
		return lineTankToTankShrimpsTransferForm;
	}

	@Override
	public List<LineTankToTankShrimpsTransferForm> findLineTankToTankShrimpsTransferForm() {

		List<LineTankToTankShrimpsTransferForm> LineTankToTankShrimpsTransferFormListData = entityManager.createQuery(
				"SELECT lineTankToTankShrimpsTransferForm FROM LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm ",
				LineTankToTankShrimpsTransferForm.class).getResultList();

		return LineTankToTankShrimpsTransferFormListData;
	}

	@Override
	public LineTankToTankShrimpsTransferForm findLineTankToTankShrimpsTransferFormByID(Long lineTankToTankShrimpsTransfer_id) {

		List<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransferForm = entityManager.createQuery(
				"SELECT lineTankToTankShrimpsTransferForm FROM LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm WHERE lineTankToTankShrimpsTransferForm.lineTankToTankShrimpsTransfer_id='"
						+ lineTankToTankShrimpsTransfer_id + "'",
				LineTankToTankShrimpsTransferForm.class).getResultList();

		if (lineTankToTankShrimpsTransferForm.size() > 0) {

			return lineTankToTankShrimpsTransferForm.get(0);
		}

		return new LineTankToTankShrimpsTransferForm();
	}

	@Override
	public LineTankToTankShrimpsTransferForm updateLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		System.out.println(
				"Merged ID BEFORE ====== " + lineTankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer_id());
		entityManager.merge(lineTankToTankShrimpsTransferForm);
		System.out.println(
				"Merged ID AFTER ====== " + lineTankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer_id());

		return lineTankToTankShrimpsTransferForm;
	}

	@Override
	public Boolean deleteLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		entityManager.remove(entityManager.merge(lineTankToTankShrimpsTransferForm));
		return true;
	}

}