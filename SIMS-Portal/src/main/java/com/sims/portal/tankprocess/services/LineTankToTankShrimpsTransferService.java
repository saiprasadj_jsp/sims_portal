package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;

public interface LineTankToTankShrimpsTransferService {

	public LineTankToTankShrimpsTransferForm saveLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);

	public List<LineTankToTankShrimpsTransferForm> findLineTankToTankShrimpsTransferFormDetails();

	public LineTankToTankShrimpsTransferForm findLineTankToTankShrimpsTransferDetailsByID(Long id);

	public LineTankToTankShrimpsTransferForm updateLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);

	public Boolean deleteLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);
}
