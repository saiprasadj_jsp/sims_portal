package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.DailyTankRecordingForm;
import com.sims.portal.tankprocess.dao.DailyTankRecordingDao;

@Repository
@Transactional
public class DailyTankRecordingDaoImpl implements DailyTankRecordingDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public DailyTankRecordingForm saveDailyTankRecording(DailyTankRecordingForm dailyTankRecordingForm) {

		entityManager.persist(dailyTankRecordingForm);
		return dailyTankRecordingForm;
	}

	@Override
	public List<DailyTankRecordingForm> findDailyTankRecordingFormDetails() {

		List<DailyTankRecordingForm> dailyTankRecordingFormListData = entityManager
				.createQuery("SELECT dailyTankRecordingForm FROM DailyTankRecordingForm dailyTankRecordingForm ",
						DailyTankRecordingForm.class)
				.getResultList();

		return dailyTankRecordingFormListData;
	}

	@Override
	public DailyTankRecordingForm findDailyTankRecordingDetailsByCode(Long dailytankrecording_id) {

		List<DailyTankRecordingForm> dailyTankRecordingForm = entityManager.createQuery(
				"SELECT dailyTankRecordingForm FROM DailyTankRecordingForm dailyTankRecordingForm WHERE dailyTankRecordingForm.dailytankrecording_id='"
						+ dailytankrecording_id + "'",
				DailyTankRecordingForm.class).getResultList();

		if (dailyTankRecordingForm.size() > 0) {

			return dailyTankRecordingForm.get(0);
		}

		return new DailyTankRecordingForm();
	}

	@Override
	public DailyTankRecordingForm updateDailyTankRecordingForm(DailyTankRecordingForm dailyTankRecordingForm) {

		System.out.println("Merged ID BEFORE ====== " + dailyTankRecordingForm.getDailytankrecording_id());
		entityManager.merge(dailyTankRecordingForm);
		System.out.println("Merged ID AFTER ====== " + dailyTankRecordingForm.getDailytankrecording_id());

		return dailyTankRecordingForm;
	}

	@Override
	public Boolean deleteDailyTankRecordingForm(DailyTankRecordingForm dailyTankRecordingForm) {

		entityManager.remove(entityManager.merge(dailyTankRecordingForm));
		return true;
	}

}
