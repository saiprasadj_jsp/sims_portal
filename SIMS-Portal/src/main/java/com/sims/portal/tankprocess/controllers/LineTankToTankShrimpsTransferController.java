package com.sims.portal.tankprocess.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferDTO;
import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;
import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.LineTankToTankShrimpsTransferService;
import com.sims.portal.tankprocess.services.TankToTankShrimpsTransferService;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_MAIN_URL)
public class LineTankToTankShrimpsTransferController {

	@Autowired
	private TankToTankShrimpsTransferService tankToTankShrimpsTransferService;

	@Autowired
	private LineTankToTankShrimpsTransferService lineTankToTankShrimpsTransferService;

	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;

	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLineTankToTankShrimpsTransferDetails() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLineTankToTankShrimpsTransferDetails(modelAndView);
		modelAndView.addObject("lineTankToTankShrimpsTransferForm", new LineTankToTankShrimpsTransferForm());
		modelAndView.addObject("tankToTankShrimpsTransferURL",
				TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
	public void saveAccountMaster(@PathVariable(name = "id") Long tanktotankshrimpstransfer_id,
			@RequestParam("toTankCode") String toTankCode, @RequestParam("toTankName") String toTankName,
			@RequestParam("toWarehouseCode") String toWarehouseCode,
			@RequestParam("toWarehouseName") String toWarehouseName, @RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm = tankToTankShrimpsTransferService
				.findTankToTankShrimpsTransferFormDetailsByID(tanktotankshrimpstransfer_id);

		System.out.println(
				"List Size ======= " + tankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer().size());
		LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm = new LineTankToTankShrimpsTransferForm();
		lineTankToTankShrimpsTransferForm.setTankToTankShrimpsTransfer(tankToTankShrimpsTransferForm);
		setLineTankToTankShrimpsTransferData(toTankCode, toTankName, toWarehouseCode, toWarehouseName, productCode,
				productName, productDescription, UOM, quantity, remarks, lineTankToTankShrimpsTransferForm);

		ModelAndView modelAndView = new ModelAndView();
		lineTankToTankShrimpsTransferService.saveLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);
		setDefaultDataForTankToTankShrimpsTransferPage(modelAndView);
		findAllLineTankToTankShrimpsTransferDetails(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

	}

	private void setLineTankToTankShrimpsTransferData(String toTankCode, String toTankName, String toWarehouseCode,
			String toWarehouseName, String productCode, String productName, String productDescription, String UOM,
			Double quantity, String remarks, LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {
		lineTankToTankShrimpsTransferForm.setToTankCode(toTankCode);
		lineTankToTankShrimpsTransferForm.setToTankName(toTankName);
		lineTankToTankShrimpsTransferForm.setToWarehouseCode(toWarehouseCode);
		lineTankToTankShrimpsTransferForm.setToWarehouseName(toWarehouseName);
		lineTankToTankShrimpsTransferForm.setProductCode(productCode);
		lineTankToTankShrimpsTransferForm.setProductDescription(productDescription);
		lineTankToTankShrimpsTransferForm.setProductName(productName);
		lineTankToTankShrimpsTransferForm.setQuantity(quantity);
		lineTankToTankShrimpsTransferForm.setUOM(UOM);
		lineTankToTankShrimpsTransferForm.setRemarks(remarks);
	}

	public ModelAndView findAllLineTankToTankShrimpsTransferDetails(ModelAndView modelAndView) {

		List<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransferFormListData = lineTankToTankShrimpsTransferService
				.findLineTankToTankShrimpsTransferFormDetails();
		modelAndView.addObject("lineTankToTankShrimpsTransferFormListData", lineTankToTankShrimpsTransferFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForTankToTankShrimpsTransferPage(ModelAndView modelAndView) {

		modelAndView.addObject("lineTankToTankShrimpsTransferForm", new LineTankToTankShrimpsTransferForm());
		modelAndView.addObject("tankToTankShrimpsTransferURL",
				TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public LineTankToTankShrimpsTransferDTO findLineTankToTankShrimpsTransferDetailsByID(
			@PathVariable(name = "id") Long lineTankToTankShrimpsTransferForm_id) {

		System.out.println("lineTankToTankShrimpsTransferForm_id Received &&&&&&&&&&&&&&  "
				+ lineTankToTankShrimpsTransferForm_id);
		ModelAndView modelAndView = new ModelAndView();
		LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm = lineTankToTankShrimpsTransferService
				.findLineTankToTankShrimpsTransferDetailsByID(lineTankToTankShrimpsTransferForm_id);
		modelAndView.addObject("lineTankToTankShrimpsTransferForm", lineTankToTankShrimpsTransferForm);
		// findLineTankToTankShrimpsTransferFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		LineTankToTankShrimpsTransferDTO dto = new LineTankToTankShrimpsTransferDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_UPDATE_URL
				+ lineTankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer_id());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_TANK_TO_TANK_SHRIMPS_TRANSFER_CONTROLLER_DELETE_URL
				+ lineTankToTankShrimpsTransferForm.getLineTankToTankShrimpsTransfer_id());
		return dto.getLineStockingOfShrimpsDTO(lineTankToTankShrimpsTransferForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLineTankToTankShrimpsTransferForm(@PathVariable(name = "id") Long tanktotankshrimpstransfer_id,
			@RequestParam("toTankCode") String toTankCode, @RequestParam("toTankName") String toTankName,
			@RequestParam("toWarehouseCode") String toWarehouseCode,
			@RequestParam("toWarehouseName") String toWarehouseName, @RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("UPDATING Line ID =========== " + tanktotankshrimpstransfer_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + tanktotankshrimpstransfer_id);

		LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm = lineTankToTankShrimpsTransferService
				.findLineTankToTankShrimpsTransferDetailsByID(tanktotankshrimpstransfer_id);
		setLineTankToTankShrimpsTransferData(toTankCode, toTankName, toWarehouseCode, toWarehouseName, productCode,
				productName, productDescription, UOM, quantity, remarks, lineTankToTankShrimpsTransferForm);
		lineTankToTankShrimpsTransferService.updateLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);
		setDefaultDataForTankToTankShrimpsTransferPage(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deleteLineTankToTankShrimpsTransferFormByID(@PathVariable(name = "id") Long tanktotankshrimpstransfer_id,
			@RequestParam("toTankCode") String toTankCode, @RequestParam("toTankName") String toTankName,
			@RequestParam("toWarehouseCode") String toWarehouseCode,
			@RequestParam("toWarehouseName") String toWarehouseName, @RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("DELETING ID =========== " + tanktotankshrimpstransfer_id);
		ModelAndView modelAndView = new ModelAndView();
		LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm = lineTankToTankShrimpsTransferService
				.findLineTankToTankShrimpsTransferDetailsByID(tanktotankshrimpstransfer_id);
		lineTankToTankShrimpsTransferService.deleteLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findLineTankToTankShrimpsTransferFormDetails(ModelAndView modelAndView) {

		List<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransferFormList = lineTankToTankShrimpsTransferService
				.findLineTankToTankShrimpsTransferFormDetails();
		modelAndView.addObject("lineTankToTankShrimpsTransferFormListData", lineTankToTankShrimpsTransferFormList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}