package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;

public interface LineReceiptOfShrimpsFromTankService {

	public LineReceiptOfShrimpsFromTankForm saveLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);

	public List<LineReceiptOfShrimpsFromTankForm> findLineReceiptOfShrimpsFromTankFormDetails();

	public LineReceiptOfShrimpsFromTankForm findLineReceiptOfShrimpsFromTankDetailsByID(Long id);

	public LineReceiptOfShrimpsFromTankForm updateLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);

	public Boolean deleteLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm);
}
