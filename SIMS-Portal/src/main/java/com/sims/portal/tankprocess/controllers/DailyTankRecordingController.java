package com.sims.portal.tankprocess.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.models.tankprocess.beans.DailyTankRecordingForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.DailyTankRecordingService;

@Controller
@RequestMapping(value = TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_MAIN_URL)
public class DailyTankRecordingController {

	@Autowired
	private DailyTankRecordingService dailyTankRecordingService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showDailyTankRecordingForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllDailyTankRecordingForm(modelAndView);
		modelAndView.addObject("dailyTankRecordingForm", new DailyTankRecordingForm());
		modelAndView.addObject("dailyTankRecordingURL",
				TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveAccountMaster(
			@ModelAttribute("dailyTankRecordingForm") DailyTankRecordingForm dailyTankRecordingForm) {

		ModelAndView modelAndView = new ModelAndView();
		dailyTankRecordingService.saveDailyTankRecordingForm(dailyTankRecordingForm);
		setDefaultDataForDailyTankRecordingPage(modelAndView);
		findAllDailyTankRecordingForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findAllDailyTankRecordingForm(ModelAndView modelAndView) {

		List<DailyTankRecordingForm> dailyTankRecordingFormListData = dailyTankRecordingService
				.findDailyTankRecordingFormDetails();
		modelAndView.addObject("dailyTankRecordingFormListData", dailyTankRecordingFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForDailyTankRecordingPage(ModelAndView modelAndView) {

		modelAndView.addObject("dailyTankRecordingForm", new DailyTankRecordingForm());
		modelAndView.addObject("dailyTankRecordingURL",
				TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findDailyTankRecordingDetailsByCode(@PathVariable(name = "id") Long deparmentTankRecordingID) {

		System.out.println("deparmentTankRecordingID Received &&&&&&&&&&&&&&  " + deparmentTankRecordingID);
		ModelAndView modelAndView = new ModelAndView();
		DailyTankRecordingForm dailyTankRecordingForm = dailyTankRecordingService.findDailyTankRecordingDetailsByCode(deparmentTankRecordingID);
		modelAndView.addObject("dailyTankRecordingForm", dailyTankRecordingForm);
		findDailyTankRecordingFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_DAILY_TANK_RECORDING_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		modelAndView.addObject("dailyTankRecordingURL", "dailytankrecording/update/" + dailyTankRecordingForm.getDailytankrecording_id());

		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateDailyTankRecordingForm(@PathVariable(name = "id") Long dailytankrecording_id,
			@ModelAttribute("dailyTankRecordingForm") DailyTankRecordingForm dailyTankRecordingForm) {

		System.out.println("UPDATING ID =========== " + dailytankrecording_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + dailytankrecording_id);
		dailyTankRecordingForm.setDailytankrecording_id(dailytankrecording_id);
		dailyTankRecordingService.updateDailyTankRecordingForm(dailyTankRecordingForm);
		setDefaultDataForDailyTankRecordingPage(modelAndView);
		findDailyTankRecordingFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
		
		//modelAndView.setViewName("redirect:/master/account");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteDailyTankRecordingFormByID(@PathVariable(name = "id") Long dailytankrecording_id,
			@ModelAttribute("dailyTankRecordingForm") DailyTankRecordingForm dailyTankRecordingForm) {

		System.out.println("DELETING ID =========== " + dailytankrecording_id);
		ModelAndView modelAndView = new ModelAndView();
		dailyTankRecordingForm.setDailytankrecording_id(dailytankrecording_id);
		dailyTankRecordingService.deleteDailyTankRecordingForm(dailyTankRecordingForm);
		setDefaultDataForDailyTankRecordingPage(modelAndView);
		findDailyTankRecordingFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}

	public ModelAndView findDailyTankRecordingFormDetails(ModelAndView modelAndView) {

		List<DailyTankRecordingForm> dailyTankRecordingFormList = dailyTankRecordingService.findDailyTankRecordingFormDetails();
		modelAndView.addObject("dailyTankRecordingFormListData", dailyTankRecordingFormList);
		return modelAndView;
	}

}
