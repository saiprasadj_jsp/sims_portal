package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;

public interface LineStockingOfShrimpsDao {

	public LineStockingOfShrimpsToTankForm saveLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm);

	public List<LineStockingOfShrimpsToTankForm> findLineStockingOfShrimpsToTankForm();

	public LineStockingOfShrimpsToTankForm findLineStockingOfShrimpsToTankFormByID(Long lineStockingOfShrimpsToTank_id);

	public LineStockingOfShrimpsToTankForm updateLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm);

	public Boolean deleteLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm LineStockingOfShrimpsToTankForm);

}
