package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;
import com.sims.portal.tankprocess.dao.LineTankToTankShrimpsTransferDao;
import com.sims.portal.tankprocess.services.LineTankToTankShrimpsTransferService;

@Service
public class LineTankToTankShrimpsTransferServiceImpl implements LineTankToTankShrimpsTransferService {

	@Autowired
	private LineTankToTankShrimpsTransferDao lineTankToTankShrimpsTransferDao;

	@Override
	public LineTankToTankShrimpsTransferForm saveLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		lineTankToTankShrimpsTransferDao.saveLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);
		return lineTankToTankShrimpsTransferForm;
	}

	@Override
	public List<LineTankToTankShrimpsTransferForm> findLineTankToTankShrimpsTransferFormDetails() {

		List<LineTankToTankShrimpsTransferForm> lineTankToTankShrimpsTransferFormListData = lineTankToTankShrimpsTransferDao
				.findLineTankToTankShrimpsTransferForm();

		return lineTankToTankShrimpsTransferFormListData;
	}

	@Override
	public LineTankToTankShrimpsTransferForm findLineTankToTankShrimpsTransferDetailsByID(Long id) {

		return lineTankToTankShrimpsTransferDao.findLineTankToTankShrimpsTransferFormByID(id);
	}

	@Override
	public LineTankToTankShrimpsTransferForm updateLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		lineTankToTankShrimpsTransferDao.updateLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);

		return lineTankToTankShrimpsTransferForm;
	}

	@Override
	public Boolean deleteLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm) {

		return lineTankToTankShrimpsTransferDao
				.deleteLineTankToTankShrimpsTransferForm(lineTankToTankShrimpsTransferForm);
	}
}
