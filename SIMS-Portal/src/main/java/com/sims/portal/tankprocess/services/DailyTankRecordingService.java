package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.DailyTankRecordingForm;

public interface DailyTankRecordingService {

	public DailyTankRecordingForm saveDailyTankRecordingForm(DailyTankRecordingForm dailyTankRecordingForm);

	public List<DailyTankRecordingForm> findDailyTankRecordingFormDetails();
	
	public DailyTankRecordingForm findDailyTankRecordingDetailsByCode(Long code);
	
	public DailyTankRecordingForm updateDailyTankRecordingForm(DailyTankRecordingForm dailyTankRecordingForm);
	
	public Boolean deleteDailyTankRecordingForm(DailyTankRecordingForm dailyTankRecordingForm);
}
