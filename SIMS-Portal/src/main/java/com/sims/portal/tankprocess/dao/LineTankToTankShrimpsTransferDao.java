package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineTankToTankShrimpsTransferForm;

public interface LineTankToTankShrimpsTransferDao {

	public LineTankToTankShrimpsTransferForm saveLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);

	public List<LineTankToTankShrimpsTransferForm> findLineTankToTankShrimpsTransferForm();

	public LineTankToTankShrimpsTransferForm findLineTankToTankShrimpsTransferFormByID(Long lineStockingOfShrimpsToTank_id);

	public LineTankToTankShrimpsTransferForm updateLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);

	public Boolean deleteLineTankToTankShrimpsTransferForm(
			LineTankToTankShrimpsTransferForm lineTankToTankShrimpsTransferForm);

}
