package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.StockingOfShrimpsToTankForm;
import com.sims.portal.tankprocess.dao.StockingOfShrimpsDao;
import com.sims.portal.tankprocess.services.StockingOfShrimpsService;

@Service
public class StockingOfShrimpsServiceImpl implements StockingOfShrimpsService {

	@Autowired
	private StockingOfShrimpsDao stockingOfShrimpsDao;

	@Override
	public StockingOfShrimpsToTankForm saveStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		stockingOfShrimpsDao.saveStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
		return stockingOfShrimpsToTankForm;
	}

	@Override
	public List<StockingOfShrimpsToTankForm> findStockingOfShrimpsToTankFormDetails() {

		List<StockingOfShrimpsToTankForm> stockingOfShrimpsToTankFormListData = stockingOfShrimpsDao.findStockingOfShrimpsToTankForm();

		return stockingOfShrimpsToTankFormListData;
	}

	@Override
	public StockingOfShrimpsToTankForm findStockingOfShrimpsDetailsByID(Long id){

		return stockingOfShrimpsDao.findStockingOfShrimpsToTankFormByID(id);
	}
	
	@Override
	public StockingOfShrimpsToTankForm updateStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		stockingOfShrimpsDao.updateStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);

		return stockingOfShrimpsToTankForm;
	}

	@Override
	public Boolean deleteStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		return stockingOfShrimpsDao.deleteStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
	}
}
