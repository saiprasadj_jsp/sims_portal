package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;
import com.sims.portal.tankprocess.dao.LineStockingOfShrimpsDao;
import com.sims.portal.tankprocess.services.LineStockingOfShrimpsService;

@Service
public class LineStockingOfShrimpsServiceImpl implements LineStockingOfShrimpsService {

	@Autowired
	private LineStockingOfShrimpsDao lineStockingOfShrimpsDao;

	@Override
	public LineStockingOfShrimpsToTankForm saveLineStockingOfShrimpsToTankForm(LineStockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		lineStockingOfShrimpsDao.saveLineStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
		return stockingOfShrimpsToTankForm;
	}

	@Override
	public List<LineStockingOfShrimpsToTankForm> findLineStockingOfShrimpsToTankFormDetails() {

		List<LineStockingOfShrimpsToTankForm> stockingOfShrimpsToTankFormListData = lineStockingOfShrimpsDao.findLineStockingOfShrimpsToTankForm();

		return stockingOfShrimpsToTankFormListData;
	}

	@Override
	public LineStockingOfShrimpsToTankForm findLineStockingOfShrimpsDetailsByID(Long id){

		return lineStockingOfShrimpsDao.findLineStockingOfShrimpsToTankFormByID(id);
	}
	
	@Override
	public LineStockingOfShrimpsToTankForm updateLineStockingOfShrimpsToTankForm(LineStockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		lineStockingOfShrimpsDao.updateLineStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);

		return stockingOfShrimpsToTankForm;
	}

	@Override
	public Boolean deleteLineStockingOfShrimpsToTankForm(LineStockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		return lineStockingOfShrimpsDao.deleteLineStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
	}
}
