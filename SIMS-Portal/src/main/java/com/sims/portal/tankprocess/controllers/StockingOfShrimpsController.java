package com.sims.portal.tankprocess.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;
import com.sims.portal.models.tankprocess.beans.StockingOfShrimpsToTankForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.StockingOfShrimpsService;

@Controller
@RequestMapping(value = TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_MAIN_URL)
public class StockingOfShrimpsController {

	@Autowired
	private StockingOfShrimpsService stockingOfShrimpsService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showStockingOfShrimpsDetailsForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllStockingOfShrimpsDetailsForm(modelAndView);
		modelAndView.addObject("stockingOfShrimpsToTankForm", new StockingOfShrimpsToTankForm());
		modelAndView.addObject("lineStockingOfShrimpsToTankForm", new LineStockingOfShrimpsToTankForm());
		modelAndView.addObject("stockingOfShrimpsToTankURL",
				TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SAVE_URL);
		modelAndView.addObject("lineStockingOfShrimpsToTankURL",
				TankProcessConstants.TILES_LINE_STOCKING_OF_SHRIMPS_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveAccountMaster(
			@ModelAttribute("stockingOfShrimpsToTankForm") StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		ModelAndView modelAndView = new ModelAndView();
		stockingOfShrimpsService.saveStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findAllStockingOfShrimpsDetailsForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findAllStockingOfShrimpsDetailsForm(ModelAndView modelAndView) {

		List<StockingOfShrimpsToTankForm> stockingOfShrimpsToTankFormListData = stockingOfShrimpsService
				.findStockingOfShrimpsToTankFormDetails();
		modelAndView.addObject("stockingOfShrimpsToTankFormListData", stockingOfShrimpsToTankFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForStockingOsShrimpsToTankPage(ModelAndView modelAndView) {

		modelAndView.addObject("stockingOfShrimpsToTankForm", new StockingOfShrimpsToTankForm());
		modelAndView.addObject("stockingOfShrimpsToTankURL",
				TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findStockingofshrimpsDetailsByID(@PathVariable(name = "id") Long stockingOfShrimpsID) {

		System.out.println("stockingOfShrimpsID Received &&&&&&&&&&&&&&  " + stockingOfShrimpsID);
		ModelAndView modelAndView = new ModelAndView();
		StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm = stockingOfShrimpsService
				.findStockingOfShrimpsDetailsByID(stockingOfShrimpsID);
		modelAndView.addObject("stockingOfShrimpsToTankForm", stockingOfShrimpsToTankForm);
		findStockingOfShrimpsDetailsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_STOCKING_OF_SHRIMPS_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		modelAndView.addObject("stockingOfShrimpsToTankURL",
				"stockingofshrimps/update/" + stockingOfShrimpsToTankForm.getStockingofshrimpstotank_id());

		Collection<LineStockingOfShrimpsToTankForm> listOfStockingOfShrimpsToTank = stockingOfShrimpsToTankForm
				.getLineStockingOfShrimpsToTankForm();
		modelAndView.addObject("listOfLineStockingOfShrimpsToTankObj", listOfStockingOfShrimpsToTank);

		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateStockingOfShrimpsDetailsForm(@PathVariable(name = "id") Long stockingofshrimpstotank_id,
			@ModelAttribute("stockingOfShrimpsToTankForm") StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		System.out.println("UPDATING ID =========== " + stockingofshrimpstotank_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + stockingofshrimpstotank_id);
		stockingOfShrimpsToTankForm.setStockingofshrimpstotank_id(stockingofshrimpstotank_id);
		stockingOfShrimpsService.updateStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findStockingOfShrimpsDetailsFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		// modelAndView.setViewName("redirect:/master/account");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteStockingOfShrimpsDetailsFormByID(
			@PathVariable(name = "id") Long stockingofshrimpstotank_id,
			@ModelAttribute("stockingOfShrimpsToTankForm") StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		System.out.println("DELETING ID =========== " + stockingofshrimpstotank_id);
		ModelAndView modelAndView = new ModelAndView();
		stockingOfShrimpsToTankForm.setStockingofshrimpstotank_id(stockingofshrimpstotank_id);
		stockingOfShrimpsService.deleteStockingOfShrimpsToTankForm(stockingOfShrimpsToTankForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findStockingOfShrimpsDetailsFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}

	public ModelAndView findStockingOfShrimpsDetailsFormDetails(ModelAndView modelAndView) {

		List<StockingOfShrimpsToTankForm> stockingOfShrimpsToTankFormList = stockingOfShrimpsService
				.findStockingOfShrimpsToTankFormDetails();
		modelAndView.addObject("stockingOfShrimpsToTankFormListData", stockingOfShrimpsToTankFormList);
		return modelAndView;
	}

}
