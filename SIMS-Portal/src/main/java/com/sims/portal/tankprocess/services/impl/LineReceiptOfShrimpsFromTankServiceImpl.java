package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.dao.LineReceiptOfShrimpsFromTankDao;
import com.sims.portal.tankprocess.services.LineReceiptOfShrimpsFromTankService;

@Service
public class LineReceiptOfShrimpsFromTankServiceImpl implements LineReceiptOfShrimpsFromTankService {

	@Autowired
	private LineReceiptOfShrimpsFromTankDao lineReceiptOfShrimpsFromTankDao;

	@Override
	public LineReceiptOfShrimpsFromTankForm saveLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		lineReceiptOfShrimpsFromTankDao.saveLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);
		return lineReceiptOfShrimpsFromTankForm;
	}

	@Override
	public List<LineReceiptOfShrimpsFromTankForm> findLineReceiptOfShrimpsFromTankFormDetails() {

		List<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTankFormListData = lineReceiptOfShrimpsFromTankDao
				.findLineReceiptOfShrimpsFromTankForm();

		return lineReceiptOfShrimpsFromTankFormListData;
	}

	@Override
	public LineReceiptOfShrimpsFromTankForm findLineReceiptOfShrimpsFromTankDetailsByID(Long id) {

		return lineReceiptOfShrimpsFromTankDao.findLineReceiptOfShrimpsFromTankFormByID(id);
	}

	@Override
	public LineReceiptOfShrimpsFromTankForm updateLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		lineReceiptOfShrimpsFromTankDao.updateLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);

		return lineReceiptOfShrimpsFromTankForm;
	}

	@Override
	public Boolean deleteLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		return lineReceiptOfShrimpsFromTankDao.deleteLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);
	}
}
