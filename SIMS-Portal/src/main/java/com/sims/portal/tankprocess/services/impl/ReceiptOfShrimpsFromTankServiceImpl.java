package com.sims.portal.tankprocess.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.dao.ReceiptOfShrimpsFromTankDao;
import com.sims.portal.tankprocess.services.ReceiptOfShrimpsFromTankService;

@Service
public class ReceiptOfShrimpsFromTankServiceImpl implements ReceiptOfShrimpsFromTankService {

	@Autowired
	private ReceiptOfShrimpsFromTankDao receiptOfShrimpsFromTankDao;

	@Override
	public ReceiptOfShrimpsFromTankForm saveReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		receiptOfShrimpsFromTankDao.saveReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);
		return receiptOfShrimpsFromTankForm;
	}

	@Override
	public List<ReceiptOfShrimpsFromTankForm> findReceiptOfShrimpsFromTankFormDetails() {

		List<ReceiptOfShrimpsFromTankForm> receiptOfShrimpsFromTankFormListData = receiptOfShrimpsFromTankDao
				.findReceiptOfShrimpsFromTankForm();

		return receiptOfShrimpsFromTankFormListData;
	}

	@Override
	public ReceiptOfShrimpsFromTankForm findReceiptOfShrimpsFromTankByID(Long receiptofshrimpsfromtank_id) {

		return receiptOfShrimpsFromTankDao.findReceiptOfShrimpsFromTankFormByID(receiptofshrimpsfromtank_id);
	}

	@Override
	public ReceiptOfShrimpsFromTankForm updateReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		receiptOfShrimpsFromTankDao.updateReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);

		return receiptOfShrimpsFromTankForm;
	}

	@Override
	public Boolean deleteReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		return receiptOfShrimpsFromTankDao.deleteReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);
	}
}
