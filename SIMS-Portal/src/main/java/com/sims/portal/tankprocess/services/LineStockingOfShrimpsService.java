package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.LineStockingOfShrimpsToTankForm;

public interface LineStockingOfShrimpsService {

	public LineStockingOfShrimpsToTankForm saveLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm lineStockingOfShrimpsToTankForm);

	public List<LineStockingOfShrimpsToTankForm> findLineStockingOfShrimpsToTankFormDetails();

	public LineStockingOfShrimpsToTankForm findLineStockingOfShrimpsDetailsByID(Long id);

	public LineStockingOfShrimpsToTankForm updateLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm lineStockingOfShrimpsToTankForm);

	public Boolean deleteLineStockingOfShrimpsToTankForm(
			LineStockingOfShrimpsToTankForm lineStockingOfShrimpsToTankForm);
}
