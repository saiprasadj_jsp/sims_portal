package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.dao.LineReceiptOfShrimpsFromTankDao;

@Repository
@Transactional
public class LineReceiptOfShrimpsFromDaoImpl implements LineReceiptOfShrimpsFromTankDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LineReceiptOfShrimpsFromTankForm saveLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		entityManager.persist(lineReceiptOfShrimpsFromTankForm);
		return lineReceiptOfShrimpsFromTankForm;
	}

	@Override
	public List<LineReceiptOfShrimpsFromTankForm> findLineReceiptOfShrimpsFromTankForm() {

		List<LineReceiptOfShrimpsFromTankForm> LineReceiptOfShrimpsFromTankFormListData = entityManager.createQuery(
				"SELECT lineReceiptOfShrimpsFromTankForm FROM LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm ",
				LineReceiptOfShrimpsFromTankForm.class).getResultList();

		return LineReceiptOfShrimpsFromTankFormListData;
	}

	@Override
	public LineReceiptOfShrimpsFromTankForm findLineReceiptOfShrimpsFromTankFormByID(Long dailytankrecording_id) {

		List<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTankForm = entityManager.createQuery(
				"SELECT lineReceiptOfShrimpsFromTankForm FROM LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm WHERE lineReceiptOfShrimpsFromTankForm.lineReceiptOfShrimpsFromTank_id='"
						+ dailytankrecording_id + "'",
				LineReceiptOfShrimpsFromTankForm.class).getResultList();

		if (lineReceiptOfShrimpsFromTankForm.size() > 0) {

			return lineReceiptOfShrimpsFromTankForm.get(0);
		}

		return new LineReceiptOfShrimpsFromTankForm();
	}

	@Override
	public LineReceiptOfShrimpsFromTankForm updateLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		System.out.println(
				"Merged ID BEFORE ====== " + lineReceiptOfShrimpsFromTankForm.getLineReceiptOfShrimpsFromTank_id());
		entityManager.merge(lineReceiptOfShrimpsFromTankForm);
		System.out.println(
				"Merged ID AFTER ====== " + lineReceiptOfShrimpsFromTankForm.getLineReceiptOfShrimpsFromTank_id());

		return lineReceiptOfShrimpsFromTankForm;
	}

	@Override
	public Boolean deleteLineReceiptOfShrimpsFromTankForm(
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		entityManager.remove(entityManager.merge(lineReceiptOfShrimpsFromTankForm));
		return true;
	}

}