package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.TankToTankShrimpsTransferForm;

public interface TankToTankShrimpsTransferDao {

	public TankToTankShrimpsTransferForm saveTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);

	public List<TankToTankShrimpsTransferForm> findTankToTankShrimpsTransferForm();

	public TankToTankShrimpsTransferForm findTankToTankShrimpsTransferFormByID(Long stockingofshrimpstotank_id);

	public TankToTankShrimpsTransferForm updateTankToTankShrimpsTransferForm(
			TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);

	public Boolean deleteTankToTankShrimpsTransferForm(TankToTankShrimpsTransferForm tankToTankShrimpsTransferForm);

}
