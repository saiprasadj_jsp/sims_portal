package com.sims.portal.tankprocess.services;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;

public interface ReceiptOfShrimpsFromTankService {

	public ReceiptOfShrimpsFromTankForm saveReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);

	public List<ReceiptOfShrimpsFromTankForm> findReceiptOfShrimpsFromTankFormDetails();
	
	public ReceiptOfShrimpsFromTankForm findReceiptOfShrimpsFromTankByID(Long id);
	
	public ReceiptOfShrimpsFromTankForm updateReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);
	
	public Boolean deleteReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);
}
