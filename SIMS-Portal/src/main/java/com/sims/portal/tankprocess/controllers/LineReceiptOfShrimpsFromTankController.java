package com.sims.portal.tankprocess.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.sims.portal.masters.constants.MastersPageConstants;
import com.sims.portal.masters.services.DepartmentMasterService;
import com.sims.portal.masters.services.EmployeeMasterService;
import com.sims.portal.masters.services.ProductMasterService;
import com.sims.portal.masters.services.SectionMasterService;
import com.sims.portal.masters.services.WareHouseMasterService;
import com.sims.portal.model.masters.beans.DepartmentMasterForm;
import com.sims.portal.model.masters.beans.EmployeeMasterForm;
import com.sims.portal.model.masters.beans.ProductMasterForm;
import com.sims.portal.model.masters.beans.SectionMasterForm;
import com.sims.portal.model.masters.beans.WareHouseMasterForm;
import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankDTO;
import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;
import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.LineReceiptOfShrimpsFromTankService;
import com.sims.portal.tankprocess.services.ReceiptOfShrimpsFromTankService;

@RestController
@RequestMapping(value = TankProcessConstants.TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_MAIN_URL)
public class LineReceiptOfShrimpsFromTankController {

	@Autowired
	private ReceiptOfShrimpsFromTankService receiptOfShrimpsFromTankService;

	@Autowired
	private LineReceiptOfShrimpsFromTankService lineReceiptOfShrimpsFromTankService;

	@Autowired
	private ProductMasterService productMasterService;

	@Autowired
	private DepartmentMasterService departmentMasterService;

	@Autowired
	private EmployeeMasterService employeeMasterService;

	@Autowired
	private SectionMasterService sectionMasterService;

	@Autowired
	private WareHouseMasterService wareHouseMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showLineStockingOfShrimpsForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllLineStockingOfShrimpsForm(modelAndView);
		modelAndView.addObject("lineReceiptOfShrimpsFromTankForm", new LineReceiptOfShrimpsFromTankForm());
		modelAndView.addObject("stockingOfShrimpsToTankURL",
				TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
	public void saveAccountMaster(@PathVariable(name = "id") Long stockingOfShrimpsToTank_id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm = receiptOfShrimpsFromTankService
				.findReceiptOfShrimpsFromTankByID(stockingOfShrimpsToTank_id);

		System.out
				.println("List Size ======= " + receiptOfShrimpsFromTankForm.getLineReceiptOfShrimpsFromTank().size());
		LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm = new LineReceiptOfShrimpsFromTankForm();
		lineReceiptOfShrimpsFromTankForm.setReceiptOfShrimpsFromTank(receiptOfShrimpsFromTankForm);
		setLineStockingOfShrimpsData(productCode, productName, productDescription, UOM, quantity, remarks,
				lineReceiptOfShrimpsFromTankForm);

		ModelAndView modelAndView = new ModelAndView();
		lineReceiptOfShrimpsFromTankService.saveLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		findAllLineStockingOfShrimpsForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

	}

	private void setLineStockingOfShrimpsData(String productCode, String productName, String productDescription,
			String UOM, Double quantity, String remarks,
			LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm) {

		lineReceiptOfShrimpsFromTankForm.setProductCode(productCode);
		lineReceiptOfShrimpsFromTankForm.setProductDescription(productDescription);
		lineReceiptOfShrimpsFromTankForm.setProductName(productName);
		lineReceiptOfShrimpsFromTankForm.setQuantity(quantity);
		lineReceiptOfShrimpsFromTankForm.setUOM(UOM);
		lineReceiptOfShrimpsFromTankForm.setRemarks(remarks);
	}

	public ModelAndView findAllLineStockingOfShrimpsForm(ModelAndView modelAndView) {

		List<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTankFormListData = lineReceiptOfShrimpsFromTankService
				.findLineReceiptOfShrimpsFromTankFormDetails();
		modelAndView.addObject("lineReceiptOfShrimpsFromTankFormListData", lineReceiptOfShrimpsFromTankFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForStockingOsShrimpsToTankPage(ModelAndView modelAndView) {

		modelAndView.addObject("lineReceiptOfShrimpsFromTankForm", new LineReceiptOfShrimpsFromTankForm());
		modelAndView.addObject("stockingOfShrimpsToTankURL",
				TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@ResponseBody
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public LineReceiptOfShrimpsFromTankDTO findStockingofshrimpsDetailsByID(
			@PathVariable(name = "id") Long lineReceiptOfShrimpsFromTank_id) {

		System.out.println("stockingOfShrimpsID Received &&&&&&&&&&&&&&  " + lineReceiptOfShrimpsFromTank_id);
		ModelAndView modelAndView = new ModelAndView();
		LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm = lineReceiptOfShrimpsFromTankService
				.findLineReceiptOfShrimpsFromTankDetailsByID(lineReceiptOfShrimpsFromTank_id);
		modelAndView.addObject("lineReceiptOfShrimpsFromTankForm", lineReceiptOfShrimpsFromTankForm);
		// findLineStockingOfShrimpsFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		LineReceiptOfShrimpsFromTankDTO dto = new LineReceiptOfShrimpsFromTankDTO();
		dto.setUpdateURL(TankProcessConstants.TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_UPDATE_URL
				+ lineReceiptOfShrimpsFromTankForm.getLineReceiptOfShrimpsFromTank_id());
		dto.setDeleteURL(TankProcessConstants.TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_DELETE_URL
				+ lineReceiptOfShrimpsFromTankForm.getLineReceiptOfShrimpsFromTank_id());
		return dto.getLineStockingOfShrimpsDTO(lineReceiptOfShrimpsFromTankForm);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public void updateLineStockingOfShrimpsForm(@PathVariable(name = "id") Long linestockingOfShrimpsToTank_id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("UPDATING Line ID =========== " + linestockingOfShrimpsToTank_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + linestockingOfShrimpsToTank_id);

		LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm = lineReceiptOfShrimpsFromTankService
				.findLineReceiptOfShrimpsFromTankDetailsByID(linestockingOfShrimpsToTank_id);
		setLineStockingOfShrimpsData(productCode, productName, productDescription, UOM, quantity, remarks,
				lineReceiptOfShrimpsFromTankForm);
		lineReceiptOfShrimpsFromTankService.updateLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);
		setDefaultDataForStockingOsShrimpsToTankPage(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deleteLineStockingOfShrimpsFormByID(@PathVariable(name = "id") Long linestockingOfShrimpsToTank_id,
			@RequestParam("productCode") String productCode, @RequestParam("productName") String productName,
			@RequestParam("productDescription") String productDescription, @RequestParam("UOM") String UOM,
			@RequestParam("quantity") Double quantity, @RequestParam("remarks") String remarks) {

		System.out.println("DELETING ID =========== " + linestockingOfShrimpsToTank_id);
		ModelAndView modelAndView = new ModelAndView();
		LineReceiptOfShrimpsFromTankForm lineReceiptOfShrimpsFromTankForm = lineReceiptOfShrimpsFromTankService
				.findLineReceiptOfShrimpsFromTankDetailsByID(linestockingOfShrimpsToTank_id);
		lineReceiptOfShrimpsFromTankService.deleteLineReceiptOfShrimpsFromTankForm(lineReceiptOfShrimpsFromTankForm);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
	}

	public ModelAndView findLineStockingOfShrimpsFormDetails(ModelAndView modelAndView) {

		List<LineReceiptOfShrimpsFromTankForm> lineReceiptOfShrimpsFromTankFormList = lineReceiptOfShrimpsFromTankService
				.findLineReceiptOfShrimpsFromTankFormDetails();
		modelAndView.addObject("lineReceiptOfShrimpsFromTankFormListData", lineReceiptOfShrimpsFromTankFormList);
		return modelAndView;
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET)
	public ModelAndView getListOfProducts() {

		ModelAndView modelAndView = new ModelAndView();
		List<ProductMasterForm> listOfProducts = productMasterService.findProductMasterDetails();
		modelAndView.addObject("productMasterFormListData", listOfProducts);
		modelAndView.setViewName(MastersPageConstants.PRODUCT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getDepartmentDetails", method = RequestMethod.GET)
	public ModelAndView getListOfDepartments() {

		ModelAndView modelAndView = new ModelAndView();
		List<DepartmentMasterForm> listOfDepartments = departmentMasterService.findDepartmentMasterDetails();
		modelAndView.addObject("departmentMasterFormListData", listOfDepartments);
		modelAndView.setViewName(MastersPageConstants.DEPARTMENT_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getEmployeeDetails", method = RequestMethod.GET)
	public ModelAndView getListOfEmployees() {

		ModelAndView modelAndView = new ModelAndView();
		List<EmployeeMasterForm> listOfEmployees = employeeMasterService.findEmployeeMasterDetails();
		modelAndView.addObject("employeeMasterFormListData", listOfEmployees);
		modelAndView.setViewName(MastersPageConstants.EMPLOYEE_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getSectionDetails", method = RequestMethod.GET)
	public ModelAndView getListOfSections() {

		ModelAndView modelAndView = new ModelAndView();
		List<SectionMasterForm> listOfSections = sectionMasterService.findSectionMasterDetails();
		modelAndView.addObject("sectionMasterFormListData", listOfSections);
		modelAndView.setViewName(MastersPageConstants.SECTION_MASTER_INNER_POPUP);

		return modelAndView;
	}

	@RequestMapping(value = "/getWarehouseDetails", method = RequestMethod.GET)
	public ModelAndView getListOfWarehouses() {

		ModelAndView modelAndView = new ModelAndView();
		List<WareHouseMasterForm> listOfWarehouses = wareHouseMasterService.findWareHouseMasterDetails();
		modelAndView.addObject("warehouseMasterFormListData", listOfWarehouses);
		modelAndView.setViewName(MastersPageConstants.WAREHOUSE_MASTER_INNER_POPUP);

		return modelAndView;
	}
}