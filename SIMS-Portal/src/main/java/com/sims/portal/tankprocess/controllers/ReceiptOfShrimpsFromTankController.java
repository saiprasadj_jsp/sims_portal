package com.sims.portal.tankprocess.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sims.portal.models.tankprocess.beans.LineReceiptOfShrimpsFromTankForm;
import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;
import com.sims.portal.tankprocess.constants.TankProcessConstants;
import com.sims.portal.tankprocess.services.ReceiptOfShrimpsFromTankService;

@Controller
@RequestMapping(value = TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_MAIN_URL)
public class ReceiptOfShrimpsFromTankController {

	@Autowired
	private ReceiptOfShrimpsFromTankService receiptOfShrimpsFromTankService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showReceiptOfShrimpsFromTankForm() {

		ModelAndView modelAndView = new ModelAndView();

		findAllReceiptOfShrimpsFromTankForm(modelAndView);
		modelAndView.addObject("receiptOfShrimpsFromTankForm", new ReceiptOfShrimpsFromTankForm());
		modelAndView.addObject("lineReceiptOfShrimpsFromTankForm", new LineReceiptOfShrimpsFromTankForm());
		modelAndView.addObject("receiptOfShrimpsFromTankURL",
				TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL);
		modelAndView.addObject("lineReceiptOfShrimpsFromTankURL",
				TankProcessConstants.TILES_LINE_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveAccountMaster(
			@ModelAttribute("receiptOfShrimpsFromTankForm") ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		ModelAndView modelAndView = new ModelAndView();
		receiptOfShrimpsFromTankService.saveReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);
		setDefaultDataForReceiptOfShrimpsFromTankPage(modelAndView);
		findAllReceiptOfShrimpsFromTankForm(modelAndView);
		modelAndView.addObject("message", "Data Saved Successfully !!!");

		return modelAndView;
	}

	public ModelAndView findAllReceiptOfShrimpsFromTankForm(ModelAndView modelAndView) {

		List<ReceiptOfShrimpsFromTankForm> receiptOfShrimpsFromTankFormListData = receiptOfShrimpsFromTankService
				.findReceiptOfShrimpsFromTankFormDetails();
		modelAndView.addObject("receiptOfShrimpsFromTankFormListData", receiptOfShrimpsFromTankFormListData);

		return modelAndView;
	}

	private ModelAndView setDefaultDataForReceiptOfShrimpsFromTankPage(ModelAndView modelAndView) {

		modelAndView.addObject("receiptOfShrimpsFromTankForm", new ReceiptOfShrimpsFromTankForm());
		modelAndView.addObject("receiptOfShrimpsFromTankURL",
				TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SAVE_URL);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);

		return modelAndView;
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView findReceiptOfShrimpsFromTankDetailsByID(
			@PathVariable(name = "id") Long receiptofshrimpsfromtank_id) {

		System.out.println("receiptofshrimpsfromtank_id Received &&&&&&&&&&&&&&  " + receiptofshrimpsfromtank_id);
		ModelAndView modelAndView = new ModelAndView();
		ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm = receiptOfShrimpsFromTankService
				.findReceiptOfShrimpsFromTankByID(receiptofshrimpsfromtank_id);
		modelAndView.addObject("receiptOfShrimpsFromTankForm", receiptOfShrimpsFromTankForm);
		findReceiptOfShrimpsFromTankFormDetails(modelAndView);
		modelAndView.setViewName(TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_SHOW_MAIN_JSP);
		modelAndView.addObject("tabToShow", "details");
		modelAndView.addObject("receiptOfShrimpsFromTankURL",
				TankProcessConstants.TILES_RECEIPT_OF_SHRIMPS_FROM_TANK_CONTROLLER_UPDATE_URL
						+ receiptOfShrimpsFromTankForm.getReceiptofshrimpsfromtank_id());

		Collection<LineReceiptOfShrimpsFromTankForm> listOfLineReceiptOfShrimpsFromTank = receiptOfShrimpsFromTankForm
				.getLineReceiptOfShrimpsFromTank();
		modelAndView.addObject("listOfLineReceiptOfShrimpsFromTankObj", listOfLineReceiptOfShrimpsFromTank);

		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public ModelAndView updateReceiptOfShrimpsFromTankForm(@PathVariable(name = "id") Long receiptofshrimpsfromtank_id,
			@ModelAttribute("receiptOfShrimpsFromTankForm") ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		System.out.println("UPDATING ID =========== " + receiptofshrimpsfromtank_id);
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("UPDATE CODE === " + receiptofshrimpsfromtank_id);
		receiptOfShrimpsFromTankForm.setReceiptofshrimpsfromtank_id(receiptofshrimpsfromtank_id);
		receiptOfShrimpsFromTankService.updateReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);
		setDefaultDataForReceiptOfShrimpsFromTankPage(modelAndView);
		findReceiptOfShrimpsFromTankFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Updated Successfully !!!");

		// modelAndView.setViewName("redirect:/master/account");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteReceiptOfShrimpsFromTankFormByID(
			@PathVariable(name = "id") Long receiptofshrimpsfromtank_id,
			@ModelAttribute("receiptOfShrimpsFromTankForm") ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm) {

		System.out.println("DELETING ID =========== " + receiptofshrimpsfromtank_id);
		ModelAndView modelAndView = new ModelAndView();
		receiptOfShrimpsFromTankForm.setReceiptofshrimpsfromtank_id(receiptofshrimpsfromtank_id);
		receiptOfShrimpsFromTankService.deleteReceiptOfShrimpsFromTankForm(receiptOfShrimpsFromTankForm);
		setDefaultDataForReceiptOfShrimpsFromTankPage(modelAndView);
		findReceiptOfShrimpsFromTankFormDetails(modelAndView);
		modelAndView.addObject("message", "Data Deleted Successfully !!!");
		return modelAndView;
	}

	public ModelAndView findReceiptOfShrimpsFromTankFormDetails(ModelAndView modelAndView) {

		List<ReceiptOfShrimpsFromTankForm> receiptOfShrimpsFromTankFormList = receiptOfShrimpsFromTankService
				.findReceiptOfShrimpsFromTankFormDetails();
		modelAndView.addObject("receiptOfShrimpsFromTankFormListData", receiptOfShrimpsFromTankFormList);
		return modelAndView;
	}

}
