package com.sims.portal.tankprocess.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sims.portal.models.tankprocess.beans.StockingOfShrimpsToTankForm;
import com.sims.portal.tankprocess.dao.StockingOfShrimpsDao;

@Repository
@Transactional
public class StockingOfShrimpsDaoImpl implements StockingOfShrimpsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public StockingOfShrimpsToTankForm saveStockingOfShrimpsToTankForm(
			StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		entityManager.persist(stockingOfShrimpsToTankForm);
		return stockingOfShrimpsToTankForm;
	}

	@Override
	public List<StockingOfShrimpsToTankForm> findStockingOfShrimpsToTankForm() {

		List<StockingOfShrimpsToTankForm> stockingOfShrimpsToTankFormListData = entityManager.createQuery(
				"SELECT stockingOfShrimpsToTankForm FROM StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm ",
				StockingOfShrimpsToTankForm.class).getResultList();

		return stockingOfShrimpsToTankFormListData;
	}

	@Override
	public StockingOfShrimpsToTankForm findStockingOfShrimpsToTankFormByID(Long dailytankrecording_id) {

		List<StockingOfShrimpsToTankForm> stockingOfShrimpsToTankForm = entityManager.createQuery(
				"SELECT stockingOfShrimpsToTankForm FROM StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm WHERE stockingOfShrimpsToTankForm.stockingofshrimpstotank_id='"
						+ dailytankrecording_id + "'",
				StockingOfShrimpsToTankForm.class).getResultList();

		if (stockingOfShrimpsToTankForm.size() > 0) {

			return stockingOfShrimpsToTankForm.get(0);
		}

		return new StockingOfShrimpsToTankForm();
	}

	@Override
	public StockingOfShrimpsToTankForm updateStockingOfShrimpsToTankForm(
			StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		System.out.println("Merged ID BEFORE ====== " + stockingOfShrimpsToTankForm.getStockingofshrimpstotank_id());
		entityManager.merge(stockingOfShrimpsToTankForm);
		System.out.println("Merged ID AFTER ====== " + stockingOfShrimpsToTankForm.getStockingofshrimpstotank_id());

		return stockingOfShrimpsToTankForm;
	}

	@Override
	public Boolean deleteStockingOfShrimpsToTankForm(StockingOfShrimpsToTankForm stockingOfShrimpsToTankForm) {

		entityManager.remove(entityManager.merge(stockingOfShrimpsToTankForm));
		return true;
	}

}