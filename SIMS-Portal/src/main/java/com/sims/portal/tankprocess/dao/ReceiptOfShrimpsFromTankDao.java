package com.sims.portal.tankprocess.dao;

import java.util.List;

import com.sims.portal.models.tankprocess.beans.ReceiptOfShrimpsFromTankForm;

public interface ReceiptOfShrimpsFromTankDao {

	public ReceiptOfShrimpsFromTankForm saveReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);

	public List<ReceiptOfShrimpsFromTankForm> findReceiptOfShrimpsFromTankForm();

	public ReceiptOfShrimpsFromTankForm findReceiptOfShrimpsFromTankFormByID(Long stockingofshrimpstotank_id);

	public ReceiptOfShrimpsFromTankForm updateReceiptOfShrimpsFromTankForm(
			ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);

	public Boolean deleteReceiptOfShrimpsFromTankForm(ReceiptOfShrimpsFromTankForm receiptOfShrimpsFromTankForm);

}
