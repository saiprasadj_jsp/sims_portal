<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Direct Purchase List</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>document No</th>
							<th>Date</th>
							<th>Purchase Type</th>
							<th>Supplier Name</th>
							<th>Department</th>							
							<th>Narration</th>
							<th>LR Number</th>
							<th>Action</th>
							
							<!-- 
							 private Long documentNo;
						    private Date date;
						private String purchaseType;
						private String supplierName;
						private String department;
						private String warehouse;
						
						private String DCNo;
						private Date DCDate;	
						private String invoiceNumber;
						private Date invoiceDate;
						private String LRNumber;
						private Date LRDate;
						
						
						
						private String vehicleNumber;
						private String transporterName;
						private String freight;
						private String narration;
							
							 -->
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${directPurchaseListData}"
							var="directPurchaseListObj">
							<tr>
								<td>${directPurchaseListObj.DPId}</td>
								<td>${directPurchaseListObj.documentNo}</td>
								<td>${directPurchaseListObj.date}</td>
								<td>${directPurchaseListObj.purchaseType}</td>
								<td>${directPurchaseListObj.supplierName}</td>
								<td>${directPurchaseListObj.department}</td>								
								<td>${directPurchaseListObj.narration}</td>
								<td>${directPurchaseListObj.LRNumber}</td>
								
								<td><a href="${pageContext.request.contextPath}/admin/direct/purchase/edit/${directPurchaseListObj.DPId}">
										Edit</a> || <a
									href="${pageContext.request.contextPath}/admin/direct/purchase/delete/${directPurchaseListObj.DPId}">Delete</a>
								</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
</html>

