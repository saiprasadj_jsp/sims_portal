<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
 <%@ taglib prefix="directPurchaselines" tagdir="/WEB-INF/tags/directpurchase/lines"%> 
directPurchaselines.tag
<head>
<script type="text/javascript">
	function validateform() {
		/* var number = document.getElementById("phone").value;
		var x=document.getElementById("email").value; 
		var atposition=x.indexOf("@");  
		var dotposition=x.lastIndexOf(".");
		var msg=" ";
		if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
			  alert("Please enter a valid e-mail address ");  
			  return false;  
		 }  
		if (isFinite(number)) {
			return true;
		} else {
			alert("Please Enter Phone Number");
			return false;
		} */

		alert("from Validation");
		return true;
	}
</script>
</head>
<form:form id="DirectPurchaseDetailsForm"
	modelAttribute="directPurchasesForm" action="${pageContext.request.contextPath}/admin/${directPurchaseURL}"
	method="POST" onsubmit="return validateform()">
	
	<div class="row">
		<div class="col-lg-12">
			<h5>Direct Purchase Details</h5>
			<h5 style="color:green;">${message}</h5>
			<div class="panel-body">
				<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="document No" /></label>
							<form:input path="documentNo" id="documentNo" class="form-control" value=""></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Date" /></label>
							<form:input path="date" id="date" class="form-control"></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="purchase Type" /></label>
							<form:input path="purchaseType" id="purchaseType" class="form-control" />
						</div>
						
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Supplier Name" /></label>
							<form:input path="supplierName" id="supplierName"
								class="form-control" />
						</div>					
						
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Department" /></label>
							<form:input path="department" id="department" class="form-control" />
						</div>
						
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="WareHouse" /></label>
							<form:input path="warehouse" id="warehouse" class="form-control" />
						</div>	
						
						 <div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Vehicle Number" /></label>
							<form:input path="vehicleNumber" id="vehicleNumber"
								class="form-control" />
						</div>
					 
					 <div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Transporter Name" /></label>
							<form:input path="transporterName" id="transporterName" class="form-control" />
						</div>				
						
				</div>
				<div class="col-lg-6">				
				
				<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="DCNo" /></label>
							<form:input path="DCNo" id="DCNo" class="form-control" />
				</div>
				<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="DC Date" /></label>
							<form:input path="DCDate" id="DCDate" class="form-control" />
				</div>
				<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Invoice Number" /></label>
							<form:input path="invoiceNumber" id="invoiceNumber" class="form-control" />
						</div>
				<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Invoice Date" /></label>
							<form:input path="invoiceDate" id="invoiceDate" class="form-control" />
						</div>	
				<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="LR Number" /></label>
							<form:input path="LRNumber" id="LRNumber" class="form-control" />
						</div>	
						
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="LR Date" /></label>
							<form:input path="LRDate" id="LRDate" class="form-control" />
						</div>	
						
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Freight" /></label>
							<form:input path="freight" id="freight" class="form-control" />
						</div>
									 					
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="narration" /></label>
							<form:input path="narration" id="narration" class="form-control" />
						</div>
											
				</div>			
				
			<%-- <div class="col-lg-6">					
						
						
				</div> --%>
					
		</div>

			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="row">
			<c:choose>
				<c:when test="${directPurchaseURL == 'direct/purchase/save'}">
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Save</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit"
							disabled="disabled">Update</button>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Update</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Delete</button>
					</div>
				</c:otherwise>
			</c:choose>

		</div>

	</div>
</form:form>

<directPurchaselines:directPurchaselines/>
<div class="col-xs-6">
	<!-- <button class="btn btn-primary btn-xs" type="submit" data-toggle="modal" data-target="#myModal" onclick=showpopUp()>Add Line</button> -->
	<button type="button" id="adddirectPurchaseLine"
		class="btn btn-primary btn-xs">Add New Line</button>
</div>

<!-- Modal -->
<div class="modal fade" id="directPurchaseLineModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-content modal-forms">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="createPurchaseLabel">Direct Purchase Line</h4>
				</div>
				=================${pageContext.request.contextPath}==============
				<input id="lineSaveURL" type="hidden"
					value="${pageContext.request.contextPath}/admin/direct/purchaseline/save/${directPurchasesForm.DPId}" />
				<input id="lineUpdateURL" type="hidden"
					value="${pageContext.request.contextPath}/" /> <input
					id="lineDeleteURL" type="hidden"
					value="${pageContext.request.contextPath}/" />

				<div class="modal-body col-xs-12" style="background: white;">
					<form id="directPurchaseLineForm"
						action="${pageContext.request.contextPath}/${directPurchaseLineSaveURL}"
						method="POST" onsubmit="return validateform()">
						===DOCUMENT
						NUMBER==========${directPurchaseLineForm.DPLId}==========================
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="row">
							<div class="col-lg-12">
								<h5>Direct Purchase Line Details</h5>
								<h5 style="color: green;">${message}</h5>
								<div class="panel-body">
									<input type="hidden" name="productCode" id="productCode"
										class="form-control"></input>										
								 <input type="hidden" name="DPLId"
										id="DPLId" class="form-control"></input>
								
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Product Name" /></label> <input
													name="productName" id="productName" class="form-control"><span
													class="glyphicon glyphicon-modal-window"
													class="btn btn-info btn-lg"
													onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getProductDetails');"></span></input>
											</div>
											
												
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="UOM" /></label> <input
													name="UOM" id="UOM" class="form-control" />
											</div>
										
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Received Quantity Number" /></label>
												<input name="receivedQuantity" id="receivedQuantity"
													class="form-control"></input>
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Rejected Quantity Number" /></label>
												<input name="rejectedQuantity" id="rejectedQuantity"
													class="form-control"></input>
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Accepted Quantity Number" /></label>
												<input name="acceptedQuantity" id="acceptedQuantity"
													class="form-control"></input>
											</div>
											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Gross Quantity" /></label>
												<input name="grossQuantity" id="grossQuantity"
													class="form-control"></input>
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Rate" /></label> <input
													name="rate" id="rate" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Discount" /></label> <input
													name="Discount" id="Discount" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="VAT" /></label> <input
													name="VAT" id="VAT" class="form-control" />
											</div>
											
											 <div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="CST" /></label> <input
													name="CST" id="CST" class="form-control" />
											</div>
															
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="PO Number" /></label> <input
													name="PONumber" id="PONumber" class="form-control" />
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="PO Date" /></label> <input
													name="PODate" id="PODate" class="form-control" />
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Freight" /></label> <input
													name="freightAmount" id="freightAmount" class="form-control" />
											</div>
										
																						
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Remarks" /></label> <input
													name="remarks" id="remarks" class="form-control" />
											</div> 
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 30px;"></div>
						=====LSU====${lineStockingOfShrimpsToTankURL}=====
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="saveButton"
										type="button">Save</button>
								</div>
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs"
										class="cancel_btn button" id="cancelButton"
										data-dismiss="modal">Cancel</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="deleteButton"
										type="submit">Delete</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="updateButton"
										type="button">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>