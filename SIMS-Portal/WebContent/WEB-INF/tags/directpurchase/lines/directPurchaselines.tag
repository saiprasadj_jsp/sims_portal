<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>UOM</th>
							<th>Received Quantity </th>
							<th>Rejected Quantity </th>
							<th>Accepted Quantity </th>
							<th>Gross Quantity</th>
							<th>Discount</th>
							<th>VAT</th>
							<th>CST</th>							
							<th>Action</th>
					</tr> 
				</thead>
				<tbody>
				 	<!-- 
						private String productCode;
						private String productName;
						private String UOM;
						private Double receivedQuantity;
						private Double rejectedQuantity;
						private Double acceptedQuantity;
						private Double grossQuantity;
						private Double Discount;
						private Double VAT;
						private Double CST;
						private Double freightAmount;	 -->																
										
					 <c:forEach items="${listOfLinelistOflistOfDirectPurchaseLineLineObj}"
						var="directPurchaseLineObj" varStatus="counter">
						<tr>				
		
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}</td>
							<td>${directPurchaseLineObj.productCode}</td>
							<td>${directPurchaseLineObj.productName}</td>
							<td>${directPurchaseLineObj.UOM}</td>
							<td>${directPurchaseLineObj.receivedQuantity}</td>
							<td>${directPurchaseLineObj.rejectedQuantity}</td>
							<td>${directPurchaseLineObj.acceptedQuantity}</td>						
														
							<td>${directPurchaseLineObj.grossQuantity}</td>
					        <td>${directPurchaseLineObj.discount}</td>
					        <td>${directPurchaseLineObj.VAT}</td>
					        <td>${directPurchaseLineObj.CST}</td>		
					
							<td><a
								href="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/edit/${directPurchaseLineObj.DPLId}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/delete/${directPurchaseLineObj.DPLId}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/delete" />
		</div>
	</div>
</div>