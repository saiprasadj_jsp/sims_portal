<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>UOM</th>
							<th>Received Quantity Number</th>
							<th>Rejected Quantity Number</th>
							<th>Accepted Quantity Number</th>
							<th>Gross Quantity</th>
							<th>Discount</th>
							<th>VAT</th>
							<th>CST</th>							
							<th>Action</th>
					</tr> 
				</thead>
				<tbody>
				 	<!-- 
						private String productCode;
						private String productName;
						private String UOM;	
						private String remarks;
						private Double receivedQuantityNumber;
						private Double rejectedQuantityNumber;
						private Double acceptedQuantityNumber;
						private Double grossQuantity;
						private Double rate;
						private Double Discount;
						private Double VAT;
						private Double CST;
						private String PONumber;	
						private Date PODate;
						private Double freight;
						 -->																
					
					 <c:forEach items="${listOfLinelistOfMaterialReceiptLineObj}"
						var="materialReceiptLineObj" varStatus="counter">
						<tr>				
		
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}</td>
							<td>${materialReceiptLineObj.productCode}</td>
							<td>${materialReceiptLineObj.productName}</td>
							<td>${materialReceiptLineObj.UOM}</td>
							<td>${materialReceiptLineObj.receivedQuantityNumber}</td>
							<td>${materialReceiptLineObj.rejectedQuantityNumber}</td>
							<td>${materialReceiptLineObj.acceptedQuantityNumber}</td>						
														
							<td>${materialReceiptLineObj.grossQuantity}</td>
					        <td>${materialReceiptLineObj.discount}</td>
					        <td>${materialReceiptLineObj.VAT}</td>
					        <td>${materialReceiptLineObj.CST}</td>		
					
							<td><a
								href="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/edit/${materialReceiptLineObj.MRLId}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/delete/${materialReceiptLineObj.MRLId}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/admin/materialreceipt/materialreceiptline/delete" />
		</div>
	</div>
</div>