<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="purchaseorderlines"
	tagdir="/WEB-INF/tags/purchaseorder/lines"%>
<head>
<script type="text/javascript">
	function validateform() {
		/* var number = document.getElementById("phone").value;
		var x=document.getElementById("email").value; 
		var atposition=x.indexOf("@");  
		var dotposition=x.lastIndexOf(".");
		var msg=" ";
		if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
			  alert("Please enter a valid e-mail address ");  
			  return false;  
		 }  
		if (isFinite(number)) {
			return true;
		} else {
			alert("Please Enter Phone Number");
			return false;
		} */

		alert("from Validation");
		return true;
	}

	function showpopUp() {

		alert("for popup dipaly logic");
		return false;
	}
</script>
</head>
<form:form id="purchaseOrderDetailsForm"
	modelAttribute="purchaseOrderForm"
	action="${pageContext.request.contextPath}/admin/${purchaseOrderURL}"
	method="POST" onsubmit="return validateform()">
    <input type="hidden" id="currentURL"
		value="${requestScope['javax.servlet.forward.request_uri']}" />
	<div class="row">
		<div class="col-lg-12">
			<h5>purchase Order Details</h5>
			<h5 style="color: green;">${message}</h5>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="document No" /></label>
							<form:input path="documentNo" id="documentNo"
								class="form-control" value=""></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Date" /></label>
							<form:input path="date" id="date" class="form-control"></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="purchase Type" /></label>
							<form:input path="purchaseType" id="purchaseType"
								class="form-control" />
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="supplier Name" /></label>
							<form:input path="supplierName" id="supplierName"
								class="form-control" />
						</div>

					</div>

					<div class="col-lg-6">

						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Department" /></label>
							<form:input path="department" id="department"
								class="form-control" />
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Payment Terms" /></label>
							<form:input path="paymentTerms" id="paymentTerms"
								class="form-control" />
						</div>

						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Delivery Terms" /></label>
							<form:input path="deliveryTerms" id="deliveryTerms"
								class="form-control" />
						</div>
						<%-- <div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="employee Name" /></label><br>
							<form:select id="select" path="typeofcustomer">
								<form:option value="-" label="--Select Role--" />
								<form:options items="${typesOfCustomersMap}" />
							</form:select>
						</div> --%>


						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Narration" /></label>
							<form:input path="narration" id="narration" class="form-control" />
						</div>



					</div>

				</div>

			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="row">
			<c:choose>
				<c:when test="${purchaseOrderURL == 'purchase/purchaseorder/save'}">
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Save</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit"
							disabled="disabled">Update</button>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Update</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Delete</button>
					</div>
				</c:otherwise>
			</c:choose>

		</div>

	</div>

	<!-- <div>
	
	</div> 
 -->
</form:form>

<purchaseorderlines:purchaseorderlines />
<div class="col-xs-6">
	<!-- <button class="btn btn-primary btn-xs" type="submit" data-toggle="modal" data-target="#myModal" onclick=showpopUp()>Add Line</button> -->
	<button type="button" id="addpurchaseOrderLine"
		class="btn btn-primary btn-xs">Add New Line</button>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseOrderLineModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-content modal-forms">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="createPurchaseLabel">Purchase
						Order Line</h4>
				</div>
				=================${pageContext.request.contextPath}==============
				<input id="lineSaveURL" type="hidden"
					value="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/save/${purchaseOrderForm.POId}" />
				<input id="lineUpdateURL" type="hidden"
					value="${pageContext.request.contextPath}/" /> <input
					id="lineDeleteURL" type="hidden"
					value="${pageContext.request.contextPath}/" />

				<div class="modal-body col-xs-12" style="background: white;">
					<form id="purchaseOrderLineForm"
						action="${pageContext.request.contextPath}/${purchaseOrderLineSaveURL}"
						method="POST" onsubmit="return validateform()">
						===DOCUMENT
						NUMBER==========${purchaseOrderLineForm.POLId}==========================
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="row">
							<div class="col-lg-12">
								<h5>Purchase Order Line Details</h5>
								<h5 style="color: green;">${message}</h5>
								<div class="panel-body">
									<input type="hidden" name="productCode" id="productCode"
										class="form-control"></input>										
								 <input type="hidden" name="POLId"
										id="POLId" class="form-control"></input>
								
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Product Name" /></label> <input
													name="productName" id="productName" class="form-control"><span
													class="glyphicon glyphicon-modal-window"
													class="btn btn-info btn-lg"
													onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getProductDetails');"></span></input>
											</div>
											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Quantity" /></label>
												<input name="quantity" id="quantity"
													class="form-control"></input>
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Avilable Stock" /></label>
												<input name="availableStock" id="availableStock"
													class="form-control"></input>
											</div>
											 <div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Rrequired Date" /></label>
												<input name="requiredDate" id="requiredDate"
													class="form-control"></input>
											</div> 											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="UOM" /></label> <input
													name="UOM" id="UOM" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Purpose" /></label> <input
													name="purpose" id="purpose" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Rate" /></label> <input
													name="rate" id="rate" class="form-control" />
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Gross" /></label> <input
													name="gross" id="gross" class="form-control" />
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Discount" /></label> <input
													name="discount" id="discount" class="form-control" />
											</div>
											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="VAT" /></label> <input
													name="VAT" id="VAT" class="form-control" />
											</div> 
											
											 <div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Purchase Requisition Date" /></label> <input
													name="purchaseRequisitionDate" id="purchaseRequisitionDate" class="form-control" />
											</div>
											 
											 <div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="CST" /></label> <input
													name="CST" id="CST" class="form-control" />
											</div>
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Purchase Requisition Number" /></label> <input
													name="purchaseRequisitionNumber" id="purchaseRequisitionNumber" class="form-control" />
											</div>
																						
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Remarks" /></label> <input
													name="remarks" id="remarks" class="form-control" />
											</div> 
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 30px;"></div>
						=====LSU====${lineStockingOfShrimpsToTankURL}=====
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="saveButton"
										type="button">Save</button>
								</div>
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs"
										class="cancel_btn button" id="cancelButton"
										data-dismiss="modal">Cancel</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="deleteButton"
										type="submit">Delete</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="updateButton"
										type="button">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
