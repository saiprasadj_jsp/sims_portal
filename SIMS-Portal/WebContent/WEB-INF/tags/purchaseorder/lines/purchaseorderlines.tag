<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>UOM</th>
							<th>Quantity</th>
							<th>Available Stock</th>
							<th>Required Date</th>
							<th>Gross</th>
							<th>Discount</th>
							<th>VAT</th>
							<th>CST</th>							
							<th>Action</th>
					</tr> 
				</thead>
				<tbody>
				<!-- purchaseOrderLineObj.quantity;
					purchaseOrderLineObj.purpose;
					purchaseOrderLineObj.remarks;
					purchaseOrderLineObj.rate;
					 -->
					 <c:forEach items="${listOfLinePurchaseOrderLineObj}"
						var="purchaseOrderLineObj" varStatus="counter">
						<tr>				
		
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}</td>
							<td>${purchaseOrderLineObj.productCode}</td>
							<td>${purchaseOrderLineObj.productName}</td>
							<td>${purchaseOrderLineObj.UOM}</td>
							<td>${purchaseOrderLineObj.quantity}</td>
							<td>${purchaseOrderLineObj.availableStock}</td>
							<td>${purchaseOrderLineObj.requiredDate}</td>
							
							<td>${purchaseOrderLineObj.gross}</td>
					        <td>${purchaseOrderLineObj.discount}</td>
					        <td>${purchaseOrderLineObj.VAT}</td>
					        <td>${purchaseOrderLineObj.CST}</td>		
					
							<td><a
								href="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/edit/${purchaseOrderLineObj.POLId}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/delete/${purchaseOrderLineObj.POLId}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/delete" />
		</div>
	</div>
</div>