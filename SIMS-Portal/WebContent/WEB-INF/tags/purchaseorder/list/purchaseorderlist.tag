<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Purchase Order List</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>document No</th>
							<th>Date</th>
							<th>Purchase Type</th>
							<th>Department</th>
							<th>Supplier Name</th>
							<th>Narration</th>
							<th>Payment Terms</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${purchaseOrderListData}"
							var="purchaseOrderFormListObj">
							<tr>
								<td>${purchaseOrderFormListObj.POId}</td>
								<td>${purchaseOrderFormListObj.documentNo}</td>
								<td>${purchaseOrderFormListObj.date}</td>
								<td>${purchaseOrderFormListObj.purchaseType}</td>
								<td>${purchaseOrderFormListObj.department}</td>
								<td>${purchaseOrderFormListObj.supplierName}</td>
								<td>${purchaseOrderFormListObj.narration}</td>
								<td>${purchaseOrderFormListObj.paymentTerms}</td>
								
								<td><a href="${pageContext.request.contextPath}/admin/purchase/purchaseorder/edit/${purchaseOrderFormListObj.POId}">
										Edit</a> || <a
									href="${pageContext.request.contextPath}/admin/purchase/purchaserequisition/delete/${purchaseOrderFormListObj.POId}">Delete</a>
								</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
</html>

