<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>UOM</th>
							<th>Quantity</th>
							<th>Available Stock</th>
							<th>Required Date</th>
							<th>purpose</th>
							<th>remarks</th>
							<th>Action</th>
					</tr> 
				</thead>
				<tbody>
				<!-- private Long PRLId;	
	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
					 -->
					 <c:forEach items="${listOfLinePurchaseRequisitionLineObj}"
						var="purchaseRequisitionLineObj" varStatus="counter">
						<tr>				
		
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/admin/purchase/purchaserequisitionline/edit/${purchaseRequisitionLineObj.PRLId}</td>
							<td>${purchaseRequisitionLineObj.productCode}</td>
							<td>${purchaseRequisitionLineObj.productName}</td>
							<td>${purchaseRequisitionLineObj.UOM}</td>
							<td>${purchaseRequisitionLineObj.quantity}</td>
							<td>${purchaseRequisitionLineObj.availableStock}</td>
							<td>${purchaseRequisitionLineObj.requiredDate}</td>
							
							<td>${purchaseRequisitionLineObj.purpose}</td>
					        <td>${purchaseRequisitionLineObj.remarks}</td>
					      
							<td><a
								href="${pageContext.request.contextPath}/admin/purchase/purchaserequisitionline/edit/${purchaseRequisitionLineObj.PRLId}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/admin/purchase/purchaserequisitionline/delete/${purchaseRequisitionLineObj.PRLId}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/admin/purchase/purchaseorderline/delete" />
		</div>
	</div>
</div>