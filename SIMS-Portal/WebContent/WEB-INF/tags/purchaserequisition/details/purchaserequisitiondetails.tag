<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="purchaserequisitionlines"
	tagdir="/WEB-INF/tags/purchaserequisition/lines"%>

<head>
<script type="text/javascript">
	function validateform() {
		/* var number = document.getElementById("phone").value;
		var x=document.getElementById("email").value; 
		var atposition=x.indexOf("@");  
		var dotposition=x.lastIndexOf(".");
		var msg=" ";
		if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
			  alert("Please enter a valid e-mail address ");  
			  return false;  
		 }  
		if (isFinite(number)) {
			return true;
		} else {
			alert("Please Enter Phone Number");
			return false;
		} */

		alert("from Validation");
		return true;
	}
</script>
</head>
<form:form id="purchaseRequisitionDetailsForm"
	modelAttribute="purchaseRequisitionForm" action="${pageContext.request.contextPath}/admin/${purchaseRequisitionURL}"
	method="POST" onsubmit="return validateform()">
	<input type="hidden" id="currentURL"
		value="${requestScope['javax.servlet.forward.request_uri']}" />
	<div class="row">
		<div class="col-lg-12">
			<h5>purchase Requisition Details</h5>
			<h5 style="color: green;">${message}</h5>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="document No" /></label>
							<form:input path="documentNo" id="documentNo" class="form-control" value=""></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Date" /></label>
							<form:input path="date" id="date" class="form-control"></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="purchase Type" /></label>
							<form:input path="purchaseType" id="purchaseType" class="form-control" />
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Department" /></label>
							<form:input path="department" id="department" class="form-control" />
						</div>
						<%-- <div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="employee Name" /></label><br>
							<form:select id="select" path="typeofcustomer">
								<form:option value="-" label="--Select Role--" />
								<form:options items="${typesOfCustomersMap}" />
							</form:select>
						</div> --%>

					</div>
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="employee Name" /></label>
							<form:input path="employeeName" id="employeeName"
								class="form-control" />
						</div>					
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="section Name" /></label>
							<form:input path="sectionName" id="sectionName" class="form-control" />
						</div>
	                   <div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Narration" /></label>
							<form:input path="narration" id="narration"
								class="form-control" />
						</div>
						 
					</div>
					
				</div>

			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="row">
			<c:choose>
				<c:when test="${purchaseRequisitionURL == 'purchase/purchaserequisition/save'}">
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Save</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit"
							disabled="disabled">Update</button>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Update</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Delete</button>
					</div>
				</c:otherwise>
			</c:choose>

		</div>

	</div>
</form:form>
<purchaserequisitionlines:purchaserequisitionlines/>

<div class="col-xs-6">
	<!-- <button class="btn btn-primary btn-xs" type="submit" data-toggle="modal" data-target="#myModal" onclick=showpopUp()>Add Line</button> -->
	<button type="button" id="purchaserequisitionLine"
		class="btn btn-primary btn-xs">Add New Line</button>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseRequisitionLineModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-content modal-forms">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="createPurchaseLabel">Purchase
						Requisition Line</h4>
				</div>
				
				<!-- 
				private Long PRLId;	
	private String productCode;
	private String productName;
	private String UOM;
	private Double quantity;
	private String availableStock;
	private String requiredDate;
	private String purpose;
	private String remarks;
	
				 -->
				=================${pageContext.request.contextPath}==============
				<input id="lineSaveURL" type="hidden"
					value="${pageContext.request.contextPath}/admin/purchase/purchaserequisitionline/save/${purchaseRequisitionForm.PRId}" />
				<input id="lineUpdateURL" type="hidden"
					value="${pageContext.request.contextPath}/" /> <input
					id="lineDeleteURL" type="hidden"
					value="${pageContext.request.contextPath}/" />

				<div class="modal-body col-xs-12" style="background: white;">
					<form id="purchaseRequisitionLineForm"
						action="${pageContext.request.contextPath}/${purchaseRequisitionLineSaveURL}"
						method="POST" onsubmit="return validateform()">
						===DOCUMENT
						NUMBER==========${purchaseOrderLineForm.POLId}==========================
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="row">
							<div class="col-lg-12">
								<h5>Purchase Order Line Details</h5>
								<h5 style="color: green;">${message}</h5>
								<div class="panel-body">
									<input type="hidden" name="productCode" id="productCode"
										class="form-control"></input>										
								 <input type="hidden" name="PRLId"
										id="PRLId" class="form-control"></input>
								
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Product Name" /></label> <input
													name="productName" id="productName" class="form-control"><span
													class="glyphicon glyphicon-modal-window"
													class="btn btn-info btn-lg"
													onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getProductDetails');"></span></input>
											</div>
											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Quantity" /></label>
												<input name="quantity" id="quantity"
													class="form-control"></input>
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Avilable Stock" /></label>
												<input name="availableStock" id="availableStock"
													class="form-control"></input>
											</div>
											 <div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Rrequired Date" /></label>
												<input name="requiredDate" id="requiredDate"
													class="form-control"></input>
											</div> 											
											
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="UOM" /></label> <input
													name="UOM" id="UOM" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Purpose" /></label> <input
													name="purpose" id="purpose" class="form-control" />
											</div>
																																	
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Remarks" /></label> <input
													name="remarks" id="remarks" class="form-control" />
											</div> 
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 30px;"></div>
						=====LSU====${lineStockingOfShrimpsToTankURL}=====
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="saveButton"
										type="button">Save</button>
								</div>
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs"
										class="cancel_btn button" id="cancelButton"
										data-dismiss="modal">Cancel</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="deleteButton"
										type="submit">Delete</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="updateButton"
										type="button">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>







