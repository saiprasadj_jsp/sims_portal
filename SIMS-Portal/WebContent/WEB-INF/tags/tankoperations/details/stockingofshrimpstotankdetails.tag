<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="tankoperationsdetails"
	tagdir="/WEB-INF/tags/tankoperations/details"%>
<%@taglib prefix="tankoperationslist"
	tagdir="/WEB-INF/tags/tankoperations/list"%>

<head>
<script type="text/javascript">
	function validateform() {
		/* var number = document.getElementById("phone").value;
		var x=document.getElementById("email").value; 
		var atposition=x.indexOf("@");  
		var dotposition=x.lastIndexOf(".");
		var msg=" ";
		if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
			  alert("Please enter a valid e-mail address ");  
			  return false;  
		 }  
		if (isFinite(number)) {
			return true;
		} else {
			alert("Please Enter Phone Number");
			return false;
		} */

		alert("from Validation");
		return true;
	}
</script>
</head>
<form:form id="stockingofshrimpsDetailsForm"
	modelAttribute="stockingOfShrimpsToTankForm"
	action="${pageContext.request.contextPath}/tankoperations/${stockingOfShrimpsToTankURL}"
	method="POST" onsubmit="return validateform()">
	<input type="hidden" id="currentURL"
		value="${requestScope['javax.servlet.forward.request_uri']}" />
	===DOCUMENT NUMBER==========${stockingOfShrimpsToTankForm.stockingofshrimpstotank_id}==========================
	<div class="row">
		<div class="col-lg-12">
			<h5>Stocking Of Shrimps Details</h5>
			<h5 style="color: green;">${message}</h5>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="DocumentNumber" /></label>
							<form:input path="stockingofshrimpstotank_id"
								id="stockingofshrimpstotank_id" class="form-control"
								readonly="true"></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Department" /></label>
							<form:input path="departmentName" id="departmentName"
								class="form-control"></form:input>
							<span class="glyphicon glyphicon-modal-window"
								class="btn btn-info btn-lg"
								onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getDepartmentDetails');"></span>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Stacking Start Date & Time" /></label>
							<form:input path="stackingStartDateAndTime"
								id="stackingStartDateAndTime" class="form-control" />
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Date" /></label>
							<form:input path="presentDate" id="presentDate"
								class="form-control" />
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message
									text="Stacking Completed Date & Time" /></label>
							<form:input path="stackingCompletedDateAndTime"
								id="stackingCompletedDateAndTime" class="form-control" />
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Tank" /></label>
							<form:input path="tankName" id="tankName" class="form-control"></form:input>
							<span class="glyphicon glyphicon-modal-window"
								class="btn btn-info btn-lg"
								onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getSectionDetails');"></span>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Employee" /></label>
							<form:input path="employeeName" id="employeeName"
								class="form-control"></form:input>
							<span class="glyphicon glyphicon-modal-window"
								class="btn btn-info btn-lg"
								onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getEmployeeDetails');"></span>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Man Power Used" /></label>
							<form:input path="manPowerUsed" id="manPowerUsed"
								class="form-control"></form:input>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="WareHouse" /></label>
							<form:input path="warehouseName" id="warehouseName"
								class="form-control" />
							<span class="glyphicon glyphicon-modal-window"
								class="btn btn-info btn-lg"
								onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getWarehouseDetails');"></span>
						</div>
						<div class="form-group" style="margin-bottom: 0px">
							<label><spring:message text="Narration" /></label>
							<form:input path="narration" id="narration" class="form-control" />
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="row" style="margin-top: 30px;"></div>
	<div class="col-lg-12">
		<div class="row">
			<c:choose>
				<c:when
					test="${stockingOfShrimpsToTankURL == 'stockingofshrimps/save'}">
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Save</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit"
							disabled="disabled">Update</button>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Update</button>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-primary btn-xs" type="submit">Delete</button>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</form:form>
<tankoperationslist:stockingofshrimpstotanklines />
<div class="col-xs-6">
	<!-- <button class="btn btn-primary btn-xs" type="submit" data-toggle="modal" data-target="#myModal" onclick=showpopUp()>Add Line</button> -->
	<button type="button" id="addNewLine" class="btn btn-primary btn-xs">Add
		New Line</button>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-content modal-forms">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="createPurchaseLabel">Purchase
						Order Line</h4>
				</div>
				<input id="lineSaveURL" type="hidden"
					value="${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/save/${stockingOfShrimpsToTankForm.stockingofshrimpstotank_id}" />
				<input id="lineUpdateURL" type="hidden"
					value="${pageContext.request.contextPath}/" /> <input
					id="lineDeleteURL" type="hidden"
					value="${pageContext.request.contextPath}/" />

				<div class="modal-body col-xs-12" style="background: white;">
					<form id="lineStockingOfShrimpsToTankForm"
						action="${pageContext.request.contextPath}/tankoperations/${lineStockingOfShrimpsToTankURL}"
						method="POST" onsubmit="return validateform()">
						===DOCUMENT
						NUMBER==========${lineStockingOfShrimpsToTankForm.lineStockingOfShrimpsToTank_id}==========================
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="row">
							<div class="col-lg-12">
								<h5>Stocking Of Shrimps Details</h5>
								<h5 style="color: green;">${message}</h5>
								<div class="panel-body">
									<input type="hidden" name="productCode" id="productCode"
										class="form-control"></input> <input type="hidden"
										name="lineStockingOfShrimpsToTank_id"
										id="lineStockingOfShrimpsToTank_id" class="form-control"></input>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Product Name" /></label> <input
													name="productName" id="productName" class="form-control"><span
													class="glyphicon glyphicon-modal-window"
													class="btn btn-info btn-lg"
													onclick="openInnerPopup('${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/getProductDetails');"></span></input>
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Product Description" /></label>
												<input name="productDescription" id="productDescription"
													class="form-control"></input>
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="UOM" /></label> <input
													name="UOM" id="UOM" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message
														text="Quantity (Pieces Per Bag)" /></label> <input
													name="quantity" id="quantity" class="form-control" />
											</div>
											<div class="form-group" style="margin-bottom: 0px">
												<label><spring:message text="Remarks" /></label> <input
													name="remarks" id="remarks" class="form-control" />
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 30px;"></div>
						=====LSU====${lineStockingOfShrimpsToTankURL}=====
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="saveButton"
										type="button">Save</button>
								</div>
								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs"
										class="cancel_btn button" id="cancelButton"
										data-dismiss="modal">Cancel</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="deleteButton"
										type="submit">Delete</button>
								</div>

								<div class="col-xs-3">
									<button class="btn btn-primary btn-xs" id="updateButton"
										type="button">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>