<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%-- <%@ attribute name="dailyTankRecordingFeedingDetails" required="true"%>	 --%>

<div class="row">
	<div class="col-lg-12"><h5>Feed 1</h5></div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Name" /></label>
					<form:input path="feed1Name" id="feed1Name"  class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Quantity" /></label>
					<form:input path="feed1Quantity" id="feed1Quantity" autocomplete="off" onkeyup="feedQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Name" /></label>
					<form:input path="feed1MedicineName" id="feed1MedicineName" 
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Quantity" /></label>
					<form:input path="feed1MedicineQuantity" id="feed1MedicineQuantity" autocomplete="off" onkeyup="medicineQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Consumed %" /></label>
					<form:input path="feed1ConsumedPercentage"
						id="feed1ConsumedPercentage" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Timings" /></label>
					<form:input path="feed1MedicineTimings" id="feed1MedicineTimings"
						class="form-control"></form:input>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12"><h5>Feed 2</h5></div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Name" /></label>
					<form:input path="feed2Name" id="feed2Name" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Quantity" /></label>
					<form:input path="feed2Quantity" id="feed2Quantity" autocomplete="off" onkeyup="feedQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Name" /></label>
					<form:input path="feed2MedicineName" id="feed2MedicineName"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Quantity" /></label>
					<form:input path="feed2MedicineQuantity" id="feed2MedicineQuantity" autocomplete="off" onkeyup="medicineQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Consumed %" /></label>
					<form:input path="feed2ConsumedPercentage"
						id="feed2ConsumedPercentage" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Timings" /></label>
					<form:input path="feed2MedicineTimings" id="feed2MedicineTimings"
						class="form-control"></form:input>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12"><h5>Feed 3</h5></div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Name" /></label>
					<form:input path="feed3Name" id="feed3Name" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Quantity" /></label>
					<form:input path="feed3Quantity" id="feed3Quantity" autocomplete="off" onkeyup="feedQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Name" /></label>
					<form:input path="feed3MedicineName" id="feed3MedicineName" 
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Quantity" /></label>
					<form:input path="feed3MedicineQuantity" id="feed3MedicineQuantity" autocomplete="off" onkeyup="medicineQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Consumed %" /></label>
					<form:input path="feed3ConsumedPercentage"
						id="feed3ConsumedPercentage" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Timings" /></label>
					<form:input path="feed3MedicineTimings" id="feed3MedicineTimings"
						class="form-control"></form:input>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12"><h5>Feed 4</h5></div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Name" /></label>
					<form:input path="feed4Name" id="feed4Name" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed Quantity" /></label>
					<form:input path="feed4Quantity" id="feed4Quantity" autocomplete="off" onkeyup="feedQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Name" /></label>
					<form:input path="feed4MedicineName" id="feed4MedicineName"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Quantity" /></label>
					<form:input path="feed4MedicineQuantity" id="feed4MedicineQuantity" autocomplete="off" onkeyup="medicineQuantityCalculation();"
						class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Consumed %" /></label>
					<form:input path="feed4ConsumedPercentage"
						id="feed4ConsumedPercentage" class="form-control"></form:input>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine Timings" /></label>
					<form:input path="feed4MedicineTimings" id="feed4MedicineTimings"
						class="form-control"></form:input>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="margin-top: 50px;"></div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Feed WareHouse" /></label>
					<form:input path="feedWareHouse" id="feedWareHouse"
						class="form-control"></form:input>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Total Feed" /></label>
					<form:input path="totalFeedQantity" readonly="true" id="totalFeedQantity"
						class="form-control"></form:input>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Medicine WareHouse" /></label>
					<form:input path="feedMedicineWarehouse" id="feedMedicineWarehouse"
						class="form-control"></form:input>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="form-group" style="margin-bottom: 0px">
					<label><spring:message text="Total Medicine Quantity" /></label>
					<form:input path="totalMedicineQantity" readonly="true" id="totalMedicineQantity" 
						class="form-control"></form:input>
				</div>
			</div>
		</div>
	</div>
</div>