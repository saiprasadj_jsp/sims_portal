<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Remarks</th>
						<th>Line ID</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listOfLineStockingOfShrimpsToTankObj}"
						var="lineStockingOfShrimpsToTankObj" varStatus="counter">
						<tr>
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}</td>
							<td>${lineStockingOfShrimpsToTankObj.productName}</td>
							<td>${lineStockingOfShrimpsToTankObj.productDescription}</td>
							<td>${lineStockingOfShrimpsToTankObj.UOM}</td>
							<td>${lineStockingOfShrimpsToTankObj.quantity}</td>
							<td>${lineStockingOfShrimpsToTankObj.remarks}</td>
							<td>${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}</td>
							<td><a
								href="${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/delete/${lineStockingOfShrimpsToTankObj.lineStockingOfShrimpsToTank_id}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/tankoperations/linestockingofshrimps/delete" />
		</div>
	</div>
</div>