<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>

<div class="row">
	<div class="col-lg-12">
		<div class="card-block">
			<table id="indexedTable"
				class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>To Tank</th>
						<th>To Warehouse</th>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Remarks</th>
						<th>Line ID</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listOfLineTankToTankShrimpsTransferObj}"
						var="lineTankToTankShrimpsTransferObj" varStatus="counter">
						<tr>
							<td id="editURL${counter.index}" class="hidden">${pageContext.request.contextPath}/tankoperations/linetanktotankshrimpstransfer/edit/${lineTankToTankShrimpsTransferObj.lineTankToTankShrimpsTransfer_id}</td>
							<td>${lineTankToTankShrimpsTransferObj.toTankName}</td>
							<td>${lineTankToTankShrimpsTransferObj.toWarehouseName}</td>
							<td>${lineTankToTankShrimpsTransferObj.productName}</td>
							<td>${lineTankToTankShrimpsTransferObj.productDescription}</td>
							<td>${lineTankToTankShrimpsTransferObj.UOM}</td>
							<td>${lineTankToTankShrimpsTransferObj.quantity}</td>
							<td>${lineTankToTankShrimpsTransferObj.remarks}</td>
							<td>${lineTankToTankShrimpsTransferObj.lineTankToTankShrimpsTransfer_id}</td>
							<td><a
								href="${pageContext.request.contextPath}/tankoperations/linetanktotankshrimpstransfer/edit/${lineTankToTankShrimpsTransferObj.lineTankToTankShrimpsTransfer_id}">
									Edit</a> || <a
								href="${pageContext.request.contextPath}/tankoperations/linetanktotankshrimpstransfer/delete/${lineTankToTankShrimpsTransferObj.lineTankToTankShrimpsTransfer_id}">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" id="editURL"
				value="${pageContext.request.contextPath}/tankoperations/linetanktotankshrimpstransfer/edit" />
			<input type="hidden" id="deleteURL"
				value="${pageContext.request.contextPath}/tankoperations/linetanktotankshrimpstransfer/delete" />
		</div>
	</div>
</div>