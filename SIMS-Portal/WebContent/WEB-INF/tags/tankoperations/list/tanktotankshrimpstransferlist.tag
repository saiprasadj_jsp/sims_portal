<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Daily Tank Recording List</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Document ID</th>
							<th>Department Name</th>
							<th>From Tank Name</th>
							<th>Transfer Start Date And Time</th>
							<th>Transfer Completed Date And Time</th>
							<th>Warehouse</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${tankToTankShrimpsTransferFormListData}"
							var="tankToTankShrimpsTransferFormListObj">
							<tr>
								<td>${tankToTankShrimpsTransferFormListObj.tanktotankshrimpstransfer_id}</td>
								<td>${tankToTankShrimpsTransferFormListObj.departmentName}</td>
								<td>${tankToTankShrimpsTransferFormListObj.fromTankName}</td>
								<td>${tankToTankShrimpsTransferFormListObj.transferStartDateAndTime}</td>
								<td>${tankToTankShrimpsTransferFormListObj.transferCompletedDateAndTime}</td>
								<td>${tankToTankShrimpsTransferFormListObj.warehouseName}</td>
								<td><a
									href="${pageContext.request.contextPath}/tankoperations/tanktotankshrimpstransfer/edit/${tankToTankShrimpsTransferFormListObj.tanktotankshrimpstransfer_id}">
										Edit</a> || <a
									href="${pageContext.request.contextPath}/tankoperations/tanktotankshrimpstransfer/delete/${tankToTankShrimpsTransferFormListObj.tanktotankshrimpstransfer_id}">Delete</a>
								</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
</html>