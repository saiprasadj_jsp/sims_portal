<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<h3>Section Details</h3>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table id="sectionDetailsPopup"
					class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Section Code</th>
							<th>Section Name</th>
							<th>Section Alias</th>
							<th>Section ID</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${sectionMasterFormListData}"
							var="sectionDetailsObj" varStatus="counter">
							<tr>
								<td id="sectionName${counter.index}">${sectionDetailsObj.name}</td>
								<td id="sectionCode${counter.index}">${sectionDetailsObj.code}</td>
								<td id="sectionAlias${counter.index}">${sectionDetailsObj.alias}</td>
								<td id="sectionID${counter.index}">${sectionDetailsObj.id}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
<script type="text/javascript">
	var sectionObj = {
		sectionCode : "",
		sectionName : "",
		UOM : "",
		sectionID : ""
	};
	$("#sectionDetailsPopup").find("tr").click(function() {
		var indexVal = $(this).index();
		sectionObj.sectionCode = $("#sectionCode" + indexVal).html();
		sectionObj.sectionName = $("#sectionName" + indexVal).html();
		sectionObj.sectionID = $("#sectionID" + indexVal).html();

		window.opener.captureSectionObjDetails(sectionObj);
		window.close();
	});
</script>