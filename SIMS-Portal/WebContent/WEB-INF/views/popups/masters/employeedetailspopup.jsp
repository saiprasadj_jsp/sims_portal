<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<h3>Employee Details</h3>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table id="employeeDetailsPopup"
					class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Employee Code</th>
							<th>Employee Name</th>
							<th>Employee Alias</th>
							<th>Employee ID</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${employeeMasterFormListData}"
							var="employeeDetailsObj" varStatus="counter">
							<tr>
								<td id="employeeName${counter.index}">${employeeDetailsObj.name}</td>
								<td id="employeeCode${counter.index}">${employeeDetailsObj.code}</td>
								<td id="employeeAlias${counter.index}">${employeeDetailsObj.alias}</td>
								<td id="employeeID${counter.index}">${employeeDetailsObj.id}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
<script type="text/javascript">
	var employeeObj = {
		employeeCode : "",
		employeeName : "",
		UOM : "",
		employeeID : ""
	};
	$("#employeeDetailsPopup").find("tr").click(function() {
		var indexVal = $(this).index();
		employeeObj.employeeCode = $("#employeeCode" + indexVal).html();
		employeeObj.employeeName = $("#employeeName" + indexVal).html();
		employeeObj.employeeID = $("#employeeID" + indexVal).html();

		window.opener.captureEmployeeObjDetails(employeeObj);
		window.close();
	});
</script>