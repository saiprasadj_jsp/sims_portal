<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<h3>Department Details</h3>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table id="departmentDetailsPopup"
					class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Department Code</th>
							<th>Department Name</th>
							<th>Department Alias</th>
							<th>Department ID</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${departmentMasterFormListData}"
							var="departmentDetailsObj" varStatus="counter">
							<tr>
								<td id="departmentCode${counter.index}">${departmentDetailsObj.code}</td>
								<td id="departmentName${counter.index}">${departmentDetailsObj.name}</td>
								<td id="departmentAlias${counter.index}">${departmentDetailsObj.alias}</td>
								<td id="departmentID${counter.index}">${departmentDetailsObj.id}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
<script type="text/javascript">
	var departmentObj = {
		departmentCode : "",
		departmentName : "",
		departmentID : ""
	};
	$("#departmentDetailsPopup").find("tr").click(function() {
		var indexVal = $(this).index();
		departmentObj.departmentCode = $("#departmentCode" + indexVal).html();
		departmentObj.departmentName = $("#departmentName" + indexVal).html();
		departmentObj.departmentID = $("#departmentID" + indexVal).html();

		window.opener.captureDepartmentObjDetails(departmentObj);
		window.close();
	});
</script>