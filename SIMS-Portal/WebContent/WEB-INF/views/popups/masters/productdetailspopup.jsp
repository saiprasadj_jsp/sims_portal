<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<h3>Product Details</h3>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table id="productDetailsPopup"
					class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Product Alias</th>
							<th>Product UOM</th>
							<th>Product ID</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productMasterFormListData}"
							var="productDetailsObj" varStatus="counter">
							<tr>
								<td id="productName${counter.index}">${productDetailsObj.name}</td>
								<td id="productCode${counter.index}">${productDetailsObj.code}</td>
								<td id="productAlias${counter.index}">${productDetailsObj.alias}</td>
								<td id="productUOM${counter.index}">${productDetailsObj.uom}</td>
								<td id="productID${counter.index}">${productDetailsObj.id}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
<script type="text/javascript">
	var productObj = {
		productCode : "",
		productName : "",
		UOM : "",
		productID : ""
	};
	$("#productDetailsPopup").find("tr").click(function() {
		var indexVal = $(this).index();
		productObj.productCode = $("#productCode" + indexVal).html();
		productObj.productName = $("#productName" + indexVal).html();
		productObj.UOM = $("#productUOM" + indexVal).html();
		productObj.productID = $("#productID" + indexVal).html();

		window.opener.captureProductObjDetails(productObj);
		window.close();
	});
</script>