<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<h3>Warehouse Details</h3>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="card-block">
				<table id="warehouseDetailsPopup"
					class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Warehouse Code</th>
							<th>Warehouse Name</th>
							<th>Warehouse Alias</th>
							<th>Warehouse ID</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${warehouseMasterFormListData}"
							var="warehouseDetailsObj" varStatus="counter">
							<tr>
								<td id="warehouseName${counter.index}">${warehouseDetailsObj.name}</td>
								<td id="warehouseCode${counter.index}">${warehouseDetailsObj.code}</td>
								<td id="warehouseAlias${counter.index}">${warehouseDetailsObj.alias}</td>
								<td id="warehouseID${counter.index}">${warehouseDetailsObj.id}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Prev</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!--/col-->
	</div>
</body>
<script type="text/javascript">
	var warehouseObj = {
		warehouseCode : "",
		warehouseName : "",
		warehouseID : ""
	};
	$("#warehouseDetailsPopup").find("tr").click(function() {
		var indexVal = $(this).index();
		warehouseObj.warehouseCode = $("#warehouseCode" + indexVal).html();
		warehouseObj.warehouseName = $("#warehouseName" + indexVal).html();
		warehouseObj.warehouseID = $("#warehouseID" + indexVal).html();

		window.opener.captureWarehouseObjDetails(warehouseObj);
		window.close();
	});
</script>