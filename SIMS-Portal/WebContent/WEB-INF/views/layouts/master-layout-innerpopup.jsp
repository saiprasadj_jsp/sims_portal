<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/common"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html "about:legacy-compat">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="author" content="ask" />
<link href="resources/images/favicon1.ico" rel="icon"
	type="image/x-icon">
<!-- CSS Section -->
<common:styleSheets />
<style type="text/css">
th {
	color: #fff;
    background-color: #1985ac;
    border-color: #187fa3;
}

body{
	
	 background-color: white;
}
</style>
</head>

<sec:authorize access="isAuthenticated()">
	<c:set var="isAuthenticated" value="true" />
</sec:authorize>
<body
	class="container-fluid">
	<!-- JavaScript Section -->
	<common:javaScripts />

	<div id="wrapper">

		<!-- Body : Section -->
		<!-- Main content -->
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<main class="main">
		<div>
			<tiles:insertAttribute name="body" />
		</div>
		</main>
		<!-- Footer Section -->
	</div>
</body>

</html>