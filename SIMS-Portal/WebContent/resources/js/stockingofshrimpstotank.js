$(document).ready(function() {
	// alert("From Stocking Of Shrimps To Tank Function");

});

/*
 * $("table tr").click(function() { //alert(this.rowIndex); });
 */
$("#indexedTable").find("tr").click(function() {
	var indexVal = $(this).index();
	var idName = "#lineID" + indexVal;
	var editURLVal = $("#editURL" + indexVal).html();
	$.ajax({
		type : "GET",
		url : editURLVal,
		success : function(htmlResponse) {
			/*
			 * lineStockingOfShrimpsToTank_id: 1 productCode: ""
			 * productDescription: "1212" productName: "SASAS" quantity: 121
			 * remarks: "11" uom: "1212"
			 */
			// alert("Data Posted Successfully" + htmlResponse.productName);
			setModalDialogData(htmlResponse);

			$('#myModal').modal('toggle');
		},
		error : function(e) {
			alert('error handing here' + e);
		}
	});
});

$('#myModal').on('shown.bs.modal', function(e) {
	// alert("On Show Event Called");
});

$("#addNewLine").click(function() {

	// alert("Add New line");
	setModalDialogData({});
	$('#myModal').modal('toggle');

});

$("#saveButton").click(function() {
	var lineSaveURL = $("#lineSaveURL").val();
	var datastring = $("#lineStockingOfShrimpsToTankForm").serialize();
	$.ajax({
		type : "POST",
		url : $("#lineSaveURL").val(),
		data : datastring,
		success : function(data) {
			// alert("Data Posted Successfully");
			$('#myModal').modal('toggle');
			window.location = $("#currentURL").val();
		},
		error : function(e) {
			alert('error handing here' + e);
		}
	});

});

$("#updateButton").click(function() {
	var datastring = $("#lineStockingOfShrimpsToTankForm").serialize();
	$.ajax({
		type : "POST",
		url : $("#lineUpdateURL").val(),
		data : datastring,
		success : function(data) {
			// alert("Data Posted Successfully");
			$('#myModal').modal('toggle');
			window.location = $("#currentURL").val();
		},
		error : function(e) {
			alert('error handing here' + e);
		}
	});

});

$("#deleteButton").click(function() {
	var datastring = $("#lineStockingOfShrimpsToTankForm").serialize();
	$.ajax({
		type : "POST",
		url : $("#lineDeleteURL").val(),
		data : datastring,
		success : function(data) {
			// alert("Data Deleted Successfully");
			$('#myModal').modal('toggle');
			window.location = $("#currentURL").val();
		},
		error : function(e) {
			alert('error handing here' + e);
		}
	});

});

function setModalDialogData(htmlResponse) {

	$("#productCode").val(htmlResponse.productCode);
	$("#productName").val(htmlResponse.productName);
	$("#productDescription").val(htmlResponse.productDescription);
	$("#quantity").val(htmlResponse.quantity);
	$("#UOM").val(htmlResponse.uom);
	$("#remarks").val(htmlResponse.remarks);
	$("#lineStockingOfShrimpsToTank_id").val(
			htmlResponse.lineStockingOfShrimpsToTank_id);
	$("#lineUpdateURL").val($("#lineUpdateURL").val() + htmlResponse.updateURL);
	$("#lineDeleteURL").val($("#lineDeleteURL").val() + htmlResponse.deleteURL);

	if (Object.keys(htmlResponse).length === 0
			&& htmlResponse.constructor === Object) {
		$("#saveButton").show();
		$("#deleteButton").hide();
		$("#updateButton").hide();
	} else {
		$("#saveButton").hide();
		$("#deleteButton").show();
		$("#updateButton").show();
	}
}

function openInnerPopup(popupurl) {
	var myWindow = window.open(popupurl, "MsgWindow", "width=750,height=600");
}

function captureProductObjDetails(productObj) {
	$("#productCode").val(productObj.productCode);
	$("#productName").val(productObj.productName);
	$("#UOM").val(productObj.UOM);
}

function captureDepartmentObjDetails(departmentObj) {
	$("#departmentCode").val(departmentObj.departmentCode);
	$("#departmentName").val(departmentObj.departmentName);
}

function captureEmployeeObjDetails(employeeObj) {
	$("#employeeCode").val(employeeObj.employeeCode);
	$("#employeeName").val(employeeObj.employeeName);
}

function captureSectionObjDetails(sectionObj) {
	$("#tankCode").val(sectionObj.sectionCode);
	$("#tankName").val(sectionObj.sectionName);
}

function captureWarehouseObjDetails(warehouseObj) {
	$("#warehouseCode").val(warehouseObj.warehouseCode);
	$("#warehouseName").val(warehouseObj.warehouseName);
}